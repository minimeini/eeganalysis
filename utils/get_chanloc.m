function [G, H] = get_chanloc(loadpath, savepath_loc, varargin)
    p = inputParser;
    addRequired(p, 'loadpath', @ischar);
    addRequired(p, 'savepath_loc', @ischar);
    addParameter(p, 'savepath_lap', [], @ischar);
    parse(p, loadpath, savepath_loc, varargin{:});
    
    fileID = fopen(p.Results.loadpath, 'r');
    channels = textscan(fileID, '%d %s %f %f %f %f %f %f %f %f',...
        'HeaderLines', 1);
    fclose(fileID);
    channels = channels{2};

    ConvertLocations(p.Results.loadpath, p.Results.savepath_loc, channels);
    montage = ExtractMontage(p.Results.savepath_loc,channels);
    [G, H] = GetGH(montage);
    
    if nargin > 2 && ~isempty(p.Results.savepath_lap)
        save(p.Results.savepath_lap, 'G', 'H');
    end
end
