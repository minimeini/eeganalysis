function tmprank = getrank(tmpdata, varargin)
% function tmprank = getrank(tmpdata, varargin)
%
% Note: small eigenvalues == big condition number == possibly singular
% Optional Input: rankType = {1,2,3,4}, rankTolerance, infoCriType,
% PCARange
%
% - rankType
% ---- Auto: auto selection between MATLAB default and PCA Hard
% ---- MATLAB: use default `rank` function in MATLAB
% ---- PCA Hard: use specified eigenvalue cutoff `rankTolerance`, 
% default = 1e-10,
% ---- PCA Sort: use information criterion `infoCriType` to select the
% optimal rank, searching range can be specified by `PCARange`
%%
% number of required inputs
default_args = 1;

% default value of optional inputs
rankType = 1;
rankTolerance = 1e-10;
infoCriType = 'AIC'; % MDL, AIC, BIC
PCARange = [2 rank(tmpdata)-2];

% user specified optional inputs
if nargin >= default_args + 1
    if ~isnumeric(varargin{1}) || ~ismember(varargin{1}, [1 2 3 4])
        error('INPUT ERROR: Rank Type must be numeric: 1(auto), 2(matlab), 3(PCA Hard), 4(PCA Soft).');
    end
    rankType = varargin{1};
end
if nargin >= default_args + 2
    if ~isnumeric(varargin{2})
        error('INPUT ERROR: Rank Tolerance must be numeric.');
    end
    rankTolerance = varargin{2};
end
if nargin >= default_args + 3
    if ~ischar(varargin{3}) || ~ismember(varargin{3}, {'MDL', 'AIC', 'BIC'})
        error('INPUT ERROR: Type of Information Criterion must be {MDL, AIC, BIC}');
    end
    infoCriType = varargin{3};
end
if nargin >= default_args + 4
    if ~isnumeric(varargin{4}) || numel(varargin{4})~=2
        error('INPUT ERROR: PCA searching range must be [minrank, maxrank]');
    end
    PCARange = varargin{4};
end

%%
tmprank1 = rank(tmpdata); % MATLAB default

% PCA Hard
if rankType==2 || rankType==3
    covarianceMatrix = cov(tmpdata', 1);
    [~, D] = eig (covarianceMatrix);
    tmprank2=sum (diag (D) > rankTolerance);
end

% PCA Soft
rankGrid = PCARange(1):PCARange(2);
rankScore = zeros(1, numel(rankGrid));
for i = 1:numel(rankGrid)
    rankScore(i) = infoScore_pca(data, rankGrid(i), infoCriType);
end
tmprank3 = findchangepts(rankScore, 'Statistic', 'linear');
%% get final rank
switch rankType
    case 1
        tmprank = min(tmprank1, tmprank2);
    case 2
        tmprank = tmprank1;
    case 3
        tmprank = tmprank2;
    case 4
        tmprank = tmprank3;
    otherwise
        error('INPUT ERROR: Rank Type must be numeric: 1(auto), 2(matlab), 3(PCA Hard), 4(PCA Soft).');
end
    

%% Check after calculation finished
if tmprank==tmprank1 && cond(covarianceMatrix) > 100
    warning('Covariance matrix is singular, MATLAB default algorithm might be imprecise.');
end


end