folderlist = {'U', 'V', 'W', 'X'};
metrics = {'PLV', 'wPLI'};
flabs = {'_smooth_zerolag', '_smooth_n24e0p24', '_smooth_n24p24'};
timelags = struct('interval', cell(1,3));
timelags(1).interval = [];
temp = -2:1:2; % 12 ms
timelags(2).interval = temp(temp ~= 0);
timelags(3).interval = -2:1:2;