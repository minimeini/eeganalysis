function [wd_raw, wd_prep] = initwd(type)
p = inputParser;
addRequired(p, 'type', ...
    @(x) any(validatestring(x, {'home', 'lab'})));
parse(p, type);

switch p.Results.type
    case 'home'
        cd('D:\repository\eeganalysis');
        wd_raw = '';
        wd_prep = 'D:\Study Resources\neuroscience\data'; 
    case 'lab'
        cd('G:\meini\eeganalysis');
        wd_raw = 'G:\ownCloud\EEG\Data\Movement&Imagery\expr';
        wd_prep = 'G:\meini\data'; 
end

end