function [labels, left, right] = getLabels(parScheme, varargin)
p = inputParser;
addRequired(p, 'parScheme', ...
    @(x) any(validatestring(x, {'eight_part', 'fourteen', 'none'})));
addOptional(p, 'chanlocs', []);
parse(p, parScheme, varargin{:});

switch char(p.Results.parScheme)
    case 'eight_part'
        labels = {'LF', 'RF', 'LC', 'RC', 'LP', 'RP', 'LO', 'RO'};
        left = 1:2:8;
        right = left + 1;
    case 'fourteen'
        labels = {'LF1', 'LF2', 'RF1', 'RF2', ...
            'LC1', 'LC2', 'RC1', 'RC2', ...
            'LP1', 'LP2', 'RP1', 'RP2', ...
            'LO', 'RO'};
        left = sort([1:4:9, 2:4:10]);
        right = left + 2;
        left(end+1) = 13; right(end+1) = 14;
    case 'none'
        labels = p.Results.chanlocs;
    otherwise
end
end