function [EEG] = set2mat(filename, filepath, varargin)
p = inputParser;
addRequired(p, 'filename', @ischar);
addRequired(p, 'filepath', @ischar);
addParameter(p, 'savepath', [], @ischar);
parse(p, filename, filepath, varargin{:});

filename = p.Results.filename;
EEG = pop_loadset(filename, p.Results.filepath);

if nargin > 2 && ~isempty(p.Results.savepath)
    filename = regexp(filename, '\.', 'split');
    filename = char(filename(1));
    save_epoch(EEG.data, [filename '.mat'], p.Results.savepath);
end
end