% initialization
run('C:\Respository\eeganalysis\initHome.m');

%% [parameter setting]
nbchan = 64;
foldername = 'parPLVmatrix_direct_trim';
fnlist = {'PLVmatrix', 'parPLVmatrix', 'parPLVmatrix_direct', ...
    'parPLVmatrix_direct_trim'};
[filename, tasklab] = getSubfile(fullfile(wd_prep, foldername));

times = load(fullfile(wd_prep, 'hilb_times.mat'));
temp = fieldnames(times);
times = times.(temp{1});
colorSeq = rand(64,3);

t =1;

chanlocs = readlocs(fullfile(wd_chanloc, 'actiCap64Channel.xyz'), ...
    'filetype', 'xyz');

%%
for k = 1:numel(fnlist)
    for i = 1:numel(filename)
        sp_dat = fullfile(wd_prep, ['sepcsv_' fnlist{k}], tasklab{t}, ...
            filename{i}(1:end-4));
        checkPath(sp_dat);
        
        conn = load(fullfile(wd_prep, fnlist{k}, ...
            tasklab{t}, filename{i}));
        temp = fieldnames(conn);
        conn = conn.(temp{1});
        
        for j = 1:size(conn,3)
            temp = squeeze(conn(:,:,j));
            dlmwrite(fullfile(sp_dat, ...
                [filename{i}(1:end-4) '_' num2str(j) '.csv']), temp);
        end
    end
end