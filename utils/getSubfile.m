function [filename, tasklab, foldername] = getSubfile(foldern, varargin)
p = inputParser;
addRequired(p, 'foldern', @ischar);
addOptional(p, 'format', [], @ischar);
parse(p, foldern, varargin{:});

[tasklab, ~, foldername] = getFilename(p.Results.foldern);
filename = getFilename(char(foldername(1)), p.Results.format);
end