%% Initialization
type = 'home2'; % 'home' or 'lab', 'lab2'
%%
switch type
    case 'home2'
        wd_code = 'C:\Respository\eeganalysis';
        wd_prep = 'C:\eegAnalysis';
    case 'lab2'
        wd_code = 'H:\meini\eeganalysis';
        wd_prep = 'H:\meini\data';
end
cd(wd_code);

wd_raw = fullfile(wd_prep, 'raw');
wd_ana = fullfile(wd_prep, 'analysis');
wd_chanloc = fullfile(wd_prep, 'chanlocs');
wd_vis = fullfile(wd_prep, 'visualization');

if exist(wd_prep, 'dir') ~= 7, mkdir(wd_prep); end
if exist(wd_vis, 'dir') ~= 7, mkdir(wd_vis); end

pop_editoptions('option_single', false);
folderlist = {'U', 'V', 'W', 'X'};