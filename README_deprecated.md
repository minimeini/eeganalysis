# README #

## procedure

1. `pipeline_clean.m` 对标签进行清洗，删去按 home 键时长小于 1.5s 的 trial.
2. `eeg_prep.m` load chanlocs, change data type from single to double, filter data into [1 30] Hz
    - go to step 4 without artifacts removal and get the output `Data/Movement&Imaginary/epoch_raw `
3. remove bursts and robust CAR
	- downsample data to 250 HZ
	- `clean_artifacts.m` detect and remove bad channels. remove and reconstruct bursts via ASR (artifact subspace reconstruction) whose standard deviation is larger than 20 relative to the threshold of the calibration data. Parameters:
        - `BurstCriterion` = 20
        - `BurstCriterionRefMaxBadChns` set to `off`, all data is used for calibration.
        - `BurstCriterionRefTolerances` = [-20 20]. the power tolerances outside of which a channel in a given time window is considered "bad", in standard deviations relative to a robust EEG power distribution.
    - interpolate removed channels
    - CAR based on the interpolated data
4. `epoch_and_basecor.m` epoch length: [-2 1], 0 at home button released, baseline length: [-0.5 0], 0 at home button pressed.
5. `rankDeficientICA.m` reject epochs based on the processed data, then do ICA using AMICA algorithm, then reject epochs again based on the ICA components.
6. `RemoveStrongArtifacts.m` Further deal with artifacts using wavelet enhanced ICA, `threshold` for denoising of artifacts is set to `1.5`
    - OUT: `Data/Movement&Imaginary/Preprocessed`
7. `eeg_trans.m` Large Laplacian transform.
8. `eeg_trans.m` time-frequency decomposition
    - complex morlet wavelets
    - freqs: 2 - 30 Hz (log spaced)
    - cycles: 1 - 8 (log spaced) (# of cycles: number of local minima/maxima in a wavelet)
    - wintime (ms) = winsize * 1000 / srate;
    - winsize (# of sample pnts) =(approx) srate * cycles(1) / frex(1)

## methods & references

The preprocessing procedure and methods used mainly follow [Makoto's Preprocessing Pipeline](https://sccn.ucsd.edu/wiki/Makoto's_preprocessing_pipeline).
`clean_artifacts` is an `EEGLAB` plugin, for more description, visit [Artifacts subspace reconstruction](http://sccn.ucsd.edu/eeglab/plugins/ASR.pdf)
`removeStrongArtifacts` uses wavelet enhanced ICA, MATLAB package `wICA` is [required](http://www.mat.ucm.es/~vmakarov/downloads.php). 


## source code

预处理相关的代码在此文件夹，代码更新及其他代码放在[这里](https://bitbucket.org/welkin31/eeganalysis)