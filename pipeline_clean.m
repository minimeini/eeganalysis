% clean label
filelist = getFilename(fullfile(wd_raw, '*.vhdr'));

if exist(fullfile(wd_prep, 'labelCleaned'), 'dir') ~= 7
    mkdir(fullfile(wd_prep, 'labelCleaned'));
end

for sub = 1 : numel(filelist)
    EEG = pop_loadbv(wd_raw, char(filelist(sub)));
    EEG = labelClean(EEG);
    pop_saveset(EEG, 'filename', char(filelist(sub)), ...
        'filepath', fullfile(wd_prep, 'labelCleaned'));
end
clear EEG

%%% clean exceptions
% expected case: U/V/W/X, h, H, ..., Z
[~, errTrials] = labelCheck(fullfile(wd_prep, 'labelCleaned'), ...
    '*.set', wd_ana, 'exceptions.mat');

if exist(fullfile(wd_prep, 'trialCleaned'), 'dir') == 0
    mkdir(fullfile(wd_prep, 'trialCleaned'));
end
load(fullfile(wd_ana, 'exceptions.mat'));

cleanTrials(errTrials, fullfile(wd_prep, 'labelCleaned'), ...
    '*.set', fullfile(wd_prep, 'trialCleaned'));
% labelCheck(fullfile(wd_prep, 'trialCleaned'), ...
%     '*.set', wd_ana, 'checkClean.mat');
%% delete trials whose homeDur < 1.5s
load(fullfile(wd_ana, 'durStatus.mat'));
if exist(fullfile(wd_prep, 'trialCleaned2'), 'dir') ~= 7
    mkdir(fullfile(wd_prep, 'trialCleaned2'));
end
cleanTrials2(homeDur, fullfile(wd_prep, 'trialCleaned'), ...
    '*.set', fullfile(wd_prep, 'trialCleaned2'));
% labelCheck(fullfile(wd_prep, 'trialCleaned2'), ...
%     '*.set', wd_ana, 'checkClean2.mat');
%% for imagination, delete trials whose balls were pressed
cleanTrials3(fullfile(wd_prep, 'trialCleaned2'), '*.set', ...
    fullfile(wd_prep, 'trialCleaned3'));
% labelCheck(fullfile(wd_prep, 'trialCleaned3'), ...
%     '*.set', wd_ana, 'checkClean3.mat');

% [filelist, ~] = getFilename(fullfile(wd_prep, 'trialCleaned3', '*.set'));
% load(fullfile(wd_prep, 'trialCleaned3', 'cleanTrials3Log.mat'));
% cnt = 0;
% for i = 1 : length(errLog)
%     if errLog(i).bad > 10
%         cnt = cnt + 1;
%         badFile = char(filelist(i));
%         delete(fullfile(wd_prep, 'trialCleaned3',[badFile(1:end-4) '.*']));
%     end
% end
% fprintf('reject %d subject(s).\n', cnt);
%% are all movements recorded?
errTrials = labelCheck2(fullfile(wd_prep, 'trialCleaned3'), '*.set', ...
    wd_ana, 'misLabel.mat');
cleanTrials4(errTrials, fullfile(wd_prep, 'trialCleaned3'), '*.set', ...
    fullfile(wd_prep, 'trialCleaned4'));
% errTrials = labelCheck2(fullfile(wd_prep, 'trialCleaned4'), '*.set', ...
%     wd_ana, 'misLabel_after.mat');
%% adjust label for epoching
dirname = 'trialCleaned4';
[filelist, totalFile] = getFilename(fullfile(wd_prep, dirname, '*.set'));

for sub = 1 : totalFile
    EEG = pop_loadset(char(filelist(sub)), fullfile(wd_prep, dirname));
    if strcmp(char(EEG.event(1).type), 'U'), continue; end
    disp('start point U is missed.');
    temp = EEG.event(1);
    temp.type = 'U';
    EEG.event = [temp EEG.event];
    pop_saveset(EEG, 'filepath', fullfile(wd_prep, dirname), ...
        'filename', char(filelist(sub)));
end