%%
clear all
wd = 'C:\Users\meini\OneDrive\eegAnalysis';
wd_fig = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd);

wd_prep = wd;
%% clean labels and trials
run(fullfile(wd_code, 'pipeline_clean.m'));

%% exclude resting period and concatenate the remained
exclude_rest(wd_prep, 'trialCleaned4', 1);

%% sin -> double; filt [1 100Hz]; load chanlocs; baseline correction
% passband edge = [1 100]Hz
% transition bandwidth = 1Hz
% cutoff frequency = [0.5 100.5]Hz
chanloc = readlocs(fullfile(wd_chanloc, 'actiCap64Channel.xyz'));
eeg_prep(wd_prep, 'norest', chanloc, [1 100], 1);

%% remove outliers
val = 90; cutpct = 92.5;
remove_outliers(wd_prep, 'bc_filt1-100_norest', val, cutpct);
%% clean line noise at 50Hz and 100Hz
noiseloc = [50 100];
bandwidth = 2;
operfolder = 'reve_bc_filt1-100_norest';
remove_linenoise(wd_prep, operfolder, noiseloc, bandwidth)
%% low-pass filter at 40Hz, cutoff frequency = 45Hz
foldern = 'cl50-100-w2_reve_bc_filt1-100_norest';
[filename,~,filelist] = ...
    getFilename(fullfile(wd_prep, foldern), '*.set');
savepath = fullfile(wd_prep, ['lp40_' foldern]);
checkPath(savepath);
for i = 1:numel(filelist)
    fname = char(filename(i)); fname = fname(1:end-4);
    eeg = pop_loadset(char(filelist(i)));
    eeg = pop_eegfiltnew(eeg, [], 40);    
    pop_saveset(eeg, 'filename', fname, 'filepath', savepath);
end
clear eeg
% renamed the output folder as `lp40_bc_filt1-100`
%% interpolate outliers
% [important] not necessary actually
% if interpolate, please do it in the most conservative manner
thres = 20; % soft edge
hardedge = 300;
hardthres = 24;

foldername = 'lp40_bc_filt1-100';
savepath = fullfile(wd_prep, ...
    ['intpo' num2str(thres) '-' num2str(hardthres) '_' foldername]);
checkPath(savepath);
[filename,~,filelist] = getFilename(fullfile(wd_prep, foldername), '*.set');

lowbound = ones(numel(filelist), 65) * (-hardedge);
upbound = ones(numel(filelist), 65) * hardedge;
varnames = [{'sub'}, arrayfun(@(x) ['chan' num2str(x)],1:64,...
    'UniformOutput',0)];

for i = 1:numel(filelist)
    fname = char(filename(i));
    fname = fname(1:end-4);
    
    subn = regexp(fname, '\_', 'split');
    subn = char(subn(1)); 
    subn = str2double(subn(4:end));
    lowbound(i,1) = subn;
    upbound(i,1) = subn;
    
    eeg = pop_loadset(char(filelist(i)));
    
    for j = 1:eeg.nbchan
        [~,~,low,up] = ...
            filloutliers(squeeze(eeg.data(j,:)), ...
            'nearest', 'quartiles', 'ThresholdFactor', thres);
        if abs(low)>hardedge || abs(up)>hardedge
            [eeg.data(j,:),~,lowbound(i,j+1),upbound(i,j+1)] = ...
                filloutliers(squeeze(eeg.data(j,:)), ...
                'nearest', 'quartiles', 'ThresholdFactor', thres);            
        end
        eeg.data(j,:) = ...
            filloutliers(squeeze(eeg.data(j,:)), ...
            'nearest', 'quartiles', 'ThresholdFactor', hardthres);
    end
    pop_saveset(eeg, 'filename', fname, 'filepath', savepath);
end

writeToCSV(lowbound, fullfile(wd_ana, ...
    ['lowbound_revo' num2str(thres) '_' foldername '.csv']), varnames);
writeToCSV(upbound, fullfile(wd_ana, ...
    ['upbound_revo' num2str(thres) '_' foldername '.csv']), varnames);

%% re-epoch the concatenated data
% epoch = [-1.4 0.4]s
% timeseg = zeros(numel(filelist), 2);
foldername = 'intpo20-24_lp40_bc_filt1-100';
[filename, ~, filelist] = ...
    getFilename(fullfile(wd_prep, foldername), '*.set');
savepath = fullfile(wd_prep, ['epoch_' foldername]);
for i = 1:numel(filelist)
    eegraw = pop_loadset(char(filelist(i)));
    fname = char(filename(i));
    fname = fname(1:end-4);
    
    for j = 1:numel(folderlist)
        savep = fullfile(savepath, char(folderlist(j)));
        checkPath(savep);
        eegepoch = pop_epoch(eegraw, folderlist(j), [-1.4 0.4]);
        baseline = calcBaseline(eegepoch, eegepoch.times, [-1400 -500]);
        eegepoch.data = eegepoch.data - baseline;
        pop_saveset(eegepoch, 'filename', fname, ...
            'filepath', savep);
    end
end

%% reject epochs
% aim at 5% ~ 7.5% rejection rate if rejection is needed
% 1. reject epochs that contains value that exceeds `valEdge`
% 2. for every channel, find epochs that
%    `SD > max(prctile(chanstd, softEdge), hardEdge)`
% 3. for each epoch, counts the number of channels where they are marked
%    as bad at step 2
% 4. reject epochs whose the total number of channels marked as 
%    bad > prctile(chancnt,cutpct), 
%    where the rejection rate `cutpct` is pre-defined.
valEdge = 500;
softEdge = 92.5;
hardEdge = 150;
softCutEdge = 92.5;
hardCutEdge = 10;

foldername = 'epoch_intpo20-24_lp40_bc_filt1-100_norest';
savepath = fullfile(wd_prep, ['reve_' foldername]);
[~,~,foldername] = getFilename(fullfile(wd_prep,foldername));
[filename,~,filelist] = getFilename(foldername,'*.set');

% savepathAna = fullfile(wd_ana, ['epochStdStat_' foldername]);
% varnames = {'std90pct', 'std92.5pct', 'std95pct', ...
%     'std150', 'std200', 'val500'};
% histpct = zeros(3, numel(filelist), 64);
% cutpctStat = zeros(numel(filelist), 3);
% varnames = {'pct90', 'pct92.5', 'pct95'};
for i = 1:numel(filelist)    
    fname = char(filename(i));
    fname = fname(1:end-4);
    taskn = regexp(char(filelist(i)),'\\','split');
    taskn = char(taskn(end-1));
    savep = fullfile(savepath, taskn);
    checkPath(savep);
    
    eeg = pop_loadset(filelist(i));
    
    [~,rejIdx] = eegthresh(eeg.data,eeg.pnts,1:eeg.nbchan,...
        -valEdge,valEdge,[eeg.xmin eeg.xmax],eeg.xmin,eeg.xmax);
    if ~isempty(rejIdx)
        eeg = pop_select(eeg, 'notrial', rejIdx);
    end
    
    meaneeg = repmat(mean(eeg.data,2),1,eeg.pnts,1);
    stdeeg = squeeze(sum((eeg.data-meaneeg).^2, 2) / (eeg.pnts-1));
    stdbound = repmat(max(prctile(stdeeg, softEdge, 2),...
        hardEdge*ones(64,1)), 1, eeg.trials);
    chancnt = sum(stdeeg > stdbound, 1);
    epochRemoved = chancnt > max(prctile(chancnt,softCutEdge), hardCutEdge);
    
    eeg =pop_select(eeg, 'notrial', find(epochRemoved));
    pop_saveset(eeg, 'filename', fname, 'filepath', savep);
    
%     cutpctStat(i,:) = [prctile(chancnt,90) prctile(chancnt,92.5) prctile(chancnt,95)];
    
%     stdstat = [prctile(stdeeg,90,2) prctile(stdeeg,92.5,2) ...
%         prctile(stdeeg,95,2) sum(stdeeg>150,2) sum(stdeeg>200,2) ...
%         repmat(numel(rejIdx),64,1)];
%     savep = fullfile(savepathAna, taskn);
%     checkPath(savep);
%     writeToCSV(stdstat, fullfile(savep, ...
%         ['epochStdStat_' taskn '_' fname '.csv']), varnames);
    
    % use this to determine the value of hardEdge
    % across all channels and subjects
%     histpct(1,i,:) = squeeze(prctile(stdeeg,90,2));
%     histpct(2,i,:) = squeeze(prctile(stdeeg,92.5,2));
%     histpct(3,i,:) = squeeze(prctile(stdeeg,95,2));
    
%     
end

writeToCSV(cutpctStat, fullfile(savepathAna, 'cutpctStat.csv'),varnames);

% f = figure;
% subplot(3,1,1);
% histogram(squeeze(cutpctStat(:,1)), 'BinWidth', 5);
% title('cutpct = 90');
% subplot(3,1,2);
% histogram(squeeze(cutpctStat(:,2)), 'BinWidth', 5);
% title('cutpct = 92.5');
% subplot(3,1,3);
% histogram(squeeze(cutpctStat(:,3)), 'BinWidth', 5);
% title('cutpct = 95');
% suptitle('histogram of cutpct to define hardEdge');

%% detect, remove bad channels + CAR
foldern = 'reve_epoch_intpo20-24_lp40_bc_filt1-100_norest';
tasklab = getFilename(fullfile(wd_prep, foldern));
filename = getFilename(fullfile(wd_prep, foldern, 'U'), '*.set');

badchans = [41 46 17 22 28 32 ... % channels in the outermost ring
    37 7 42 ... % flat channels which lost contact
    33 54]; % crazy channels with high amplitudes

for t = 1:numel(tasklab)
savepath = fullfile(wd_prep, 'car_rejbadchan', char(tasklab(t)));
checkPath(savepath);

for i = 1:numel(filename)
    subn = regexp(char(filename(i)), '\_', 'split');
    subn = char(subn(1));
    subn = str2double(subn(4:end));
    fname = char(filename(i));
    fname = fname(1:end-4);
        
    eegorig = pop_loadset(fullfile(wd_prep, foldern, ...
        char(tasklab(t)), char(filename(i))));
%     eegnew = pop_select(eegorig, 'nochannel', badchans);
    eeg_ft = eeg_re_reference(eegorig, 'CAR', badchans);
    eegnew = trans_eegdata(eeg_ft, 'eeglab');
    
    pop_saveset(eegnew, 'filename', fname, 'filepath', savepath);
end
end
%% ICA decomposition
foldername = 'car_rejbadchan';
savepath = fullfile(wd_prep,['ica_' foldername]);
[~,~,foldername] = getFilename(fullfile(wd_prep,foldername));
[filename,~,filelist] = getFilename(foldername,'*.set');
for i = 1:numel(filelist)
    fname = char(filename(i));
    fname = fname(1:end-4);
    taskn = regexp(char(filelist(i)),'\\','split');
    taskn = char(taskn(end-1));
    savep = fullfile(savepath, taskn);
    checkPath(savep);
    
    eeg = pop_loadset(char(filelist(i)));
    eeg = rankDeficientICA(eeg,'runica','icarank', 1);
    
    pop_saveset(eeg,'filename',fname,'filepath',savep);
end

%% reject ICs
[~,~,rejstatpath] = getFilename(fullfile(wd_prep, 'rejStat'));
foldern = 'ica_car_rejbadchan';
[tasklab,~,foldername] = getFilename(fullfile(wd_prep, foldern));
filename = getFilename(char(foldername(1)), '*.set');
savepath = fullfile(wd_prep, ['rej' foldern]);

pct_rej = 0.12;

for t = 1:numel(foldername)
    %%
    rejstat = csvread(char(rejstatpath(t)),1,0);
    savep = fullfile(savepath, char(tasklab(t)));
    checkPath(savep);
    
    for i = 1:numel(filename)
        filen = char(filename(i)); filen = filen(1:end-4);
        subn = regexp(filen,'\_','split');
        fname = char(subn(1));
        subn = str2double(fname(4:end)); 
        
        icrej = find(rejstat(rejstat(:,1)==subn,2:end)>0, ...
            round(pct_rej * (size(rejstat,2)-1)), 'first');
        eeg = pop_loadset(fullfile(char(foldername(t)), ...
            char(filename(i))));
        eegsub = pop_subcomp(eeg, icrej, 0, 0);
        
        pop_saveset(eegsub,'filename',filen, 'filepath',savep);
    end
end

%% hilbert transform for data filtered to 8~12 Hz
% baseline correction and common averaging reference before this step
foldern = 'rejica_car_rejbadchan';
[tasklab,~,foldername] = getFilename(fullfile(wd_prep, foldern));
filename = getFilename(char(foldername(1)), '*.set');

savepath1 = fullfile(wd_prep, 'preprocessed_realeeg');
savepath2 = fullfile(wd_prep, 'preprocessed_hilbeeg');

for t = 1:numel(tasklab)
    %%
    savep1 = fullfile(savepath1, tasklab{t});
    checkPath(savep1);
    savep2 = fullfile(savepath2, tasklab{t});
    checkPath(savep2);
    
    for i = 1:numel(filename)
        fname = regexp(filename{i},'\_','split');
        fname = fname{1};
        
        eeg = pop_loadset(fullfile(foldername{t},filename{i}));
        % narrow band-pass filter
        eeg = pop_eegfiltnew(eeg, 8, 12); % cutoff = [7 13], tran bw = 2
        % surface laplacian
        eeg_ft = eeg_re_reference(eeg, 'SL');
        eegnew = trans_eegdata(eeg_ft, 'eeglab');
        
        eeg.data = eegnew.data;
        pop_saveset(eeg,'filename',filename{i},'filepath',savep1);
        
        % hilbert transform
        data = zeros(size(eeg.data));
        for c = 1:size(eeg.data,1)
            data(c,:,:) = cell2mat(arrayfun(@(t) ...
                hilbert(squeeze(eeg.data(c,:,t)))', ...
                1:size(eeg.data,3),'UniformOutput',0));
        end
        save(fullfile(savep2, [fname '.mat']), 'data');
        
    end
end

