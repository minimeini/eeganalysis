function [idx, latency] = findIndex(struct, label, startIdx, varargin)
    p = inputParser;
    addRequired(p, 'struct', @isstruct);
    addRequired(p, 'label', @ischar);
    addRequired(p, 'startIdx', @isnumeric);
    addParameter(p, 'cleanNull', false, @islogical);
    parse(p, struct, label, startIdx, varargin{:});
    
    struct = p.Results.struct;
    label = p.Results.label;
    startIdx = p.Results.startIdx;
    
    idx = zeros(1, length(startIdx));
    latency = zeros(1, length(startIdx));
    cnt = 1;
    for i = 1 : length(struct)
        if cnt == length(startIdx)
            if i > startIdx(cnt - 1) && strcmp(struct(i).type, label)
                idx(cnt) = i;
                latency(cnt) = struct(i).latency;
            end
        else
            if strcmp(struct(i).type, label)
                if i > startIdx(cnt) && i < startIdx(cnt+1)
                    idx(cnt) = i;
                    latency(cnt) = struct(i).latency;
                end
            end
        
            if i == startIdx(cnt + 1)
                cnt = cnt + 1;
            end
        end
    end
    
    if (nargin > 3) && ~isempty(p.Results.cleanNull)
        if p.Results.cleanNull
            for cnt = 0 : length(startIdx) - 1
                if idx(length(startIdx) - cnt) == 0
                    idx(length(startIdx) - cnt) = []; 
                    latency(length(startIdx) - cnt) = [];
                end
            end
        end
    end
    
end