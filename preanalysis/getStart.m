function [startStat, startIdx, startTime] = getStart(EEG, trialLabel, cleanNull)
    startIdx = zeros(1, 300);
    startTime = zeros(1, 300);
    startType = zeros(1, 300);
    cnt = 1;
    for i = 1 : length(EEG.event)
        if ismember(EEG.event(i).type, trialLabel)
            startIdx(cnt) = i;
            startTime(cnt) = EEG.event(i).latency;
            startType(cnt) = EEG.event(i).type;
            cnt = cnt + 1;
        end
    end
    
    if cleanNull
        for i = 0 : 299
            if startIdx(300 - i) == 0
                startIdx(300 - i) = [];
                startTime(300 - i) = [];
                startType(300 - i) = [];
            end
        end
    end
    
    startStat.idx = startIdx;
    startStat.time  = startTime;
    startStat.type = startType;
    startStat.total = length(startIdx);
    startStat.nona = sum(startIdx ~= 0);
end