%% 
% epochfolder = 'norest_nobasecor_epoch';
% concatfolder = 'norest_nobasecor'; 
filtfolder = 'filt1-100_norest'; % nobasecor, but filtered
corfiltfold = 'filt1-100_bc_norest';
filtcorfold = 'bc_filt1-100_norest';
%%
subn = 1; % that is subject 10
filename = ...
    getFilename(fullfile(wd_prep,filtfolder),'*.set');
fname = regexp(char(filename(subn)), '\_', 'split');
fname = char(fname(1));

% eeg = pop_loadset(fullfile(wd_prep, concatfolder, char(filename(1))));
% eegepoch = pop_loadset(fullfile(wd_prep, epochfolder, char(filename(1))));
eegfilt = pop_loadset(fullfile(wd_prep, filtfolder, char(filename(1))));
eegcorfilt = pop_loadset(fullfile(wd_prep, corfiltfold, char(filename(1))));
eegfiltcor = pop_loadset(fullfile(wd_prep, filtcorfold, char(filename(1))));
%% visualize single-channel continuous time-series
% find if there is any nonstationary in the EEG data without baseline
% correction

savepath = fullfile(wd_vis, ['ts_' concatfolder]);
savep = fullfile(savepath, fname);
checkPath(savep);
for j = 1:eeg.nbchan
    eegtemp = squeeze(eeg.data(j,:));
        
    f = figure;
    f.Visible = 'off';
    plot(eeg.times, eegtemp);
    line([eeg.times(1) eeg.times(end)], ...
        [mean(eegtemp) mean(eegtemp)], ...
        'Color', 'red');
    line([eeg.times(1) eeg.times(end)], ...
        [mean(eegtemp)-3*std(eegtemp) ...
        mean(eegtemp)-3*std(eegtemp)], ...
        'Color', 'red');
    line([eeg.times(1) eeg.times(end)], ...
        [mean(eegtemp)+3*std(eegtemp) ...
        mean(eegtemp)+3*std(eegtemp)], ...
        'Color', 'red');  
    title([fname '-chan' num2str(j)]);
    xlabel(replace(foldername, '_', '-'));
       
    print(f, fullfile(savep, ...
        [fname '_chan' num2str(j) '.png']), '-dpng');
       
    close all
    delete(f);
    delete(gcf);
end

%% baseline correction
% scheme 1. mean([-1.5 -0.5])
% scheme 2. median([-1.5 -0.5])
% scheme 3. mean([-1.5 -0.5] with <5% prc and >95% prc excluded)
times = eegepoch.times;
base1 = repmat(mean(eegepoch.data(:,times>=-1500 & times<=-500,:),2), ...
    1,numel(times),1);
base2 = repmat(median(eegepoch.data(:,times>=-1500 & times<=-500,:),2), ...
    1,numel(times),1);
base3 = zeros(size(eegepoch.data));
for i = 1:eegepoch.nbchan
    for j = 1:eegepoch.trials
        eegtemp = squeeze(eegepoch.data(i,times>=-1500 & times<=-500,j));
        eegtemp = eegtemp(eegtemp>prctile(eegtemp,5) & ...
            eegtemp<prctile(eegtemp,95));
        base3(i,:,j) = mean(eegtemp);
    end
end

eegcor1 = eegepoch.data - base1;
eegcor2 = eegepoch.data - base2;
eegcor3 = eegepoch.data - base3;

%% for every channel, plot the TS per epoch
% line 1: no basecor
% line 2: basecor in scheme 1
% line 3: basecor in scheme 2
% line 4: basecor in scheme 3
savepath = fullfile(wd_vis, 'sub10_comp_basecor');
for i = 1:eegepoch.nbchan
    savep = fullfile(savepath, ['chan' num2str(i)]);
    checkPath(savep);
    for j = 1:eegepoch.trials
        f = figure;
        f.Visible = 'off';

        plot(times, squeeze(eegepoch.data(i,1:1000,j)), ...
            'Color', 'r');        
        line(times, squeeze(eegcor1(i,1:1000,j)), 'Color', 'g');
        line(times, squeeze(eegcor2(i,1:1000,j)), 'Color', 'b');
        line(times, squeeze(eegcor3(i,1:1000,j)), 'Color', 'y');

        
        title(['chan' num2str(i) '-epoch' num2str(j)]);
        xlabel('comparison of baseline correction scheme');
        
        print(f, fullfile(savep, ...
            ['sub10_chan' num2str(i) '_epoch' num2str(j) '.png']), ...
            '-dpng');
        delete(f);
        delete(gcf);
    end
end

%%
ntime = 1000;
meanraw = squeeze(mean(eegepoch.data(:,1:ntime,:), 2));
meancor1 = squeeze(mean(eegcor1(:,1:ntime,:), 2));
meancor2 = squeeze(mean(eegcor2(:,1:ntime,:), 2));
meancor3 = squeeze(mean(eegcor3(:,1:ntime,:), 2));

savepath = fullfile(wd_vis, 'sub10_comp_basecor', 'epochmean');
checkPath(savepath);
for i = 1:eegepoch.nbchan
    f = figure;
    f.Visible = 'off';
    plot(1:eegepoch.trials, squeeze(meanraw(i,:)), 'Color', 'r');
    line(1:eegepoch.trials, squeeze(meancor3(i,:)), 'Color', 'y');
    line(1:eegepoch.trials, squeeze(meancor2(i,:)), 'Color', 'b');
    line(1:eegepoch.trials, squeeze(meancor1(i,:)), 'Color', 'g');
    title(['sub10-chan' num2str(i)]);
    xlabel('epoch mean');
    
    print(f, fullfile(savepath, ['sub10_chan' num2str(i) '.png']), '-dpng');
    close all
    delete(f);
    delete(gcf);
end
%% compare the histogram and periodogram on the whole continuous data
% 1. before any baseline correction or bandpass filter 
%    `eeg`
% 2. baseline correction using scheme 3 without bandpass filter 
%    `eegcor3concat`
% 4. baseline correction using scheme 3 and filter to [1 100]Hz
%    `eegfiltcor`
% 3. filter to [1 100]Hz without baseline correction 
%    `eegfilt`
savepath = fullfile(wd_vis, 'sub10_comp_basecor', 'periodogram');
checkPath(savepath);
eegcor3concat = reshape(eegcor3, 64, []);
srate = eeg.srate;
for i = 1:eeg.nbchan
    f = figure;
    f.Visible = 'off';
    subplot(2,2,1);
    periodogram(squeeze(eeg.data(i,:)), [], [], srate);
    title('raw data');
    
    subplot(2,2,2);
    periodogram(squeeze(eegcor3concat(i,:)), [], [], srate);
    title('basecor');
    
    subplot(2,2,3);
    periodogram(squeeze(eegfilt.data(i,:)), [], [], srate);
    title('filtered');
    
    subplot(2,2,4);
    periodogram(squeeze(eegfiltcor.data(i,:)), [], [], srate);
    title('filtered and basecor');
    suptitle(['sub10-chan' num2str(i)]);    
    
    print(f, fullfile(savepath, ...
        ['sub10_chan' num2str(i) '.png']), '-dpng');
    close all
    delete(f);
    delete(gcf);
end


%% compare baseline correction with high-pass filter 
% where bandpass edge = [1 100Hz], cutoff frequency = [0.5 100.5]Hz
% the the time series of epoch mean
% 1. only filter: `eegfilt`
% 2. baseline correction first then filter: `eegcorfilt`
% 3. filter first then baseline correction: `eegfiltcor`

eegfiltepoch = pop_epoch(eegfilt, {'U','V','W','X'}, [-1.5 0.5]);
eegcorfiltepoch = pop_epoch(eegcorfilt, {'U','V','W','X'}, [-1.5 0.5]);
eegfiltcorepoch = pop_epoch(eegfiltcor, {'U','V','W','X'}, [-1.5 0.5]);

% ntime = numel(eegfiltepoch.times);

% meanraw = squeeze(mean(eegepoch.data(:,1:ntime,:),2));
meanfilt = squeeze(mean(eegfiltepoch.data,2));
meancorfilt = squeeze(mean(eegcorfiltepoch.data,2));
meanfiltcor = squeeze(mean(eegfiltcorepoch.data,2));
% meancor3 = squeeze(mean(eegcor3(:,1:ntime,:),2));

savepath = fullfile(wd_vis, 'sub10_comp_basecor', 'epochmean_f&bcf&fbc');
savepath2 = fullfile(wd_vis, 'sub10_comp_basecor', 'epochmean2_f&bcf&fbc');
checkPath(savepath);
checkPath(savepath2);
for i = 1:64
    f = figure;
    f.Visible = 'off';
%     plot(1:eegfiltepoch.trials, squeeze(meanfilt(i,:)), 'Color', 'r')
%     line(1:eegfiltepoch.trials, squeeze(meancorfilt(i,:)), 'Color', 'g');
%     line(1:eegfiltepoch.trials, squeeze(meanfiltcor(i,:)), 'Color', 'b');
%     title(['sub10-chan' num2str(i)]);
%     xlabel('r=filt, g=bc+filt, b=filt+bc');
%     
%     print(f, fullfile(savepath, ['sub10_chan' num2str(i) '.png']), '-dpng');
%     close all
%     delete(f);
%     delete(gcf);
    
    subplot(3,1,1);
    plot(1:eegfiltepoch.trials, squeeze(meanfilt(i,:)));
    title('filter to [1 100]Hz');
    subplot(3,1,2);
    plot(1:eegfiltepoch.trials, squeeze(meancorfilt(i,:)));
    title('baseline correction + filter to [1 100]Hz');
    subplot(3,1,3);
    plot(1:eegfiltepoch.trials, squeeze(meanfiltcor(i,:)));
    title('filter to [1 100]Hz + baseline correction');
    suptitle(['epoch mean comparison - sub10 - chan' num2str(i)]);
    
    print(f, fullfile(savepath2, ['sub10_chan' num2str(i) '.png']), '-dpng');
    close all
    delete(f);
    delete(gcf);
    
end

%% conclusion. use scheme 3, never try to use scheme 2