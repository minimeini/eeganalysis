function [startStat, errTrials] = labelCheck(filepath, format, savepath, savename)
p = inputParser;
addRequired(p, 'filepath', @ischar);
addRequired(p, 'format', ...
    @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savepath', @ischar);
addRequired(p, 'savename', @ischar);

parse(p, filepath, format, savepath, savename);
filepath = p.Results.filepath;
format = p.Results.format;

filelist = dir(fullfile(filepath, format));
filelist = {filelist.name};
totalFile = length(filelist);

for sub = 1 : totalFile
    [startStat(sub), errTrials(sub)] ...
        = labelCount(filepath, char(filelist(sub)), format);
end

save(fullfile(p.Results.savepath, p.Results.savename), ...
    'startStat', 'errTrials', '-v7.3');
end

function [startStat, errTrials] = labelCount(filepath, filename, format)
    p = inputParser;
    addRequired(p, 'filepath', @ischar);
    addRequired(p, 'filename', @ischar);
    addRequired(p, 'format', ...
        @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
    parse(p, filepath, filename, format);
%-- parameter parsing ---------%

    trialStart = {'U', 'V', 'W', 'X'};
    trialEnd = 'Z';
    homePressed = 'h';
    homeReleased = 'H';

    if strcmp(p.Results.format, '*.vhdr')
        EEG = pop_loadbv(p.Results.filepath, p.Results.filename);
    else
        EEG = pop_loadset(p.Results.filename, p.Results.filepath);
    end
    
    [startStat, startIdx, ~] = getStart(EEG, trialStart, false);
    finIdx = findIndex(EEG.event, trialEnd, startIdx);
    presIdx = findIndex(EEG.event, homePressed, startIdx);
    relIdx = findIndex(EEG.event, homeReleased, startIdx);
    errTrials = findException(startIdx, finIdx, presIdx, relIdx);
    errTrials.end = length(EEG.event);
end

function [errTrials] = findException(startIdx, finIdx, presIdx, relIdx)
    errLog = 0;
    errTrial = -1;
    errIdx = -1;
    for i = 1 : 300
        if finIdx(i) == 0 || presIdx(i) == 0 || relIdx(i) == 0
            if errLog == 0
                errTrial = i;
                errIdx = startIdx(i);
            else
                errTrial = [errTrial i];
                errIdx = [errIdx startIdx(i)];
            end
            errLog = errLog + 1;
        end
    end
    
    errTotal = 300;
    l = length(errTrial);
    for i = 0 : l - 1
        if errIdx(l - i) == 0
            errIdx(l - i) = [];
            errTrial(l - i) = [];
            errTotal = errTotal - 1;
        end
    end

    if sum(startIdx ~= 0) == errTrial(end)
        if length(errTrial) == 1
            errTrial = -1;
            errIdx = -1;
        else
            errTrial(end) = [];
            errIdx(end) = [];
        end
    end
    
    errTrials.trial = errTrial;
    errTrials.idx = errIdx;
    errTrials.total = errTotal;
end