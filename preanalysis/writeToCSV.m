function [] = writeToCSV(data, filename, varargin)
p = inputParser;
addRequired(p, 'data', @isnumeric);
addRequired(p, 'filename', @ischar);
addOptional(p, 'header', [], @iscell); % optional
parse(p, data, filename, varargin{:});
filename = p.Results.filename;
header = p.Results.header;

fid = fopen(fullfile(filename),'w'); 
if ~isempty(header) %write header to file
    commaHeader = [header;repmat({','},1,numel(header))]; % insert commas
    commaHeader = commaHeader(:)';
    textHeader = cell2mat(commaHeader);
    textHeader = textHeader(1:end-1);
    fprintf(fid,'%s\n',textHeader);
end
fclose(fid);
%write data to end of file
dlmwrite(fullfile(filename), data, '-append');

end