function [finStat] = getEnd(EEG, startStat)
finIdx = zeros(1, length(startStat.idx));
finTime = zeros(1, length(startStat.idx));
finType = zeros(1, length(startStat.idx));

for cnt = 1 : length(startStat.idx)
    if cnt <  length(startStat.idx)
        finIdx(cnt) = startStat.idx(cnt + 1) - 2;
    else
        finIdx(cnt) = length(EEG.event) - 1;
    end
    finTime(cnt) = EEG.event(finIdx(cnt)).latency;
    finType(cnt) = EEG.event(finIdx(cnt)).type;
end

finStat.idx = finIdx;
finStat.time = finTime;
finStat.type = finType;
end