%%
foldername = 'rev_bc_filt1-100_norest';
[filename,~,filelist] = ...
    getFilename(fullfile(wd_prep, foldername), '*.set');
filecnt = numel(filelist);

%% visualize periodogram
for i = 1:filecnt
    fname = char(filename(i)); fname = fname(1:end-4);
    subn = regexp(fname, '\_', 'split');
    subn = char(subn(1));
    savepath = fullfile(wd_vis, ['periodogram_' foldername], subn);
    checkPath(savepath);
    
    eeg = pop_loadset(char(filelist(i)));
    eegdata = eeg.data;
    
    for j = 1:eeg.nbchan
        f = figure;
        f.Visible = 'off';
        periodogram(squeeze(eegdata(j,:)),[],[],eeg.srate);
        title([subn '-chan' num2str(j)]);
        print(f, fullfile(savepath, ...
            [subn '_chan' num2str(j)]), '-dpng');
    
        delete(f)
        delete(gcf)
    end

end

%% bandpower analysis
varnames = {'chan', 'bp1_w1', 'bp50_w2', 'bp50_w10', 'bp100_w1'};
bpstat = zeros(filecnt, 64, 5);
savepath = fullfile(wd_ana, ['bandpower_' foldername]);
checkPath(savepath);

subn = zeros(1,filecnt);
for i = 1:filecnt
    filen = regexp(char(filename(i)), '\_', 'split');
    filen = char(filen(1));
    subn(i) = str2double(filen(4:end));
    eeg = pop_loadset(char(filelist(i)));
    
    bpstat(i,:,1) = 1:64;
    for j = 1:eeg.nbchan
        bpstat(i,j,2) = bandpower(squeeze(eeg.data(j,:)), ...
            eeg.srate, [0.5 1.5]);
        bpstat(i,j,3) = bandpower(squeeze(eeg.data(j,:)), ...
            eeg.srate, [49 51]);
        bpstat(i,j,4) = bandpower(squeeze(eeg.data(j,:)), ...
            eeg.srate, [45 55]);
        bpstat(i,j,5) = bandpower(squeeze(eeg.data(j,:)), ...
            eeg.srate, [99.5 100.5]);
    end
    writeToCSV(squeeze(bpstat(i,:,:)), ...
        fullfile(savepath, ['bpstat_' filen '.csv']), ...
        varnames);
end

%% summary of bandpower
groundvar = {'sub', ...
    'prc1_bp1_w1', 'prc5_bp1_w1', 'aver_bp1_w1', ...
    'prc95_bp1_w1', 'prc99_bp1_w1', ...
    'prc1_bp50_w2', 'prc5_bp50_w2', 'aver_bp50_w2', ...
    'prc95_bp50_w2', 'prc99_bp50_w2'};
groundbpstat = zeros(filecnt, numel(groundvar));
groundbpstat(:,1) = subn;

groundbpstat(:,2) = prctile(squeeze(bpstat(:,:,2)), 1, 2);
groundbpstat(:,3) = prctile(squeeze(bpstat(:,:,2)), 5, 2);
groundbpstat(:,4) = mean(squeeze(bpstat(:,:,2)), 2);
groundbpstat(:,5) = prctile(squeeze(bpstat(:,:,2)), 95, 2);
groundbpstat(:,6) = prctile(squeeze(bpstat(:,:,2)), 99, 2);

groundbpstat(:,7) = prctile(squeeze(bpstat(:,:,3)), 1, 2);
groundbpstat(:,8) = prctile(squeeze(bpstat(:,:,3)), 5, 2);
groundbpstat(:,9) = mean(squeeze(bpstat(:,:,3)), 2);
groundbpstat(:,10) = prctile(squeeze(bpstat(:,:,3)), 95, 2);
groundbpstat(:,11) = prctile(squeeze(bpstat(:,:,3)), 99, 2);

writeToCSV(groundbpstat, ...
    fullfile(wd_ana, ['groundbpstat_' foldername '.csv']), ...
    groundvar);

%% find bad channels which
% if bp50_w2 > max(prctile(bp50_w2,95), 300)
%
% badchan.crazychan (> max)
% badchan.highcut = max(prctile(bp50_w2,95), 100)
if exist('badchan', 'var'), clear badchan; end
badchanidx =zeros(filecnt, 1+64);
for i = 1:filecnt
    filen = regexp(char(filename(i)), '\_', 'split');
    filen = char(filen(1));    
    bpstatsub = csvread(fullfile(wd_ana,['bandpower_' foldername], ...
        ['bpstat_' filen '.csv']), 1, 1);
    % bpstatsub = 64 * [bp1_w1, bp50_w2, bp50_w10, bp100_w1]
    
    badchan(i).subnum = filen;
    badchan(i).highcut = max(prctile(squeeze(bpstatsub(:,2)),95), 300);
    
    badchanidx(i,1) = str2double(filen(4:end));
    badchanidx(i,2:end) = squeeze(bpstatsub(:,2)) > badchan(i).highcut;
    
    badchan(i).crazychan = find(badchanidx(i,2:end));
    badchan(i).crazypower = bpstatsub(badchan(i).crazychan, 2);
    
end
badchan = struct2table(badchan);
% writetable(badchan, fullfile(wd_ana, ...
%     ['badchan_bandpower_' foldername '.csv']));
writeToCSV(badchanidx, fullfile(wd_ana, ...
    ['chanRemoved_bp_' foldername '.csv']), ...
    [{'sub'} arrayfun(@(x) ['chan' num2str(x)],1:64,'UniformOutput',0)]);

%% single-channel continuous-time-domain plot for dead/crazy channels
savepath_crazy = fullfile(wd_vis, ['crazychan_' foldername]);
checkPath(savepath_crazy);

for i = 1:filecnt
    eeg = pop_loadset(char(filelist(i)));
    crazychan = cell2mat(badchan.crazychan(i))';
    
    for j = 1:eeg.nbchan
        if ismember(j,crazychan)            
            eegtemp = squeeze(eeg.data(j,:));
            
            f = figure('units', 'normalized', ...
                'outerposition', [0 0 1 1], ...
                'Visible', 'off');
            plot(eeg.times, eegtemp);
            line([eeg.times(1) eeg.times(end)], ...
                [mean(eegtemp) mean(eegtemp)], ...
                'Color', 'red');
            line([eeg.times(1) eeg.times(end)], ...
                [mean(eegtemp)-3*std(eegtemp) ...
                mean(eegtemp)-3*std(eegtemp)], ...
                'Color', 'red');
            line([eeg.times(1) eeg.times(end)], ...
                [mean(eegtemp)+3*std(eegtemp) ...
                mean(eegtemp)+3*std(eegtemp)], ...
                'Color', 'red');
            
            title(['sub' num2str(subn(i)) ...
                '-chan' num2str(j)]);
            
            print(f, fullfile(savepath_crazy, ...
                ['sub' num2str(subn(i)) ...
                '_chan' num2str(j) '_.png']), '-dpng');
            close all
            delete(f);
            delete(gcf);  
        end    
    end
end