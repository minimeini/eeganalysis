function [errTrials] = labelCheck2(filepath, format, savepath, savename)
p = inputParser;
addRequired(p, 'filepath', @ischar);
addRequired(p, 'format', ...
    @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savepath', @ischar);
addRequired(p, 'savename', @ischar);

parse(p, filepath, format, savepath, savename);
filepath = p.Results.filepath;
format = p.Results.format;

filelist = dir(fullfile(filepath, format));
filelist = {filelist.name};
totalFile = length(filelist);

for sub = 1 : totalFile
    if strcmp(p.Results.format, '*.vhdr')
        EEG = pop_loadbv(filepath, char(filelist(sub)));
    else
        EEG = pop_loadset(char(filelist(sub)), filepath);
    end
    [~, startIdx, ~] = getStart(EEG, {'U', 'V', 'W', 'X'}, true);
    diffIdx = diff(startIdx);
    
    errTrial = 0;
    errIdx = 0;
    for i = 1 : length(diffIdx)
        if (diffIdx(i) == 14  || diffIdx(i) == 6), continue; end
        if errTrial(1) == 0
            errTrial = i;
            errIdx = startIdx(i);
        else
            errTrial = [errTrial i];
            errIdx = [errIdx startIdx(i)];
        end
    end
    
    errTrials(sub).idx = errIdx;
    errTrials(sub).trial = errTrial;
    errTrials(sub).total = length(startIdx);
    if errTrial(1) == 0
        errTrials(sub).bad = 0;
    else
        errTrials(sub).bad = length(errIdx);
    end
end

save(fullfile(p.Results.savepath, p.Results.savename), 'errTrials');
end