%% partition
% re: right execution; ri: right imagination;
% le: left execution; li: left imagination;
partitionAll(fullfile(wd_prep, 'trialCleaned4'), '*.set', ...
    fullfile(wd_prep, 'partitioned'));
%% analysis latencies for random buttons
[~, ~, dur] = checkDuration(fullfile(wd_prep, 'partitioned'));

folderlist = dir(fullfile(fullfile(wd_prep, 'partitioned')));
folderlist = {folderlist.name};
folderlist = folderlist(3 : end);
disp(folderlist);
totalFile = length(dir(fullfile(wd_prep, 'partitioned', ...
    char(folderlist(1)), '*.set')));

if exist(fullfile(wd_vis, 'presStat'), 'dir') ~= 7
    mkdir(wd_vis, 'presStat')
end

for sub = 1 : totalFile
    h = figure;
    set(h, 'Visible', 'off');
    hold on
    plot(1 : length(dur(sub).le), dur(sub).le);
    plot(1 : length(dur(sub).li), dur(sub).li);
    plot(1 : length(dur(sub).re), dur(sub).re);
    plot(1 : length(dur(sub).ri), dur(sub).ri);
    hold off
    title(['Duration - sub' num2str(sub)]);
    ylabel('milliseconds');
    legend('le', 'li', 're', 'ri');
    saveas(h, fullfile(wd_vis, 'presStat', ['dur_sub' num2str(sub) '.png']));
end