function [min_, quar1, mean_, quar3, max_, all] = getPercentile(vector)
    min_ = min(vector);
    quar1 = prctile(vector, 0.25);
    mean_ = mean(vector);
    quar3 = prctile(vector, 0.75);
    max_ = max(vector);
    all = [min_ quar1 mean_ quar3 max_];
end