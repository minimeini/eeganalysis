%% for each subject, conduct single-channel epoch based outlier analysis
val = [90 95 99 99.5];
cutpct = 92.5;
varnames = {'sub', 'cutoff90', 'ecnt90', ...
    'ecnt90U', 'ecnt90W', 'ecnt90V', 'ecnt90X', ...
    'cutoff95', 'ecnt95', ...
    'ecnt95U', 'ecnt95W', 'ecnt95V', 'ecnt95X', ...
    'cutoff99', 'ecnt99', ...
    'ecnt99U', 'ecnt99W', 'ecnt99V', 'ecnt99X', ...
    'cutoff99_5', 'ecnt99_5', ...
    'ecnt99_5U', 'ecnt99_5W', 'ecnt99_5V', 'ecnt99_5X'};
outlierStat = zeros(numel(filelist), numel(varnames));

varnames2 = [{'sub'} ...
    arrayfun(@(x) ['chan' num2str(x)],1:64,'UniformOutput',0)];
outlierDetail = zeros(numel(filelist), numel(varnames2), numel(val));

[~,~,filelist] = ...
    getFilename(fullfile(wd_prep,'mergeNobasecorRaw'), '*.set');
for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    fname = regexp(char(fname(end)), '\_', 'split');
    fname = char(fname(1));
    fname = str2double(fname(4:end));
    
    eeg = pop_loadset(char(filelist(i)));
    eegcat = reshape(eeg.data, 64, []);
    ntimes = size(eegcat, 2);
    netimes = size(eeg.data, 2);
    
% order = U, W, V, X
    task = {'U', 'W', 'V', 'X'};
    taskcnt = zeros(1,4);
    for j = 1:4
        taskcnt(j) = sum(ismember({eeg.event.type}, task(j)));
    end
    
% [max99 max99_5 max99_9] = chan(64) * 3
    max = [prctile(abs(eegcat), val(1), 2) ...
        prctile(abs(eegcat), val(2), 2) ... % max99 = max(:,1) = 64*1
        prctile(abs(eegcat), val(3), 2) ... 
        prctile(abs(eegcat), val(4), 2)]; 

    for j = 1:numel(val)
    % ecnt(j).data = total window of extreme value 
    % for every epoch on all channel
    % ecnt(j).data = total epochs * 1
        ecnt(j).data = squeeze(sum(sum(...
            reshape(abs(eegcat)>repmat(max(:,j),1,ntimes),size(eeg.data)), ...
            2), 1));
        ecnt(j).cutoff = prctile(ecnt(j).data, cutpct);
        ecnt(j).cnt = sum(ecnt(j).data > ecnt(j).cutoff);
        ecnt(j).val = val(j);
        
        ecnt(j).pertask = zeros(1,4);
        for k = 1:4
            if k==1
                s = 1; e = taskcnt(1);
            else
                s = s+taskcnt(k-1); e = e+taskcnt(k);
            end
            
            temp = squeeze(sum(sum( ...
                abs(eeg.data(:,:,s:e)) > repmat(max(:,j),1,netimes,taskcnt(k)), ...
                2), 1));
            ecnt(j).pertask(k) = sum(temp > ecnt(j).cutoff);
        end
    end
    
    outlierStat(i,:) = [fname ecnt(1).cutoff ecnt(1).cnt ecnt(1).pertask ...
        ecnt(2).cutoff ecnt(2).cnt ecnt(2).pertask ...
        ecnt(3).cutoff ecnt(3).cnt ecnt(3).pertask ...
        ecnt(4).cutoff ecnt(4).cnt ecnt(4).pertask];
    outlierDetail(i, 2:end, :) = max;
    outlierDetail(i, 1, :) = fname;
end

writeToCSV(outlierStat, ...
    fullfile(wd_ana, ['outlierStat_cut' num2str(cutpct) '_raw.csv']), ...
    varnames);

% for i = 1:numel(val)
%     digits(2);
%     writeToCSV(squeeze(outlierDetail(:,:,i)), ...
%         fullfile(wd_ana, ['outlierDetail_' num2str(val(i)) '_raw.csv']), ...
%         varnames2);
% end