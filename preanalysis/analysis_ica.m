foldern = 'ica_rejica_car_revc_reve_intpo_lp40_bc_filt1-100';
savepath1 = fullfile(wd_vis, ['ica_topo_' foldern]);
savepath2 = fullfile(wd_vis, ['ica_ts_' foldern]);
savepath3 = fullfile(wd_vis, ['rejica_topo_' foldern]);
savepath4 = fullfile(wd_vis, ['rejica_ts_' foldern]);
savepath5 = fullfile(wd_vis, ['rejica_chan_' foldern]);
savepathFin = fullfile(wd_prep, ['rej' foldern]);
%%
[tasklab,~,foldername] = getFilename(fullfile(wd_prep,foldern));
tasklab = tasklab(~contains(tasklab, '.csv'));
rejstatname = foldername(contains(foldername, '.csv'));
foldername = foldername(~contains(foldername, '.csv'));
[~,~,filelist] = getFilename(foldername, '*.set');
filename = getFilename(char(foldername(1)), '*.set');
%%

for i = 1:numel(filelist)
    subn = regexp(char(filename(i)),'\_','split');
    subn = char(subn(1));
    taskn = regexp(char(filelist(i)),'\\','split');
    taskn = char(taskn(end-1));
    savep1 = fullfile(savepath1, taskn, subn);
    savep2 = fullfile(savepath2, taskn, subn);
    checkPath(savep1);
    checkPath(savep2);
    
    eeg = pop_loadset(char(filelist(i)));
    icacont = reshape(eeg.icaact,size(eeg.icaact,1),[]);
    
    for j = 1:size(eeg.icaact,1)
        pop_prop(eeg,0,j,[],{'freqrange' [1 33.75]});
        print(gcf,fullfile(savep1,...
            ['ica_topo_' subn '_ic' num2str(j) '.png']),'-dpng');
        close all
        delete(gcf);
    
        f = figure;
        plot(1:eeg.trials*eeg.pnts,squeeze(icacont(j,:)));
        title(['ica-ts-' subn '-ic' num2str(j)]);
        print(f,fullfile(savep2,...
            ['ica_ts_' subn '_ic' num2str(j) '.png']),'-dpng');
        close all
        delete(f);
    end
end

%%
imloadp1 = 'rejica_topo_ica_car_revc_reve_intpo_lp40_bc_filt1-100';
imloadp2 = 'rejica_ts_ica_car_revc_reve_intpo_lp40_bc_filt1-100';
savepath = 'rejica_comp_ica_car_revc_reve_intpo_lp40_bc_filt1-100';
taskp = getFilename(fullfile(wd_vis, imloadp1));
subp = getFilename(fullfile(wd_vis, imloadp2, char(taskp(1))));

for i = 1:numel(taskp)
    for j = 1:numel(subp)
        savep = fullfile(wd_vis,savepath,char(taskp(i)),char(subp(j)));
        checkPath(savep);
        path1 = fullfile(wd_vis,imloadp1,char(taskp(i)),char(subp(j)));
        path2 = fullfile(wd_vis,imloadp2,char(taskp(i)),char(subp(j)));
        [filename,~,filelist1] = getFilename(path1);
        [~,~,filelist2] = getFilename(path2);
        for k = 1:numel(filelist1)
            img1 = imread(char(filelist1(k)));
            img2 = imread(char(filelist2(k)));
            
            f = figure;
            f.Units = 'normalized'; 
            f.Position = [0 0 0.28 1];
            f.Visible = 'off';
            
            ax1 = subplot(2,1,1);
            image(ax1,img1,'CDataMapping','scaled');
            title('IC properties');
            ax2 = subplot(2,1,2);
            image(ax2,img2,'CDataMapping','scaled');
            title('continuous time-series of IC');
            
            print(f,fullfile(savep,char(filename(k))),'-dpng');
            close all
            delete(f);
            delete(gcf);
        end
    end
end

%% check the quality of IC rejection
chanrevpath = fullfile(wd_ana, 'reve_epoch_intpo20-24_lp40_bc_filt1-100_norestStat');
for t = 1:numel(foldername)
    rejstat = csvread(char(rejstatname(t)),1,0);
%     savepFin = fullfile(savepathFin, char(tasklab(t)));
%     checkPath(savepFin);
    chanrev = csvread(fullfile(chanrevpath, ['chanRemoved_' char(tasklab(t)) '.csv']),1,0);
    
    for i = 1:numel(filename)
        filen = char(filename(i)); filen = filen(1:end-4);
        subn = regexp(filen,'\_','split');
        fname = char(subn(1));
        subn = str2double(fname(4:end));
        
        savep5 = fullfile(savepath5, char(tasklab(t)), fname);
        checkPath(savep5); 
        
        chanrest = find(chanrev(chanrev(:,1)==subn,2:end)==0);
        icrej = find(rejstat(rejstat(:,1)==subn,2:end)>0.5);
        eeg = pop_loadset(fullfile(char(foldername(t)), ...
            char(filename(i))));
        eegsub = pop_subcomp(eeg, icrej, 0, 0);
        
%         pop_saveset(eegsub,'filename',filen, 'filepath',savepFin);
        
        erp = squeeze(mean(eeg.data,3));
        erpsub = squeeze(mean(eegsub.data,3));
        
%         eeg = rankDeficientICA(eeg, 'runica');
% 
%         icacont = reshape(eeg.icaact,size(eeg.icaact,1),[]);
%     
%         for j = 1:size(eeg.icaact,1)
%             pop_prop(eeg,0,j,[],{'freqrange' [1 33.75]});
%             print(gcf,fullfile(savep3,...
%                 ['ica_topo_' fname '_ic' num2str(j) '.png']),'-dpng');
%             close all
%             delete(gcf);
%     
%             f = figure;
%             plot(1:eeg.trials*eeg.pnts,squeeze(icacont(j,:)));
%             title(['ica-ts-' fname '-ic' num2str(j)]);
%             print(f,fullfile(savep4,...
%                 ['ica_ts_' fname '_ic' num2str(j) '.png']),'-dpng');
%             close all
%             delete(f);
%         end
        
        % cont. TS per channel & ERP per channel
        for j = 1:eeg.nbchan
        
            f = figure;
            f.Visible = 'off';
            subplot(2,2,1);
            eegtemp = squeeze(reshape(eeg.data(j,:),1,[]));  
            contimes = 1:size(eegtemp,2);
            plot(contimes, eegtemp);
            line([contimes(1) contimes(end)], ...
                [mean(eegtemp) mean(eegtemp)], ...
                'Color', 'red');
            line([contimes(1) contimes(end)], ...
                [mean(eegtemp)-3*std(eegtemp) ...
                mean(eegtemp)-3*std(eegtemp)], ...
                'Color', 'red');
            line([contimes(1) contimes(end)], ...
                [mean(eegtemp)+3*std(eegtemp) ...
                mean(eegtemp)+3*std(eegtemp)], ...
                'Color', 'red');  
            title(['origTS-' fname '-chan' num2str(chanrest(j))]);
            
            subplot(2,2,2);
            eegtemp = squeeze(reshape(eegsub.data(j,:),1,[]));
            plot(contimes, eegtemp);
            line([contimes(1) contimes(end)], ...
                [mean(eegtemp) mean(eegtemp)], ...
                'Color', 'red');
            line([contimes(1) contimes(end)], ...
                [mean(eegtemp)-3*std(eegtemp) ...
                mean(eegtemp)-3*std(eegtemp)], ...
                'Color', 'red');
            line([contimes(1) contimes(end)], ...
                [mean(eegtemp)+3*std(eegtemp) ...
                mean(eegtemp)+3*std(eegtemp)], ...
                'Color', 'red');  
            title(['icrejTS-' fname '-chan' num2str(chanrest(j))]);
            
            subplot(2,2,3);
            plot(eeg.times, erp(j,:));
            line([eeg.times(1) eeg.times(end)], ...
                [mean(erp(j,:)) mean(erp(j,:))], ...
                'Color', 'red');
            line([eeg.times(1) eeg.times(end)], ...
                [mean(erp(j,:))-3*std(erp(j,:)) ...
                mean(erp(j,:))-3*std(erp(j,:))], ...
                'Color', 'red');
            line([eeg.times(1) eeg.times(end)], ...
                [mean(erp(j,:))+3*std(erp(j,:)) ...
                mean(erp(j,:))+3*std(erp(j,:))], ...
                'Color', 'red');  
            title(['origERP-' fname '-chan' num2str(chanrest(j))]);
        
            subplot(2,2,4);
            plot(eegsub.times, erpsub(j,:));
            line([eegsub.times(1) eegsub.times(end)], ...
                [mean(erpsub(j,:)) mean(erpsub(j,:))], ...
                'Color', 'red');
            line([eegsub.times(1) eegsub.times(end)], ...
                [mean(erpsub(j,:))-3*std(erpsub(j,:)) ...
                mean(erpsub(j,:))-3*std(erpsub(j,:))], ...
                'Color', 'red');
            line([eegsub.times(1) eegsub.times(end)], ...
                [mean(erpsub(j,:))+3*std(erpsub(j,:)) ...
                mean(erpsub(j,:))+3*std(erpsub(j,:))], ...
                'Color', 'red');  
            title(['icrejERP-' fname '-chan' num2str(chanrest(j))]);            
            
            print(f, fullfile(savep5, ...
            [fname '_chan' num2str(chanrest(j)) '.png']), '-dpng');
        
            close all
            delete(f);
            delete(gcf);
        end
    end
end

%% check rejected components
figpath = fullfile(wd_vis, 'ica_comp_ica_car_revc_reve_intpo_lp40_bc_filt1-100');
savefigpath = fullfile(wd_vis, 'rejectedComponent');
for t = 1:numel(tasklab)
    rejstat = csvread(char(rejstatname(t)),1,0);
    for i = 1:numel(filename)
        fname = regexp(char(filename(i)), '\_', 'split');
        fname = char(fname(1));
        subn = str2double(fname(4:end));
        
        figp = fullfile(figpath, char(tasklab(t)), fname);
        savep = fullfile(savefigpath, char(tasklab(t)), fname);
        checkPath(savep);
        
        rejcomp = find(rejstat(rejstat(:,1)==subn,2:end)>0.5);
        for j = 1:numel(rejcomp)
            fign = ['ica_topo_' fname '_ic' num2str(rejcomp(j)) '.png'];
            copyfile(fullfile(figp, fign), fullfile(savep, fign));
        end
        
    end
end

%% rebuild rejstat based on the `rejectedComponent` folder
savepath = fullfile(wd_prep, 'rejStat');
checkPath(savepath);
filename = getFilename(fullfile(savefigpath, char(tasklab(1))));
for t = 1:numel(tasklab)
    rejstat = zeros(numel(filename), 65);
    for i = 1:numel(filename)
        fname = char(filename(i));
        rejstat(i,1) = str2double(fname(4:end));
        
        figlist = getFilename(fullfile(savefigpath, ...
            char(tasklab(t)), char(filename(i))));
        if ~isempty(figlist)
            rejic = zeros(1,numel(figlist));
            for j = 1:numel(figlist)
                temp = regexp(char(figlist(j)),'\_','split');
                temp = regexp(char(temp(end)),'\.','split');
                temp = char(temp(1));
                rejic(j) = str2double(temp(3:end));
            end
        end
        rejstat(i,rejic + 1) = 1;
    end
    
    writeToCSV(rejstat, fullfile(savepath, ...
        ['revIC_' char(tasklab(t)) '.csv']), ...
        [{'sub'} arrayfun(@(x)['IC' num2str(x)],1:64,'UniformOutput',0)]);
end


%% visualize time-freq properties for ICrejected and TF data
tffold1 = 'tf_intpc_rejica_car_revc_reve_intpo_lp40_bc_filt1-100';
tffold2 = 'tf_lap2_intpc_rejica_car_revc_reve_intpo_lp40_bc_filt1-100';
tasklab = 'U';
