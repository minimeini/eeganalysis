% for partitioned EEG only
function [durStat, durMean, dur] = checkDuration(filepath, varargin)
p = inputParser;
addRequired(p, 'filepath', @ischar);
addParameter(p, 'savedir', [], @ischar);
parse(p, filepath, varargin{:});

filepath = p.Results.filepath;
savedir = p.Results.savedir;

folderlist = dir(fullfile(filepath));
folderlist = {folderlist.name};
folderlist = folderlist(3 : end);
disp(folderlist);

totalFile = length(dir(fullfile(filepath, char(folderlist(1)), '*.set')));
durStat = zeros(totalFile, length(folderlist) * 7);
durMean = zeros(totalFile, 4);
for type = 1 : length(folderlist)
    filelist = dir(fullfile(filepath, char(folderlist(type)), '*.set'));
    filelist = {filelist.name};
    for sub = 1 : totalFile
        EEG = pop_loadset(char(filelist(sub)), ...
            fullfile(filepath, char(folderlist(type))));
        switch char(folderlist(type))
            case 'le'
                [durStat(sub, 1 : 7), dur(sub).le] = getDuration(EEG, 'exe');
                durMean(sub, 1) = mean(dur(sub).le);
            case 'li'
                [durStat(sub, 8 : 14), dur(sub).li] = getDuration(EEG, 'img');
                durMean(sub, 2) = mean(dur(sub).li);
            case 're'
                [durStat(sub, 15 : 21), dur(sub).re] = getDuration(EEG, 'exe');
                durMean(sub, 3) = mean(dur(sub).re);
            case 'ri'
                [durStat(sub, 22 : 28), dur(sub).ri] = getDuration(EEG, 'img');
                durMean(sub, 4) = mean(dur(sub).ri);
        end
    end
end

if ~isempty(savedir) 
    orderlist = fileOrder(fullfile(filepath, char(folderlist(1))), '*.set');
    writeToCSV([orderlist' durStat], 'presStat.csv', savedir);
    writeToCSV([orderlist' durMean], 'presStat_mean.csv', savedir, ...
        'header', {'sub' 'le' 'li' 're' 'ri'});
    save(fullfile(savedir, 'presStat_detail.mat'), 'dur');
end
end

function [durStat, dur] = getDuration(EEG, type)
% durStat: 7 * 1 vector = [min quar1 mean quar3 max var totalTrials];
% dur: length(EEG.event) * 1 vector = {duration}
% return duration in milliseconds

p = inputParser;
addRequired(p, 'EEG', @isstruct);
addRequired(p, 'type', ...
    @(x) any(validatestring(x, {'img', 'exe'})));
parse(p, EEG, type);

EEG = p.Results.EEG;
type = p.Results.type;

if strcmp(type, 'exe')
    len = 14;
else
    len = 6;
end
totalTrials = length(EEG.event) / len;

dur = -1;
for i = 1 : totalTrials
    s_idx = len * (i - 1) + 1;
    startIdx = s_idx + 2;
    endIdx = s_idx + len - 2;
    
    % convert into milliseconds: 
    % delta(latency) / srate * 1000 = milliseconds
    durTemp = (EEG.event(endIdx).latency - EEG.event(startIdx).latency) * 2;
    if dur(1) == -1
        dur = durTemp;
    else
        dur = [dur durTemp];
    end
end

[~, ~, ~, ~, ~, durStat] = getPercentile(dur);
durStat = [durStat var(dur) totalTrials];

end