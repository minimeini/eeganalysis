%%
foldern = 'reve_epoch_intpo20-24_lp40_bc_filt1-100_norest';
savepath = fullfile(wd_ana, [foldern 'Stat']);
[tasklab,~,foldername] = getFilename(fullfile(wd_prep,foldern));
filename = getFilename(char(foldername(1)), '*.set');

%%
lowsoft = 7.5; lowhard = 1.8;
upsoft = 92.5; uphard = 21;

%% detect bad channels based on std
% criteria
% 1. std < min(prctile(std,5), 2) or
% 2. std > max(prctile(std,90), 12) or

varnames = {'sub', ['stdpct' num2str(lowsoft)], ...
    ['stdpct' num2str(upsoft)], ...
    ['badstd_lowhard' num2str(lowhard)], ...
    ['badstd_uphard' num2str(uphard)], ...
    'badstdlow', 'badstdup'};

for t = 1:numel(tasklab)
[~,~,statpath] = ...
    getFilename(fullfile(savepath, char(tasklab(t))));
pctstat = zeros(numel(statpath), numel(varnames));
for i = 1:numel(statpath)
    subn = regexp(char(statpath(i)),'\.','split');
    subn = regexp(char(subn(1)),'\\','split');
    subn = regexp(char(subn(end)),'\_','split');
    subn = char(subn(end));
    subn = str2double(subn(4:end));
    
    diststat = csvread(char(statpath(i)), 1, 1);
    stdtemp = squeeze(diststat(:,5));
    
    stdpct_max = prctile(stdtemp, upsoft);
    stdpct_min = prctile(stdtemp, lowsoft);
    
    pctstat(i,:) = [subn stdpct_min stdpct_max ...
        sum(stdtemp<=lowhard) sum(stdtemp>=uphard) ...
        sum(stdtemp<=min(stdpct_min,lowhard)) ...
        sum(stdtemp>=max(stdpct_max,uphard))]; 
end

[~,idx] = sort(squeeze(pctstat(:,1)));
pctstat = pctstat(idx,:);
writeToCSV(pctstat, fullfile(savepath, ...
    ['pctstat_badchan_' char(tasklab(t)) '.csv']), varnames);
end

%% remove and interpolate bad channels using std + CAR
% just reject channels with SD hihger than its 90% percentile per subject
% or with SD lower than its 5% percentile

% savepath = fullfile(wd_prep, ['car_std_' foldername]);
% checkPath(savepath);

for t = 1:numel(tasklab)
chanRemoved = zeros(numel(filelist), 1+64);
if exist('badchan', 'var'), clear badchan; end

for sub = 1 : numel(filelist)
    fnamesave = char(filename(sub));
    fnamesave = fnamesave(1:end-4);
    subn = regexp(fnamesave,'\_','split');
    subn = char(subn(1)); subn = str2double(subn(4:end));
    
    origEEG = pop_loadset(fullfile(wd_prep,foldern,...
        char(tasklab(t)),char(filename(sub))));
    eegdata = reshape(origEEG.data, origEEG.nbchan, []);
    ntimes = size(eegdata,2);
    deviant = eegdata - repmat(mean(eegdata,2),1,ntimes);
    chanstd = sqrt(1/(ntimes-1) * diag(deviant * deviant'))';
    
    lowbound = min(prctile(chanstd,lowsoft), lowhard);
    upbound = max(prctile(chanstd,upsoft), uphard);
    chanidx = chanstd<=lowbound | chanstd>=upbound;
    chanRemoved(sub,:) = [subn chanidx];
    
    badchan(sub).subn = subn;
    badchan(sub).lowbound = lowbound;
    badchan(sub).lowbad = find(chanstd<=lowbound);
    badchan(sub).lowbadstd = chanstd(chanstd<=lowbound);
    
    badchan(sub).upbound = upbound;
    badchan(sub).upbad = find(chanstd>=upbound);
    badchan(sub).upbadstd = chanstd(chanstd>=upbound);
    
%     EEG = pop_select(origEEG, 'nochannel', find(chanidx));
%     EEG = pop_interp(EEG, origEEG.chanlocs, 'spherical');
%     EEG.data = EEG.data - repmat(mean(EEG.data,1), 64, 1);
%     pop_saveset(EEG, 'filename', fnamesave, ...
%         'filepath', savepath);
end

varnames = [{'sub'} ...
    arrayfun(@(x) ['chan' num2str(x)], 1:64, 'UniformOutput', 0)];
writeToCSV(chanRemoved, fullfile(savepath, ...
    ['chanRemoved_std_' char(tasklab(t)) '.csv']), varnames);
end

%% single-channel continuous-time-domain plot for dead/crazy channels
savepath_dead = fullfile(wd_vis, ['std_deadchan_' foldername]);
savepath_crazy = fullfile(wd_vis, ['std_crazychan_' foldername]);
checkPath(savepath_dead);
checkPath(savepath_crazy);

for i = 1:numel(filelist)
    eeg = pop_loadset(char(filelist(i)));
    crazychan = badchan(i).upbad;
    deadchan = badchan(i).lowbad;
    
    for j = 1:eeg.nbchan
        if ismember(j,crazychan) || ismember(j,deadchan)
            if ismember(j,crazychan)
                savep = savepath_crazy;
            else
                savep = savepath_dead;
            end
            
            eegtemp = squeeze(eeg.data(j,:));
            
            f = figure('units', 'normalized', ...
                'outerposition', [0 0 1 1], ...
                'Visible', 'off');
            plot(eeg.times, eegtemp);
            line([eeg.times(1) eeg.times(end)], ...
                [mean(eegtemp) mean(eegtemp)], ...
                'Color', 'red');
            line([eeg.times(1) eeg.times(end)], ...
                [mean(eegtemp)-3*std(eegtemp) ...
                mean(eegtemp)-3*std(eegtemp)], ...
                'Color', 'red');
            line([eeg.times(1) eeg.times(end)], ...
                [mean(eegtemp)+3*std(eegtemp) ...
                mean(eegtemp)+3*std(eegtemp)], ...
                'Color', 'red');
            
            title(['sub' num2str(subn(i)) ...
                '-chan' num2str(j)]);
            
            print(f, fullfile(savep, ...
                ['sub' num2str(subn(i)) ...
                '_chan' num2str(j) '_.png']), '-dpng');
            close all
            delete(f);
            delete(gcf);  
        end    
    end
end


%% conclusion. std + bandpower
chanrev_total = zeros(numel(filelist),4+1);
for t = 1:numel(tasklab)
chanrev_std = csvread(fullfile(savepath, ...
    ['chanRemoved_std_' char(tasklab(t)) '.csv']),1,0);
chanrev_bp = csvread(fullfile(wd_ana, ...
    'chanRemoved_bp_rev_bc_filt1-100_norest.csv'),1,0);
[~,idx] = sort(chanrev_std(:,1));
chanrev_std = chanrev_std(idx,:);
[~,idx] = sort(chanrev_bp(:,1));
chanrev_bp = chanrev_bp(idx,:);

chanrev = [chanrev_std(:,1) ...
    logical(chanrev_std(:,2:end) + chanrev_bp(:,2:end))];
writeToCSV(chanrev, fullfile(savepath,...
    ['chanRemoved_' char(tasklab(t)) '.csv']), ...
    [{'sub'} arrayfun(@(x)['chan' num2str(x)],...
    1:64,'UniformOutput',0)]);

chanrev_total(:,t+1) = sum(chanrev(:,2:end),2);
end
chanrev_total(:,1) = chanrev_std(:,1);
writeToCSV(chanrev_total,fullfile(savepath, 'chanRemoved_total.csv'), ...
    [{'sub'} tasklab]);