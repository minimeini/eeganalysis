function [orderlist] = fileOrder(filepath, format)
p = inputParser;
addRequired(p, 'filepath', @ischar);
addRequired(p, 'format', @ischar); % '*.type'
parse(p, filepath, format);

[filelist, totalFile] = getFilename(p.Results.filepath, p.Results.format);
orderlist = zeros(1, totalFile);
for i = 1 : totalFile
    f = regexp(char(filelist(i)), '_*_', 'split');
    f = char(f(1));
    f = f(4:end);
    orderlist(i) = str2double(f); 
end
end