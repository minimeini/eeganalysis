%% analyze the distribution per channel for every subject
% compare the results from RAW and epochRAW (concatednated)
foldername = 'lap2_intpc_rejica_car_revc_reve_intpo_lp40_bc_filt1-100';
tasklab = getFilename(fullfile(wd_prep, foldername));
filelist = getFilename(fullfile(wd_prep, foldername, ...
    char(tasklab(1))),'*.set');
%% analyze the distribution in time domain
varnames = {'chan', 'min', 'mean', 'median', 'max', 'std', 'kurt'};
savepath = fullfile(wd_ana, [foldername 'Stat']);
checkPath(savepath);
rawstat = zeros(numel(filelist), 64, numel(varnames));
for t = 1:numel(tasklab)
for i = 1:numel(filelist)[
    fname = regexp(char(filelist(i)), '\_', 'split');
    fname = char(fname(1));
    
    eeg = pop_loadset(fullfile(wd_prep,foldername,...
        char(tasklab(t)),char(filelist(i))));
    eegdata = reshape(eeg.data,eeg.nbchan,[]);
    
    
    rawstat(i,1:eeg.nbchan,1) = 1:eeg.nbchan;
    rawstat(i,1:eeg.nbchan,2) = min(eegdata, [], 2);
    rawstat(i,1:eeg.nbchan,3) = mean(eegdata, 2);
    rawstat(i,1:eeg.nbchan,4) = median(eegdata, 2);
    rawstat(i,1:eeg.nbchan,5) = max(eegdata, [], 2);
        
    meaneeg = repmat(mean(eegdata,2), 1, size(eegdata,2));
    rawstat(i,1:eeg.nbchan,6) = sqrt((1/(size(eegdata,2)-1)) * ...
        diag((eegdata-meaneeg)*(eegdata-meaneeg)'));
    
    for j = 1:size(eegdata,1)
        rawstat(i,j,7) = kurt(squeeze(eegdata(j,:)));
    end
    
    savep = fullfile(savepath, char(tasklab(t)));
    checkPath(savep);
    writeToCSV(squeeze(rawstat(i,:,:)), ...
        fullfile(savep, ['Stat_' char(tasklab(t)) '_' fname '.csv']), ...
        varnames);
end

writeToCSV(squeeze(mean(rawstat,1)), ...
    fullfile(savepath, [char(tasklab(t)) '_Stat_ground.csv']), varnames);
end
%% visualize single-channel time-series for every subject
savepath = fullfile(wd_vis, ['ts_' foldername]);

for i = 1:numel(filelist)
    fname = regexp(char(filename(i)), '\_', 'split');
    fname = char(fname(1));
    savep = fullfile(savepath, fname);
    checkPath(savep);
    
    eeg = pop_loadset(char(filelist(i)));
    for j = 1:eeg.nbchan
        eegtemp = squeeze(eeg.data(j,:));
        
        f = figure;
        f.Visible = 'off';
        plot(eeg.times, eegtemp);
        line([eeg.times(1) eeg.times(end)], ...
            [mean(eegtemp) mean(eegtemp)], ...
            'Color', 'red');
        line([eeg.times(1) eeg.times(end)], ...
            [mean(eegtemp)-3*std(eegtemp) ...
            mean(eegtemp)-3*std(eegtemp)], ...
            'Color', 'red');
        line([eeg.times(1) eeg.times(end)], ...
            [mean(eegtemp)+3*std(eegtemp) ...
            mean(eegtemp)+3*std(eegtemp)], ...
            'Color', 'red');  
        title([fname '-chan' num2str(j)]);
        xlabel(replace(foldername, '_', '-'));
        
        print(f, fullfile(savep, ...
            [fname '_chan' num2str(j) '.png']), '-dpng');
        
        close all
        delete(f);
        delete(gcf);
    end
end

%% compare the parameter settings of outliers interpolation
savepath = fullfile(wd_vis, ['comp_revo15--15-24_' foldername]);
rawfold = 'lp30_cl50-100-w2_rev_bc_filt1-100_norest';
revofold1 = 'revo15_lp30_cl50-100-w2_rev_bc_filt1-100_norest';
revofold2 = 'revo15-24_lp30_cl50-100-w2_rev_bc_filt1-100_norest';

for i = 1:numel(filelist)
    fname = regexp(char(filename(i)), '\_', 'split');
    fname = char(fname(1));
    savep = fullfile(savepath, fname);
    checkPath(savep);
    
    eegraw = pop_loadset(fullfile(wd_prep, rawfold, char(filename(i))));
    eegrevo1 = pop_loadset(fullfile(wd_prep, revofold1, char(filename(i))));
    eegrevo2 = pop_loadset(fullfile(wd_prep, revofold2, char(filename(i))));
    times = eegraw.times;
    
    for j = 1:eeg.nbchan
        eegrawtemp = squeeze(eegraw.data(j,:));
        eegrevo1temp = squeeze(eegrevo1.data(j,:));
        eegrevo2temp = squeeze(eegrevo2.data(j,:));
        
        f = figure;
        f.Visible = 'off';
        
        subplot(3,1,1);
        plot(times, eegrawtemp);
        line([times(1) times(end)], ...
            [mean(eegrawtemp) mean(eegrawtemp)], ...
            'Color', 'red');
        line([times(1) times(end)], ...
            [mean(eegrawtemp)-3*std(eegrawtemp) ...
            mean(eegrawtemp)-3*std(eegrawtemp)], ...
            'Color', 'red');
        line([times(1) times(end)], ...
            [mean(eegrawtemp)+3*std(eegrawtemp) ...
            mean(eegrawtemp)+3*std(eegrawtemp)], ...
            'Color', 'red');  
        title('no outlier interpolation');
        
        subplot(3,1,2);
        plot(times, eegrevo1temp);
        line([times(1) times(end)], ...
            [mean(eegrevo1temp) mean(eegrevo1temp)], ...
            'Color', 'red');
        line([times(1) times(end)], ...
            [mean(eegrevo1temp)-3*std(eegrevo1temp) ...
            mean(eegrevo1temp)-3*std(eegrevo1temp)], ...
            'Color', 'red');
        line([times(1) times(end)], ...
            [mean(eegrevo1temp)+3*std(eegrevo1temp) ...
            mean(eegrevo1temp)+3*std(eegrevo1temp)], ...
            'Color', 'red');  
        title('thres = 25, hard edge = 200');        

        subplot(3,1,3);
        plot(times, eegrevo2temp);
        line([times(1) times(end)], ...
            [mean(eegrevo2temp) mean(eegrevo2temp)], ...
            'Color', 'red');
        line([times(1) times(end)], ...
            [mean(eegrevo2temp)-3*std(eegrevo2temp) ...
            mean(eegrevo2temp)-3*std(eegrevo2temp)], ...
            'Color', 'red');
        line([times(1) times(end)], ...
            [mean(eegrevo2temp)+3*std(eegrevo2temp) ...
            mean(eegrevo2temp)+3*std(eegrevo2temp)], ...
            'Color', 'red');  
        title('thres = 15, hard edge = 200, hard thres = 24');   
        
        suptitle([fname '-chan' num2str(j)]);
        
        print(f, fullfile(savep, ...
            [fname '_chan' num2str(j) '.png']), '-dpng');
        
        close all
        delete(f);
        delete(gcf);
    end
end