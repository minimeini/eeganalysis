%% analyze latencies for "home"
filelist = getFilename(fullfile(wd_prep, 'trialCleaned', '*.set'));

trialStart = {'U', 'V', 'W', 'X'};
trialEnd = 'Z';
homePressed = 'h';
homeReleased = 'H';
    
for sub = 1 : numel(filelist)
    EEG = pop_loadset(char(filelist(sub)), fullfile(wd_prep, 'trialCleaned'));
    startStat(sub) = getStart(EEG, trialStart, true);
    finStat(sub) = getEnd(EEG, startStat(sub));
    [presStat(sub).idx, presStat(sub).lat] = findIndex(EEG.event, ...
        homePressed, startStat(sub).idx);
    [relStat(sub).idx, relStat(sub).lat] = findIndex(EEG.event, ...
        homeReleased, startStat(sub).idx);
    homeDur(sub).dur = relStat(sub).lat - presStat(sub).lat;
    ballDur(sub).dur = finStat(sub).time - relStat(sub).lat;
end

% save(fullfile(wd_ana, 'trialStatus.mat'), 'startStat', 'dur', '-v7.3');
% save(fullfile(wd_ana, 'taskStatus.mat'), 'finStat', 'presStat', 'relStat');
clear EEG

for sub = 1 : numel(filelist)
    [homeDur(sub).min, homeDur(sub).quar1, ...
        homeDur(sub).mean, homeDur(sub).quar3, homeDur(sub).max] ...
        = getPercentile(homeDur(sub).dur);
    homeDur(sub).var = var(homeDur(sub).dur);
    
    [ballDur(sub).min, ballDur(sub).quar1, ...
        ballDur(sub).mean, ballDur(sub).quar3, ballDur(sub).max] ...
        = getPercentile(ballDur(sub).dur);
    ballDur(sub).var = var(ballDur(sub).dur);

    homeDur(sub).bad = sum(homeDur(sub).dur < (1.5 * 500));
end
save(fullfile(wd_ana, 'durStatus.mat'), 'homeDur', 'ballDur');

orderlist = fileOrder(wd_raw, '*.vhdr');
writeToCSV([orderlist' [homeDur.min]' [homeDur.quar1]' [homeDur.mean]' ...
    [homeDur.quar3]' [homeDur.max]' [homeDur.var]' ...
    [homeDur.bad]'], fullfile(wd_ana, 'homdDur.csv'), ...
    {'sub' 'min', 'quar1', 'mean', 'quar3', 'max', 'var', 'bad'});

writeToCSV([orderlist' [ballDur.min]' [ballDur.quar1]' [ballDur.mean]' ...
    [ballDur.quar3]' [ballDur.max]' [ballDur.var]'], ...
    fullfile(wd_ana, 'ballDur.csv'), ...
    {'sub' 'min', 'quar1', 'mean', 'quar3', 'max', 'var'});
