function [eeg_noise_signal, eeg_noise] = sim_eeg_noise(eeg_signal, gain, ...
    fs, rec_len, varargin)
% function eeg_noise = sim_eeg_noise(eeg_signal, gain, varargin)
%
% REQUIRED: eeg_signal, gain, fs, rec_len, 
% OPTIONAL: bandpass, noise_rate, snr (in [0.1, 0.9])

narg = 4;
bandpass = []; noise_rate = 0.02; snr = 0.1;
if nargin >= narg + 1, bandpass = varargin{1}; end
if nargin >= narg + 2, noise_rate = varargin{2}; end
if nargin >= narg + 3, snr = varargin{3}; end

if isempty(bandpass), bandpass = [8 13]; end

%% basic settings
nbchan = size(gain, 1);
nsource = size(gain, 2);
n_noise_sources = round(noise_rate * nsource);
noise_inds = randi(round(nsource), 1, n_noise_sources);
N = fs * rec_len; % resulting number of samples

[b_band, a_band] = butter(3, bandpass/fs*2);
[b_high, a_high] = butter(3, 0.1/fs*2, 'high');

%% biological noise, mixture of independent pink noise sources 
pn = mkpinknoise(N, n_noise_sources)';
eeg_brain_noise_raw = gain(:, noise_inds) * pn;
norm_brain_noise = norm(filtfilt(b_band, a_band, pn')', 'fro');
eeg_brain_noise = eeg_brain_noise_raw ./ norm_brain_noise;

%% sensor noise
eeg_sensor_noise = randn(nbchan, N);
eeg_sensor_noise = eeg_sensor_noise ./ norm(eeg_sensor_noise, 'fro');

%% EEG signal with noise
eeg_brain_signal_noise = snr * eeg_signal + (1-snr) * eeg_brain_noise;
eeg_brain_signal_noise = eeg_brain_signal_noise ./ ...
    norm(eeg_brain_signal_noise, 'fro');

% overall noise is dominated by biological noise
eeg_noise_signal = 0.9*eeg_brain_signal_noise + 0.1*eeg_sensor_noise;
eeg_noise_signal = filtfilt(b_high, a_high, eeg_noise_signal')';

%% pure EEG noise
eeg_brain_noise = eeg_brain_noise_raw ./ norm(eeg_brain_noise_raw, 'fro');
eeg_noise = 0.9*eeg_brain_noise + 0.1*eeg_sensor_noise;  
eeg_noise = filtfilt(b_high, a_high, eeg_noise')';

end