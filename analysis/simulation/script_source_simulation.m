%% Introduction
% Source Estimation
%
% `Head Model`: the modeling of the electromagnetic properties of the head
% and of the sensor array
% ---- Orientation
% -------- Unconstrained: three orthogonal dipoles, use the triplet to 
% account mathematically for local currents flowing in arbitrary directions. 
% (# of Elementry sources) = (3 orientations) * (# of vertices)
% -------- Constrained: one dipole is assigned to each vertex with its
% orientation perpendicular to the cortical surface
% (# of Elementry sources) = (# of vertices/)
%
%
% `Source Model`: the estimation of the brain sources which produced the
% data

%% Initialize
clear all
wd = 'C:\Users\meini\OneDrive\eegAnalysis';
wd_fig = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd);

regname = {...
    'Left Occipital', 'Left Parietal', ...
    'Left Temporal', 'Left Frontal', ...
    'Right Occipital', 'Right Parietal', ...
    'Right Temporal', 'Right Frontal'};

% basic parameter
fs = 500;
rec_len = 2; % length in seconds
nepochs = 3;
seglen_uni = 10;

% electrodes
times = (1:2:2000) - 1500;
elec = readlocs(fullfile(wd, 'chanlocs', 'actiCap64Channel.xyz'));

% selected ROIs
regidx = [2 8]; % Left Parietal and Right Frontal

%% Load Cortical Mesh and Head Model
protocol = ['C:\Users\meini\OneDrive\eegAnalysis\database_bst\' ... 
    'Protocol01'];
headmodel = load(fullfile(protocol, 'data\Subject01\sub1_tmn_1107', ...
    'headmodel_surf_openmeeg.mat'));

cortexmesh = load(fullfile(protocol, 'anat\Subject01', ...
    'tess_cortex_spm_20484V.mat'));
cort_manifold = [];
cort_manifold.vc = cortexmesh.Vertices; % SCS/CTF coordinates in meters
cort_manifold.tri = cortexmesh.Faces;

% SCS: Subject Coordinate System (default in millimeters)
% defined in the following way
% - based on: NAS, LPA, RPA
% - origin: midway on the line joining LPA and RPA
% - Axis X: from the origin towards the NAS
% - Axis Y: from the origin towards LPA and orthogonal to X axis
% - Axis Z: from the origin towards the top of the head

% FIELD: Gain, GridLoc, GridOrient (ignore if constrained)
% headmodel.GridLoc = cortexmesh.Vertices

gain = bst_gain_orient(headmodel.Gain, headmodel.GridOrient);
gridloc = headmodel.GridLoc;

aidx = find(contains({cortexmesh.Atlas.Name}, 'DK8'));
atlas = cortexmesh.Atlas(aidx);
scouts = atlas.Scouts;
regions = {scouts.Label};
nregions = numel(regions);

for i = 1:numel(scouts)
    scouts(i).Region = regname{i};
end

% aidx_novol = find(contains({cortexmesh.Atlas.Name}, 'ElecMap'));
% atlas_novol = cortexmesh.Atlas(aidx_novol);
% scouts_novol = atlas_novol.Scouts;
%% Pick Two Distinguished Brain Sources
% ----------------------------------
% Procedure
% >>>>>>>>>>>>>>>>>
% 1. Pick two ROIs
% 2. For each ROI, a random node is picked as the center of the source
% acitivity. This node is required to be at least 10 mm (geodesic 
% distance) away from the boundary of the ROI.
%
% ----------------------------------
% Parameter Setting
% >>>>>>>>>>>>>>>>>
% extend brain region
% distance metric = `geodesic distance` between nodes
% `geodesic distance`: the distance between two vertices in a graph is the
% number of edges in a shortest path connecting them
% `eccentricity` of a vertex is the greatest geodesic distance between this
% vertex and any other vertex
% `radius`: the minimum eccentricity of any vertex
% `diameter`: the maximum eccentricity of any vertex in the whole graph

% 1. create selectable region for each ROI
% >> The selectable region consists of points that are at least 15 mm away
% from all other ROIs, in L2-norm.
%
% roi
% - idx: indices of vertices in this ROI
% - center: index of the central vertex in this ROI
% - pnts: coordinates of vertices in this ROI
% - face: indices of points in each face
% - adjoint: indices of adjoint ROIs
% - nbdist: distance to the adjoin ROIs for each point in this ROI
% - pidx: indices of vertices not in the edge of this ROI

roi_adjoint = {[2 3 5], [1 3 4 6], [1 2 4], [2 3 8], ...
    [1 6 7], [2 5 7 8], [5 6 8], [4 6 7]};

roi = struct();
for i = 1:numel(scouts)
    roi(i).idx = scouts(i).Vertices;
    roi(i).center = scouts(i).Seed;
    
    roi(i).pnts = cortexmesh.Vertices(roi(i).idx,:);
    face_idx = arrayfun(@(x) ...
        all(ismember(cortexmesh.Faces(x,:), roi(i).idx)), ...
        1:size(cortexmesh.Faces,1));
    roi(i).face = cortexmesh.Faces(face_idx,:);
    
    roi(i).adjoint = roi_adjoint{i};
end

for i = 1:numel(scouts)
    roi(i).nbdist = zeros(numel(roi(i).idx), numel(roi(i).adjoint));
    for j = 1:numel(roi(i).idx)
        pnt = roi(i).pnts(j,:);
        for k = 1:numel(roi(i).adjoint)
            ridx = roi(i).adjoint(k);
            pdist = arrayfun(@(x) norm(pnt-roi(ridx).pnts(x,:)), ...
                1:numel(roi(ridx).idx));
            roi(i).nbdist(j,k) = min(pdist(:));
        end
    end
end

for i = 1:numel(scouts)
    for j = 1:numel(roi(i).adjoint)
        tmp = roi(i).nbdist(:,j) > 0.015;
        if j == 1
            cond = tmp;
        else
            cond = cond .* tmp;
        end
    end
    roi(i).pidx = roi(i).idx(cond==1);
end
save(fullfile(wd, 'simulation', 'roi.mat'), 'roi');

%% Simulate on the Sensor Space - Define the ROIs
roi_elec = struct();

roi_elec(1).idx = sort([60 61 28 29 30 62]);
roi_elec(1).pidx = sort([60 61 29]);
roi_elec(1).center = 29; roi_elec(1).adjoint = [2 3 5];

roi_elec(2).idx = sort([47 13 48 14 18 52 19 53 23 56 24 57]); 
roi_elec(2).pidx = sort([52 19]);
roi_elec(2).center = 52; roi_elec(2).adjoint = [1 3 4 6];

roi_elec(3).idx = sort([41 42 12 51 17]); roi_elec(3).pidx = sort([12]);
roi_elec(3).center = 12; roi_elec(3).adjoint = [1 2 4];

roi_elec(4).idx = sort([1 33 34 3 37 4 38 5 8 43 9]); 
roi_elec(4).pidx = sort([37 4 38]);
roi_elec(4).center = 4; roi_elec(4).adjoint = [2 3 8];

roi_elec(5).idx = sort([62 63 64 30 31 32]); 
roi_elec(5).pidx = sort([63 64 31]);
roi_elec(5).center = 31; roi_elec(5).adjoint = [1 6 7];

roi_elec(6).idx = sort([14 49 15 50 16 53 20 54 21 25 58 26 59 27]); 
roi_elec(6).pidx = sort([20 54]);
roi_elec(6).center = 54; roi_elec(6).adjoint = [2 5 7 8];

roi_elec(7).idx = sort([45 16 55 46 22]); roi_elec(7).pidx = sort([15]);
roi_elec(7).center = 16; roi_elec(7).adjoint = [5 6 8];

roi_elec(8).idx = sort([2 35 36 5 39 6 40 7 10 44 11]); 
roi_elec(8).pidx = sort([39 6 40]);
roi_elec(8).center = 6; roi_elec(8).adjoint = [4 6 7];

for i = 1:numel(scouts)
    roi_elec(i).pnts = [ [elec(roi_elec(i).idx).X]' ...
        [elec(roi_elec(i).idx).Y]' [elec(roi_elec(i).idx).Y]'];
end

%% minimal ROI for validation of volume conduction
roi_min = struct();

roi_min(1).idx = 1; roi_min(1).pidx = 1; roi_min(1).center = 1;
roi_min(1).adjoint = [2 4 5]; roi_min(1).pnts = [-1 1];

roi_min(2).idx = 2; roi_min(2).pidx = 2; roi_min(2).center = 2;
roi_min(2).adjoint = [1 3 4]; roi_min(2).pnts = [1 1];

roi_min(3).idx = 3; roi_min(3).pidx = 3; roi_min(3).center = 3;
roi_min(3).adjoint = [2 4]; roi_min(3).pnts = [2 -1];

roi_min(4).idx = 4; roi_min(4).pidx = 4; roi_min(4).center = 4;
roi_min(4).adjoint = [1 2 3 5]; roi_min(4).pnts = [0 -1];

roi_min(5).idx = 5; roi_min(5).pidx = 5; roi_min(5).center = 5;
roi_min(5).adjoint = [1 4]; roi_min(5).pnts = [-2 -1];

%% simulate minimal source -> sensor
[eeg_pure_part1, conn_part1] = sim_eeg_pure_chaos(roi_min,[1 5],...
    5,500,2,3,1,0,0,1,0,0);
[eeg_pure_part2, conn_part2] = sim_eeg_pure_chaos(roi_min,[2 4],...
    5,500,2,3,1,0,0,1,0,0);

eeg_pure = eeg_pure_part1 + eeg_pure_part2;
conn = conn_part1 + conn_part2;

gain_min = diag(diag(ones(5)));
gain_min(3,1) = rand(1);
gain_min(5,1) = rand(1);

eeg_sensor_pure = zeros(size(eeg_pure));
eeg_sensor_noise = randn(size(eeg_pure));

for i = 1:3
    eeg_sensor_pure(:,:,i) = gain_min * squeeze(eeg_pure(:,:,i));
end

for i = 1:3
    eeg_sensor_noise(:,:,i) = squeeze(eeg_sensor_noise(:,:,i)) ./ ...
        norm(squeeze(eeg_sensor_noise(:,:,i)),'fro');
end

alpha = 0.001;
eeg_sensor = (1-alpha)*eeg_sensor_pure + alpha*eeg_sensor_noise;
eeg_sensor = eeg_sensor .* 1e4;

save(fullfile(wd,'simulation','sim_chaos_minimal.mat'), ...
    'eeg_sensor_pure', 'eeg_sensor', 'conn', '-v7.3');

%% simulate realistic EEG data in source space and sensor space
% load(fullfile(wd,'simulation','roi.mat'));
% nvertices = size(cortexmesh.Vertices,1);
nepochs = 9; nseg = 3;
iter = ceil(nepochs/nseg);
label = 'sen'; % sen or src
alpha = 0.01; % observational noise == snratio

for i = 1:iter
    for stat = 0:1 % 1 = exist iid noisy source/channel
        [eeg_pure, conn] = sim_eeg_pure_chaos(roi_elec,regidx,64,...
            fs,rec_len,nseg,1,1,stat,0.25,0.6,0.05);
        save(fullfile(wd,'simulation', ...
            ['sim_chaos_bn' num2str(stat) '_' label ...
            '_seg' num2str(i) '.mat']),'eeg_pure');
        
        if strcmp(label, 'src')
            eeg_sensor_pure = zeros(size(gain,1), size(eeg_pure,2), ...
                size(eeg_pure,3));
            for j = 1:nseg
                eeg_sensor_pure(:,:,j) = gain * squeeze(eeg_pure(:,:,j));
            end
        else
            eeg_sensor_pure = eeg_pure .* 1e4;
        end
        
        eeg_sensor_noise = randn(size(eeg_sensor_pure));
        for j = 1:nseg
            eeg_sensor_noise(:,:,j) = ...
                squeeze(eeg_sensor_noise(:,:,j)) ./ ...
                norm(squeeze(eeg_sensor_noise(:,:,j)), 'fro') .* 1e4;
        end
        eeg_sensor = (1-alpha)*eeg_sensor_pure + alpha*eeg_sensor_noise;
        
        save(fullfile(wd,'simulation',...
            ['sim_chaos_bn' num2str(stat) '_' label ...
            '_fin_seg' num2str(i) '.mat']), ...
            'eeg_sensor_pure', 'eeg_sensor', 'conn', '-v7.3');
    end
    fprintf('*');
end
fprintf('\n');

%% simulate a network directly
% connectivity: 0 - 1
% nodes: 64
% symmetric, node self-connection
nepochs = 9; nseg = 3;
iter = ceil(nepochs/nseg);
label = 'sen';
snratio = 0.01; % snratio == alpha
elec = readlocs(fullfile(wd, 'chanlocs', 'actiCap64Channel.xyz'));
prcint = [97 100];

for i = 1:iter
    for stat = 0:1
        if exist('conn','var') ~= 0, clear conn; end
        load(fullfile(wd,'simulation',...
            ['sim_chaos_bn' num2str(stat) '_' label ...
            '_fin_seg' num2str(i) '.mat']));
        
        tmp = triu(conn .* (rand(size(conn)).*0.4+0.6),1);
        conn_true = tmp + tmp';
        tmp = triu(rand(size(conn)),1);
        zidx = randi(numel(tmp),1,randi(round(numel(tmp)/4)));
        tmp(zidx) = 0;
        conn_noise = tmp + tmp';
        
        conn = (1-snratio)*conn_true + snratio*conn_noise;
        
%         figure;
%         connectome(conn,'none','chanlabel',{elec.labels},'visible','on',...
%             'lothresh',0.2, 'hithresh',1);
%         title('Ground Truth');

        save(fullfile(wd,'simulation',...
            ['sim_conn_bn' num2str(stat) '_' label ...
            '_seg' num2str(i) '.mat']), 'conn');
    end
end

%%
load(fullfile(wd,'simulation','sim_conn_bn0_sen_seg2.mat'));
figure;
connectome(conn,'none','chanlabel',{elec.labels},'visible','on',...
           'lothresh',0.1, 'hithresh',1);
title('Ground Truth');