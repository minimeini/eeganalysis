function [eeg_pure, conn, fullidx, iid_idx, pnt_weights] = ...
    sim_eeg_pure_chaos(roi, regidx, nvertices, fs, rec_len, nepochs, ...
    add_inter, add_intra, add_iid, varargin)
% function [eeg_pure, conn, fullidx, iid_idx, pnt_weights] = sim_eeg_pure_chaos(gain, cort_manifold, sigmas, centers, fs, rec_len, varargin)
%
% REQUIRED: roi, regidx, nbchan, fs, rec_len, nepochs, add_inter, add_intra, add_iid
% OPTIONAL: minpct_inter, minpct_intra, maxpct_iid, coupled, snratio
%% parsing
nargs = -nargin('sim_eeg_pure_chaos') - 1;
minpct_inter = 0.05; 
minpct_intra = 0.15; 
maxpct_iid = 0.01;
snratio = 0.1;
coupled = -1;

if nargin >= nargs + 1, minpct_inter = varargin{1}; end
if nargin >= nargs + 2, minpct_intra = varargin{2}; end
if nargin >= nargs + 3, maxpct_iid = varargin{3}; end
if nargin >= nargs + 4, coupled = varargin{4}; end
if nargin >= nargs + 5, snratio = varargin{5}; end

%% compute total points in the whole region
idx = [];
for i = 1:numel(roi)
    idx = [idx roi(i).idx];
end
idx = unique(idx);
npnts = numel(idx);

%% select number of inter-regional coupling and intra-regional coupling
rng('shuffle');
pnt_inter = min(numel(roi(regidx(1)).idx), numel(roi(regidx(2)).idx));
pct_inter = rand(1)*(1-minpct_inter) + minpct_inter;
num_inter = round(pct_inter * pnt_inter);

pnt_intra = [numel(roi(regidx(1)).idx)  numel(roi(regidx(2)).idx)];
pct_intra = rand(1,2).*(1-minpct_intra) + minpct_intra;
num_intra = round(pct_intra .* pnt_intra);

%% generate oscilate pairs (inter and intra)
oscilate_inter = []; idx_inter = [];
if add_inter == 1
    if coupled == -1
        ctmp = round(rand(1)*0.3+0.5,1);
    else
        ctmp = coupled;
    end
    [oscilate_inter, idx_inter] = sim_chaos_pair('inter', ...
        roi(regidx), num_inter, nepochs, fs, rec_len, ctmp);
end

oscilate_intra = []; idx_intra = [];
if add_intra == 1
    if coupled == -1
        ctmp = round(rand(1)*0.2+0.75,1);
    else
        ctmp = coupled;
    end
    [oscilate_intra_a, idx_intra_a] = sim_chaos_pair('intra', ...
        roi(regidx(1)), num_intra(1), nepochs, fs, rec_len, ctmp);
    
    if coupled == -1
        ctmp = round(rand(1)*0.2+0.75,1);
    else
        ctmp = coupled;
    end
    [oscilate_intra_b, idx_intra_b] = sim_chaos_pair('intra', ...
        roi(regidx(2)), num_intra(2), nepochs, fs, rec_len, ctmp);
    
    oscilate_intra = [oscilate_intra_a oscilate_intra_b];
    idx_intra = [idx_intra_a; idx_intra_b];
end

oscilate_pair = [oscilate_inter oscilate_intra];
fullidx = [idx_inter; idx_intra];

if ~isempty(fullidx)
    conn = ideal_conn(nvertices, fullidx);
    pnt_weights = nodes_weight(nvertices, fullidx, num_inter, add_intra);
else
    conn = [];
    pnt_weights = [];
end



%% generate oscilate single points
iid_idx = 0;
if add_iid == 1
    used_idx = unique(fullidx(:));
    unused_idx = setdiff(idx, used_idx);
    pct_iid = rand(1) * maxpct_iid;
    num_iid = round(numel(unused_idx) * pct_iid);
    
    [oscilate_single, iid_idx] = sim_chaos_single(unused_idx, ...
        num_iid, fs, rec_len, nepochs);
end

%% create pure eeg by integrating all oscilate activities
eeg_pure = zeros(nvertices, fs*rec_len, nepochs);

for i = 1:nvertices
    tmp = zeros(1, fs*rec_len, nepochs);
    
    if ~isempty(fullidx) && (add_inter+add_intra>0) % interactive source
        if ismember(i,unique(fullidx(:))) 
            for j = 1:pnt_weights(i).npair
                loc = find(fullidx(pnt_weights(i).pair_idx(j),:) == i, ...
                    1, 'first');
                tmp = tmp + ...
                    oscilate_pair(pnt_weights(i).pair_idx(j)).data(loc,:,:) ...
                    .* pnt_weights(i).weight(j);
            end
        end
    end
        
    if (add_iid==1) && ismember(i,iid_idx)  % independent source
        loc = find(iid_idx == i,1,'first');
        tmp = snratio .* oscilate_single(loc).data;
    end
    
    eeg_pure(i,:,:) = tmp;
end

end

%%
function [oscilate_pair, fullidx] = sim_chaos_pair(roi_type, roi, num_pair, nepochs, fs, rec_len, coupling)

global C;
C = coupling;

oscilate_pair = struct();
fullidx = zeros(num_pair, 2);
times = 0:(fs*rec_len-1);

for i = 1:num_pair
    rng('shuffle');
    
    % select  oscillate pairs
    isdup = 1;
    while isdup == 1
        switch roi_type
            case 'inter'
                if numel(roi) < 2
                    error('INPUT ERROR: not sufficient ROIs.');
                end
                tmpidx = [randi(numel(roi(1).idx),1) ...
                    randi(numel(roi(2).idx),1)];
                tmpidx = [roi(1).idx(tmpidx(1)) ...
                    roi(2).idx(tmpidx(2))];
        
                shuf = round(rand(1,1));
                if shuf == 1, tmpidx = fliplr(tmpidx); end
            case 'intra'
                if numel(roi) > 1
                    error('INPUT ERROR: too much ROIs.');
                end
                tmpidx = randi(numel(roi.idx),1,2);
                tmpidx = roi.idx(tmpidx);
            otherwise
                error('Invalid ROI types.');
        end
        
        isdup = any(arrayfun(@(x) isequal(fullidx(x,:),tmpidx), ...
            1:size(fullidx,1))) || (tmpidx(1)==tmpidx(2));
    end
    fullidx(i,:) = tmpidx;
    oscilate_pair(i).idx = tmpidx;

    % generate coupled time series
    eegtmp = zeros(2, fs*rec_len, nepochs);
    for j = 1:nepochs
        init = rand(1,6);
        [~,tmpx] = ode45('coupled_rossler',times,init);
        tmp = tmpx(:,[1 4])';
        tmp = tmp ./ norm(tmp, 'fro');
        eegtmp(:,:,j) = tmp;
    end
    oscilate_pair(i).data = eegtmp;
end

end

%%
function [oscilate_single, iid_idx] = sim_chaos_single(unused_idx, num_iid, fs, rec_len, nepochs)
oscilate_sys = {'Rossler', 'Lorentz'};
times = 0:(fs*rec_len-1);
oscilate_single = struct();

iid_idx = unused_idx(randi(numel(unused_idx), 1, num_iid));
for i = 1:num_iid
    oscilate_single(i).idx = iid_idx(i);
    eegtmp = zeros(1, fs*rec_len, nepochs);
    dice_method = randi(2,1); % 1=rossler, 2=lorenz
    for j = 1:nepochs
        rng('shuffle');
        init = rand(1,3);
        [~,tmpx] = ode45(oscilate_sys{dice_method},times,init);
        tmp = tmpx(:,1)';
        tmp = tmp ./ norm(tmp, 'fro');
        eegtmp(:,:,j) = tmp;
    end
    oscilate_single(i).data = eegtmp;
end
end

%%
function conn = ideal_conn(nbchan, fullidx)
conn = zeros(nbchan,nbchan);
for i = 1:size(fullidx,1)
    conn(fullidx(i,1),fullidx(i,2)) = 1;
    conn(fullidx(i,2),fullidx(i,1)) = 1;
end
end

%%
function weight_st = nodes_weight(nbchan, fullidx, num_inter, add_intra)
weight_st = struct();
for i = 1:nbchan
    idx = arrayfun(@(x) ismember(i,fullidx(x,:)),1:size(fullidx,1));
    weight_st(i).pair_idx = find(idx==1);
    weight_st(i).npair = numel(weight_st(i).pair_idx);
    if ~isempty(weight_st(i).pair_idx)
        weight_st(i).weight = zeros(1, weight_st(i).npair);
        remain_weight = 1; ninter = 0; 
        
        % weights for inter-region
        if any(weight_st(i).pair_idx <= num_inter)
            ninter = sum(weight_st(i).pair_idx <= num_inter);
            tmp_total = rand(1)*0.3 + 0.4;
            tmp = rand(1,ninter);
            tmp = tmp ./ sum(tmp) .* tmp_total;
            
            weight_st(i).weight(1:ninter) = tmp;
            remain_weight = 1 - sum(tmp);
        end
        
        % weights for intra-region        
        if add_intra == 1
            if any(weight_st(i).pair_idx > num_inter)
                nintra = sum(weight_st(i).pair_idx > num_inter);
                tmp = rand(1,nintra);
                tmp = tmp ./ sum(tmp) .* remain_weight;
                weight_st(i).weight(ninter+1:end) = tmp;
            end
        end
        
    else
        weight_st(i).weight = [];
    end
    
    weight_st(i).weight = weight_st(i).weight ./ ...
        sum(weight_st(i).weight);
end
end