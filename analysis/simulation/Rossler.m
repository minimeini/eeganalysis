function dx = Rossler(t,x)
dx = zeros(3, 1);

lambda = 6;
a = 0.2; b = 0.2; c = 5.7;

dx(1) = -lambda * (x(2) + x(3)); 
dx(2) = lambda * (x(1) + a*x(2));
dx(3) = lambda * (b*x(1) + x(3)*(x(1) - c));
end