function [ref, names, labels] = ref_DesikanKilliany()

names = {'prefrontal', 'frontal', 'central', 'parietal', ...
    'temporal', 'occipital', 'limbic'};
labels = {'LPF', 'RPF', 'LF', 'RF', 'LC', 'RC', 'LP', 'RP', ...
    'LT', 'RT', 'LO', 'RO', 'LL', 'RL'};

ref = { ...
    'bankssts', 'temporal';
    'caudalanteriorcingulate', 'limbic';
    'caudalmiddlefrontal', 'frontal';
    'cuneus', 'occipital';
    'entorhinal', 'temporal';
    'frontalpole', 'prefrontal';
    'fusiform', 'temporal';
    'inferiorparietal', 'parietal';
    'inferiortemporal', 'temporal';
    'insula', 'temporal';
    'isthmuscingulate', 'limbic';
    'lateraloccipital', 'occipital';
    'lateralorbitofrontal', 'prefrontal';
    'lingual', 'occipital';
    'medialorbitofrontal', 'prefrontal';
    'middletemporal', 'temporal';
    'paracentral', 'central';
    'parahippocampal', 'temporal';
    'parsopercularis', 'frontal';
    'parsorbitalis', 'prefrontal';
    'parstriangularis', 'frontal';
    'pericalcarine', 'occipital';
    'postcentral', 'central';
    'posteriorcingulate', 'limbic';
    'precentral', 'central';
    'precuneus', 'parietal';
    'rostralanteriorcingulate', 'limbic';
    'rostralmiddlefrontal', 'frontal';
    'superiorfrontal', 'frontal';
    'superiorparietal', 'parietal';
    'superiortemporal', 'temporal';
    'supramarginal', 'parietal';
    'temporalpole', 'temporal';
    'transversetemporal', 'temporal';
};

% npf = 4; if sum(ismember(ref(:,2),'prefrontal'))~=npf, error('errorParc');end
% nf = 5; if sum(ismember(ref(:,2),'frontal'))~=nf, error('errorParc');end
% nc = 3; if sum(ismember(ref(:,2),'central'))~=nc, error('errorParc');end
% np = 4; if sum(ismember(ref(:,2),'parietal'))~=np, error('errorParc');end
% nt = 10; if sum(ismember(ref(:,2),'temporal'))~=nt, error('errorParc');end
% no = 4; if sum(ismember(ref(:,2),'occipital'))~=no, error('errorParc');end
% nl = 4; if sum(ismember(ref(:,2),'limbic'))~=nl, error('errorParc');end
% 

end