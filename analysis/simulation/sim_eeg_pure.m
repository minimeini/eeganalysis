function [eeg_signal_int, eeg_signal_nonint, source_int, source_nonint] = ...
    sim_eeg_pure(gain, cort_manifold, roi, sigmas, centers, ...
    fs, rec_len, varargin)
% function [eeg_signal_int, eeg_signal_nonint] = sim_eeg_pure(gain, cort_manifold, sigmas, centers, fs, rec_len, varargin)
%
% REQUIRED: gain, cort_manifold, roi, sigmas, centers, fs, rec_len
% OPTIONAL: bandpass, method
%% parsing
nargs = 7;
bandpass = [8 13]; method = 'chaos'; % method: {'chaos', 'ar'}
if nargin >= nargs + 1, bandpass = varargin{1}; end
if nargin >= nargs + 2, method = varargin{2}; end

if numel(roi)~=numel(centers) || numel(centers)~=numel(sigmas)
    error('INPUT ERROR: numel(roi) == numel(centers) == numel(sigmas).');
end

nsource = numel(centers);
nvertice = size(cort_manifold.vc,1);

%% Generate Spatial Extent of Amplitudes of source activities
source_amp = zeros(nvertice, nsource);
for i = 1:nsource
    % generate Gaussian distributions on cortical manifold
    [~, source_amp(:,i)] = graphrbf(cort_manifold, ...
        sigmas(i), centers(i));
    
    % The amplitude at nodes located outside the seed ROI is set to zero.
    if ~ismember(centers(i), roi(i).pidx)
        error('INPUT ERROR: center must in the pickable index of ROI.');
    end
    out_roi = setdiff(1:nvertice, roi(i).idx);
    source_amp(out_roi,i) = 0;
    
    if sum(source_amp(:,i)>0) == 0
        error('CALCULATION ERROR: no source activity.');
    end
    
    % divided by the L2-norm
    source_amp(:,i) = source_amp(:,i) ./ norm(source_amp(:,i),2);
    
    if sum(isnan(source_amp(:,i))) ~= 0
    error('CALCULATION ERROR: NaN in source_amp.');
    end
end


%% Generate two interactive time series
switch method
    case 'ar'
        [source_int, source_nonint, ar_lag] = ...
            generate_sources_ar(fs, rec_len, bandpass);
    case 'chaos'
        init = rand(1,6);
        [t,x] = ode45('rossler_lorenz',times-min(times),init);
        source_int = x(:,[1 4])';
        [t,x1] = ode45('Rossler',times-min(times),init(1:3));
        [t,x2] = ode45('Lorentz',times-min(times),init(4:end));
        source_nonint = [x1(:,1) x2(:,1)]';
        clear x x1 x2
    otherwise
        error('INPUT ERROR: method must be ''ar'' or ''chaos''.');
end

%% Normalization
source_int = source_amp * source_int;
source_int = source_int ./ norm(source_int, 'fro');

source_nonint = source_amp * source_nonint;
source_nonint = source_nonint ./ norm(source_nonint, 'fro');

%% sources -> sensors
eeg_signal_int = gain * source_int;
if sum(reshape(isnan(eeg_signal_int), 1, [])) ~= 0
    error('CALCULATION ERROR: NAN in source signal normalization.');
end
eeg_signal_nonint = gain * source_nonint;
if sum(reshape(isnan(eeg_signal_nonint), 1, [])) ~= 0
    error('CALCULATION ERROR: NAN in ar source generation.');
end
end