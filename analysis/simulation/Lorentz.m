function dx = Lorentz(t, x)
dx = zeros(3, 1);

sigma = 10;
b = 8/3;
r = 28;

dx(1) = sigma*(x(2) - x(1)); 
dx(2) = r*x(1) - x(2) - x(1)*x(3);
dx(3) = x(1)*x(2) - b*x(3);
end