%% Initialization
clear all
wd = 'C:\Users\meini\OneDrive\eegAnalysis';
wd_fig = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd);

filepath = fullfile(wd, 'simulation');

%% coupled Colpitts
ntrials = 100; nvert = 9; npnts=500;
data_comp = zeros(nvert,npnts,ntrials);

%% Simulation
% 1. Execution
% Stop and try new initial condition if running time > 1 second
% 
% 2. ODE Definition
% > p64: the whole system contains 64 points
% >>> `coupled_colpitts_p64_r6`: each subregion contains 6 points
% >>> `coupled_colpitts_p64_r4`: each subregion contains 4 points
% >>> `coupled_colpitts_p64_r12`: each subregion contains 12 points

for s = 1:ntrials
    tic
    rng('shuffle'); init = rand(1,3*nvert);
    [~,x] = ode45('coupled_colpitts_p9_r4',(0:npnts-1+100)+3*1e6,init);
    
    for i = 1:nvert
        data_comp(i,:,s) = x(1+100:npnts+100,3*(i-1)+1) ...
            + 1i*x(1+100:npnts+100,3*(i-1)+2);
    end
    toc
end
%%
cidx = [];
for i = 1:nvert
    [ridx,ctmp] = find(isnan(squeeze(data_comp(i,:,:))));
    ctmp = unique(ctmp);
    cidx = unique([cidx ctmp]);
end
data_comp = data_comp(:,:,setdiff(1:size(data_comp,3),cidx));

%%
tmp = squeeze(data_comp(4,100:end,1));
figure;plot(imag(tmp));

%%
data_used = data_comp;

if isreal(data_used)
    data_comp2 = zeros(size(data_used));
    for c = 1:size(data_used,1)
        data_comp2(c,:,:) = cell2mat(arrayfun(@(t) ...
            hilbert(squeeze(data_used(c,:,t)))',...
            1:size(data_used,3),'UniformOutput',0));
    end
else
    data_comp2 = data_comp;
end
conn_nt = abs(connMatrix(data_comp2,'ciCOH'));
%%
save(fullfile(wd,'simulation','sim_chaos_colpitts_p64_r12.mat'),...
    'data_comp','conn_nt');
%%
conn_mean = mean(conn_nt,3);
conn_std = zeros(nvert,nvert);
for i = 1:(nvert-1)
    for j = i+1:nvert
        conn_std(i,j) = std(conn_nt(i,j,:));
        conn_std(j,i) = conn_std(i,j);
    end
end
disp(conn_mean);