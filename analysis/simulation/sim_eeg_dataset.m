function [signal_int, signal_nonint, signal_int_noise, ...
    signal_nonint_noise, noise, source_int, source_nonint] = ...
    sim_eeg_dataset(gain, cort_manifold, roi, ...
    sigmas, centers, fs, rec_len, nepochs, varargin)
%% parsing
narg = 8; snr_range = [0.1 0.9];
bandpass = [8 13]; snr = 0.1; noise_rate = 0.01;
if nargin >= narg + 1
    bandpass = varargin{1};
    if ~isnumeric(bandpass) || numel(bandpass)~= 2
        error('INPUT ERROR: bandpass should be [min max].');
    end
end
if nargin >= narg + 2
    snr = varargin{2};
    if snr<snr_range(1) || snr>snr_range(2)
        error('INPUT ERROR: snr should in the range of [0.1, 0.9].');
    end
end
if nargin >= narg + 3, noise_rate = varargin{3}; end

%% looping
fprintf('Progress: ')
for i = 1:nepochs
    tmp = [];
    
    [tmp.signal_int, tmp.signal_nonint, tmp.source_int, ...
        tmp.source_nonint] = sim_eeg_pure(gain, cort_manifold, roi, ...
        sigmas, centers, fs, rec_len, bandpass, noise_rate);
    
    if sum(reshape(isnan(tmp.signal_int), 1, [])) ~= 0
        error('CALCULATION ERROR: NAN in simulated signals.');
    end
    if sum(reshape(isnan(tmp.signal_nonint), 1, [])) ~= 0
        error('CALCULATION ERROR: NAN in simulated signals.');
    end

    tmp.signal_int_noise = sim_eeg_noise(tmp.signal_int, gain, fs, ...
        rec_len, bandpass, noise_rate, snr);
    [tmp.signal_nonint_noise, tmp.noise] = sim_eeg_noise(tmp.signal_nonint, ...
        gain, fs, rec_len, bandpass, noise_rate, snr);
    
    if i == 1
        signal_int = zeros([size(tmp.signal_int) nepochs]);
        signal_nonint = signal_int;
        
        signal_int_noise = signal_int;
        signal_nonint_noise = signal_int;
        noise = signal_int;
        
        source_int = zeros([size(tmp.source_int) nepochs]);
        source_nonint = source_int;
    end
    
    signal_int(:,:,i) = tmp.signal_int;
    signal_nonint(:,:,i) = tmp.signal_nonint;
    signal_int_noise(:,:,i) = tmp.signal_int_noise;
    signal_nonint_noise(:,:,i) = tmp.signal_nonint_noise;
    noise(:,:,i) = tmp.noise;
    source_int(:,:,i) = tmp.source_int;
    source_nonint(:,:,i) = tmp.source_nonint;
    
    if mod(i, 10) == 0
        fprintf('*');
    end
end
fprintf('\n'); 

end