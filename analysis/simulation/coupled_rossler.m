%% coupled Rossler coupling system
function dx = coupled_rossler(t,x)
% Eroglu et al., 2017; Cr_lowbnd = 2*abs(diff(omega))
% Cermeli et al., 2005
% C in (0.4, 0.8) for stable synchronization

Cr = 0.6;

dx = zeros(6,1);

alpha = 1;

a = 0.2;
b = 0.2;
c = 5.7;
omega = [0.99 0.95];
% omega = [1 1];

% system 1. x(1,2,3)
dx(1) = - alpha * (omega(1)*x(2) + x(3));
dx(2) = alpha * (omega(1)*x(1) + a*x(2));
dx(3) = alpha * (b*x(1) + x(3)*(x(1)-c));

% system 2. x(4,5,6)
dx(4) = - alpha * (omega(2)*x(5) + x(6) +  Cr*(x(1)-x(4)));
dx(5) = alpha * (omega(2)*x(4) + a*x(5));
dx(6) = alpha * (b*x(4) + x(6)*(x(4)-c));

end
