%% rossler - lorenz coupling system
function dx = rossler_lorenz(t,x)
% Modified version based on Carmeli et al., 2005
% C in [0, 1], 0==iid, 1==complete synchronization

C = 0.8;

dx = zeros(6,1);

alpha = 6; % alpha=1
ar = 0.2;
br = 0.2;
c = 5.7;

sigma = 10;
bl = 8/3;
r = 28;

dx(1) = -alpha * (x(2)+x(3));
dx(2) = alpha * (x(1) + ar*x(2));
dx(3) = alpha * (br*x(1) + x(3)*(x(1)-c));

dx(4) = sigma*(x(5) - x(4)); 
dx(5) = r*x(4) - x(5) - x(4)*x(6) + C*(x(2)-x(5)).^3; 
dx(6) = x(4)*x(5) - bl*x(6);
end
