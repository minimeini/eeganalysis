function dx = coupled_lorenz(t,x)

Cl = 6;

dx = zeros(6,1);

sigma = 10;
b = 8/3;
r = [26 30];

dx(1) = -sigma*x(1) + sigma*x(2);
dx(2) = -x(1)*x(3) + r(1)*x(1) - x(2);
dx(3) = x(1)*x(2) - b*x(3);

dx(4) = -sigma*x(4) + sigma*x(5);
dx(5) = -x(4)*x(6) + r(2)*x(4) - x(5) + Cl*(x(2)-x(5)).^3;
dx(6) = x(4)*x(5) - b*x(6);

end