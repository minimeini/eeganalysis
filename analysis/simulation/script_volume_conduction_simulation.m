%% Initialization
clear all
wd = 'C:\Users\meini\OneDrive\eegAnalysis';
wd_fig = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd);

filepath = fullfile(wd, 'simulation');
nseg = 3; nused = 3;
segidx = randi(nseg,1,nused);

elec = readlocs(fullfile(wd, 'chanlocs', 'actiCap64Channel.xyz'));
%% [VISUALIZATION] Source activations in the source space
load(fullfile(wd,'simulation',['sim_chaos_bn0_' label '_seg1.mat']));
data_src=eeg_pure;
data_src_aver = squeeze(mean(data_src, 3));

protocol = ['C:\Users\meini\OneDrive\eegAnalysis\database_bst\' ... 
    'Protocol01'];
cortexmesh = load(fullfile(protocol, 'anat\Subject01', ...
    'tess_cortex_spm_20484V.mat'));

source_ft = [];
source_ft.pos = cortexmesh.Vertices;
source_ft.tri = cortexmesh.Faces;
source_ft.inside = zeros(size(source_ft.pos,1),1);
% source_ft.time = times ./ 1000;
source_ft.pow = squeeze(mean(data_src_aver,2));
[ridx, ~] = find(source_ft.pow==0);
ridx = unique(ridx);
source_ft.inside(setdiff(1:size(source_ft.pos,1),ridx)) = 1;
source_ft.inside = logical(source_ft.inside);
source_ft.powdimord = 'pos';

cfg = [];
cfg.method = 'surface';
cfg.funparameter = 'pow';
cfg.maskparameter = cfg.funparameter;
cfg.surffile = fullfile(fullfile(protocol, 'anat\Subject01', ...
    'tess_cortex_spm_20484V.mat'));
ft_sourceplot(cfg, source_ft);

%% A pair of oscillators
npnts = 100; niter = 1000;
conn_iter = zeros(1,niter);
data_comp = zeros(2,npnts);

%% 1. coupled rosslers
for iter = 1:niter
    rng('shuffle'); init = rand(1,6); delta = randi(2);
    [~,x] = ode45('coupled_rossler',...
        (0:npnts-1+delta)+npnts+3*1e5+randi(npnts),init);
    data_comp(1,:) = x(1:npnts,1) + 1i*x(1:npnts,2);
    data_comp(2,:) = x(1+delta:end,4) + 1i*x(1+delta:end,5);

    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter); % 0.8032
conn_std = std(conn_iter); % 0.0211
%% 2. iid rosslers
for iter = 1:niter
    for i = 1:2
        rng('shuffle'); init = rand(1,3);
        [~,x] = ode45('Rossler',(0:npnts-1)+1e5,init);
        data_comp(i,:) = x(1:npnts,1) + 1i*x(1:npnts,2);
    end
    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter); % 0.6641
conn_std = std(conn_iter); % 0.2960

%% 3. Rossler-Lorenz
for iter = 1:niter
    rng('shuffle'); init = rand(1,6); 
    delta = randi(2); % or no delay-bmbedding
    [~,x] = ode45('rossler_lorenz',...
        (0:npnts-1+delta)+npnts+3*1e5+randi(npnts),init);
    data_comp(1,:) = x(1:npnts,1) + 1i*x(1:npnts,2);
    data_comp(2,:) = x(1+delta:end,4) + 1i*x(1+delta:end,5);

    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter); % 0.2214
conn_std = std(conn_iter); % 0.0.0433

%% iid Gaussians
for iter = 1:niter
    for i = 1:2
        rng('shuffle');
        data_comp(i,:) = (randn(1,npnts) + 1i*randn(1,npnts))/sqrt(2);
    end
    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter); % 0.0049
conn_std = std(conn_iter); % 0.0066

%% iid Gaussian - Lorenz/Rossler/coupled-rossler/rossler-lorenz
niter = 200;
conn_iter = zeros(1,niter);
data_comp = zeros(2,npnts);
for iter = 1:niter
    rng('shuffle');
    data_comp(1,:) = (randn(1,npnts) + 1i*randn(1,npnts))/sqrt(2);
    
    rng('shuffle'); init = rand(1,6);
    [~,x] = ode45('rossler_lorenz',(0:npnts-1)+1e5,init);
    data_comp(2,:) = x(1:npnts,4) + 1i*x(1:npnts,5);

    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter); 
% 0 for Rossler, 0 for Lorenz, 0 for coupled-Rosslers, 0 for rossler in
% rossler-lorenz, 0 for lorenz in rossler-lorenz

conn_std = std(conn_iter); 
% 0 for Rossler, 0 for Lorenz, 0 for coupled-Rosslers, 0 for rossler in
% rossler-lorenz, 0 for lorenz in rossler-lorenz

%% iid coupled-rossler/rossler-lorenz - coupled-rossler/rossler-lorenz
niter = 10;
conn_iter = zeros(1,niter);
data_comp = zeros(2,npnts);
for iter = 1:niter
    rng('shuffle'); init = rand(1,6);
    [~,x] = ode45('coupled_rossler',(0:npnts-1)+1e5,init);
    data_comp(1,:) = x(1:npnts,1) + 1i*x(1:npnts,2);
    
    rng('shuffle'); init = rand(1,6);
    [~,x] = ode45('rossler_lorenz',(0:npnts-1)+1e5,init);
    data_comp(2,:) = x(1:npnts,4) + 1i*x(1:npnts,5);

    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter(conn_iter>0)); 
disp(conn_mean);
% 0.70 for R(RL)-R(RL), 0.19 for R(RL)-L(RL), 0.12 for L(RL)-L(RL)
% 0 for R(CR)-R(RL)

conn_std = std(conn_iter(conn_iter>0)); 
disp(conn_std);
% 0.29 for R(RL)-R(RL), 0.09 for R(RL)-L(RL), 0.07 for L(RL)-L(RL)
% 0 for R(CR)-R(RL)

%% iid, 1 from coupled rossler, 2 from another= oupled rossler
niter = 10;
conn_iter = zeros(1,niter);
data_comp = zeros(2,npnts);
for iter = 1:niter
    rng('shuffle'); init = rand(1,6); delta = randi(2);
    [~,x] = ode45('coupled_rossler',...
        (0:npnts-1+delta)+3*1e5+randi(npnts),init);
    data_comp(1,:) = x(1:npnts,1) + 1i*x(1:npnts,2);
    
    rng('shuffle'); init = rand(1,6); delta = randi(2);
    [~,x] = ode45('coupled_rossler',...
        (0:npnts-1+delta)+3*1e5+randi(npnts),init);
    data_comp(2,:) = x(1:npnts,1) + 1i*x(1:npnts,2);

    conn = abs(connMatrix(data_comp,'ciCOH'));
    conn = mean(conn,3);
    conn_iter(iter) = conn(1,2);
end

conn_mean = mean(conn_iter); % 0.7944
conn_std = std(conn_iter); % 0.21

%% coupled Colpitts

% 1. zero time delay
ntrials = 100; nvert = 64; npnts=500;
data_comp = zeros(nvert,npnts,ntrials);
%%
for s = 1:ntrials
    tic
    rng('shuffle'); init = rand(1,3*nvert);
    [~,x] = ode45('coupled_colpitts_p64_r12',(0:npnts-1)+3*1e6,init);
    
    for i = 1:nvert
        data_comp(i,:,s) = x(1:npnts,3*(i-1)+1) + 1i*x(1:npnts,3*(i-1)+2);
    end
    toc
end
%%
cidx = [];
for i = 1:nvert
    [ridx,ctmp] = find(isnan(squeeze(data_comp(i,:,:))));
    ctmp = unique(ctmp);
    cidx = unique([cidx ctmp]);
end
data_comp = data_comp(:,:,setdiff(1:size(data_comp,3),cidx));

%%
data_used = data_comp;

if isreal(data_used)
    data_comp2 = zeros(size(data_used));
    for c = 1:size(data_used,1)
        data_comp2(c,:,:) = cell2mat(arrayfun(@(t) ...
            hilbert(squeeze(data_used(c,:,t)))',...
            1:size(data_used,3),'UniformOutput',0));
    end
else
    data_comp2 = data_comp;
end
conn_nt = abs(connMatrix(data_comp2,'ciCOH'));
%%
save(fullfile(wd,'simulation','sim_chaos_colpitts_p64_r12.mat'),...
    'data_comp','conn_nt');
%%
conn_mean = mean(conn_nt,3);
conn_std = zeros(nvert,nvert);
for i = 1:(nvert-1)
    for j = i+1:nvert
        conn_std(i,j) = std(conn_nt(i,j,:));
        conn_std(j,i) = conn_std(i,j);
    end
end
disp(conn_mean);
%% Phase Synchronization for Coupled Rossler Oscillators + Volume Conduction
snratio = 40; % additive gaussian noise

vc = 0.1; % volume conduction
gain_min = diag(diag(ones(5)));
gain_min(3,1) = vc; gain_min(3,3) = 1-vc;
gain_min(5,1) = vc; gain_min(5,5) = 1-vc;

ntrials = 20;
npnts = 100;
nvert = 5;
fs = 500;
data_real = zeros(nvert,npnts,ntrials);

for k = 1:ntrials
    rng('shuffle'); init = rand(1,6); delta = 1 + randi(2);
    [~,x] = ode45('rossler_lorenz',...
        (0:npnts-1+delta)+npnts+3*1e5+randi(npnts),init);
    data_real(1,:,k) = x(1:npnts,1);
    data_real(5,:,k) = x(1+delta:end,4);
    clear x

    rng('shuffle'); init = rand(1,6); delta = 1+ randi(2);
    [~,x] = ode45('coupled_lorenz',...
        (0:npnts-1+delta)+3*1e5+randi(npnts),init);
    data_real(2,:,k) = x(1:npnts,1);
    data_real(4,:,k) = x(1+delta:end,4);
    clear x
    
    rng('shuffle');
    iid_type = randi(3);
    switch lower(iid_type)
        case 1 % random
            data_real(3,:,k) = randn(1,npnts);
        case 2 % rossler
            init = rand(1,3);
            [~,x] = ode45('Rossler',(0:npnts-1)+3*1e5+randi(npnts),init);
            data_real(3,:,k) = x(:,1);
            clear x
        case 3 % lorenz
            init = rand(1,3);
            [~,x] = ode45('Lorentz',(0:npnts-1)+3*1e5+randi(npnts),init);
            data_real(3,:,k) = x(:,1);
            clear x
    end
    
    if snratio ~= -1
        data_real(:,:,k) = awgn(data_real(:,:,k),snratio,'measured');
    end
end

data_mix = zeros(size(data_real));
for i = 1:size(data_real,3)
    data_real(:,:,i) = data_real(:,:,i) ./ norm(data_real(:,:,i),'fro');
    data_mix(:,:,i) = gain_min * squeeze(data_real(:,:,i));
end

% project to x-plane
data_type = 'mix';
switch data_type
    case 'mix'
        data_used = data_mix;
    case 'real'
        data_used = data_real;
end

eeg = def_eegdata_eeglab(data_used,0:(1000/fs):(npnts*(1000/fs)-1),...
    elec(1:nvert),fs);
eeg = pop_eegfiltnew(eeg, 50, 100);
data_comp_filt = zeros(size(eeg.data));
for c = 1:size(eeg.data,1)
    data_comp_filt(c,:,:) = cell2mat(arrayfun(@(t) ...
        hilbert(squeeze(eeg.data(c,:,t)))', ...
        1:size(eeg.data,3),'UniformOutput',0));
end

conn_pure = abs(connMatrix(data_comp_filt,'ciCOH', 'diagfill', 0));
conn_pure_aver = mean(conn_pure,3);

% Phase Shuffling: significance test
% IID1 & IID2: reject if pval < 0.05
% FT1: conn = conn * (1 - pval
sur_test = {'IID1', 'IID2', 'FT1'};
conn_pure_trim = conn_pure;
for itest = 1:numel(sur_test)
    niter = 500;
    conn_pure_iter = zeros(nvert,nvert,niter);
    for iter = 1:niter
        data_pure_shuffle = genSurrogate(data_used,sur_test{itest});
        eeg = def_eegdata_eeglab(data_pure_shuffle,...
            0:(1000/fs):(npnts*(1000/fs)-1),elec(1:nvert),fs);
        eeg = pop_eegfiltnew(eeg, 50, 100);
        data_comp_shuffle = zeros(size(eeg.data));
        for c = 1:size(eeg.data,1)
            data_comp_shuffle(c,:,:) = cell2mat(arrayfun(@(t) ...
                hilbert(squeeze(eeg.data(c,:,t)))', ...
                1:size(eeg.data,3),'UniformOutput',0));
        end
        conn_pure_tmp = abs(connMatrix(data_comp_shuffle,...
            'ciCOH', 'diagfill', 0));
        conn_pure_iter(:,:,iter) = mean(conn_pure_tmp,3);
    end

    mean_iter = mean(conn_pure_iter,3);
    sigma_iter = sqrt(sum((conn_pure_iter - ...
        repmat(mean(conn_pure_iter,3),1,1,...
        size(conn_pure_iter,3))).^2,3) ./ ...
        (size(conn_pure_iter,3)-1));
    z_conn = abs(conn_pure_aver - mean_iter) ./ sigma_iter;
    pval_conn = zeros(size(z_conn));
    for i = 1:(nvert-1)
        for j = i+1:nvert
            pval_conn(i,j) = 2*normcdf(z_conn(i,j),0,1,'upper');
            pval_conn(j,i) = pval_conn(i,j);
        end
    end

    if contains(sur_test{itest},'IID')
        conn_pure_trim = conn_pure_trim .* ...
            repmat(1-pval_conn,1,1,size(conn_pure_trim,3));
    else
        conn_pure_trim = conn_pure_trim .* ...
            repmat(1-pval_conn,1,1,size(conn_pure_trim,3));
    end
end

disp(mean(conn_pure,3));
disp(mean(conn_pure_trim,3));

%
cnt = 1;
save(fullfile(wd,'ciCOH_sim','minimal',...
    ['conn_minimal_' num2str(vc) '_' num2str(snratio) ...
    '_' data_type '_seg' num2str(cnt) '.mat']),...
    'conn_pure','conn_pure_trim');


%% Phase Synchronization for Minimal Networks
% Intra Rossler: 7 ===== 8 ----- 6 ----- 7
% Intra Lorenz:  2 ===== 3 ----- 4 ----- 2
% Inter Rossler-Lorenz: 6 ===== 4
% Volume Conduction: 6 ..... 1, 9 ..... 4
% IID Noise: 1 - rand(random, lorenz), 9 - rand(random, rossler)
% super-imposed interaction: equal weights

scheme_name = 'scheme3';

ntrials = 20; npnts = 100; nvert = 9; fs = 500; tlen = 1000/fs;
data_real = zeros(nvert,npnts,ntrials);

conn_ideal = zeros(nvert,nvert);
conn_ideal(6,7) = 1; conn_ideal(7,8) = 1; conn_ideal(6,8) = 1;
% conn_ideal(4,8) = 1; conn_ideal(4,9) = 1; conn_ideal(8,9) = 1;
% conn_ideal(4,6) = 1;
conn_ideal = triu(conn_ideal) + triu(conn_ideal)';

% additive gaussian noise
snratio = 40; 
% volume conduction
vc = 0.1;

gain_min = diag(diag(ones(nvert)));
gain_min(1,6) = vc; gain_min(1,1) = 1-vc;
gain_min(2,3) = vc; gain_min(2,2) = 1-vc;
gain_min(7,8) = vc; gain_min(7,7) = 1-vc;
gain_min(9,4) = vc; gain_min(9,9) = 1-vc;
weights = zeros(1,nvert);

for k = 1:ntrials
    %% intra Rossler: 7 ===== 8, 6 ----- 7, 6 ----- 8
    % Coupled Rossler: 6 ----- 7
    rng('shuffle'); init = rand(1,6); delta = 1 + randi(2);
    [~,x] = ode45('coupled_lorenz',...
        (0:npnts-1+delta)+3*1e5+randi(npnts*nvert),init);
    data_real(6,:,k) = data_real(6,:,k) + ...
        x(1:npnts,1)'./norm(x(1:npnts,1),'fro') .* 3;
    weights(6) = weights(6)+3;
    data_real(7,:,k) = data_real(7,:,k) + ...
        x(1+delta:end,4)'./norm(x(1+delta:end,4),'fro');
    weights(7) = weights(7)+1;

    % Coupled Rossler: 7 ----- 8
    rng('shuffle'); init = rand(1,6); delta = 1 + randi(2);
    [~,x] = ode45('coupled_lorenz',...
        (0:npnts-1+delta)+3*1e5+randi(npnts*nvert),init);
    data_real(7,:,k) = data_real(7,:,k) + ...
        x(1:npnts,1)'./norm(x(1:npnts,1),'fro') .* 3;
    weights(7) = weights(7)+3;
    data_real(8,:,k) = data_real(8,:,k) + ...
        x(1+delta:end,4)'./norm(x(1+delta:end,4),'fro');
    weights(8) = weights(8)+1;
    
    % Coupled Rossler: 6 ----- 8
    rng('shuffle'); init = rand(1,6); delta = 1 + randi(2);
    [~,x] = ode45('coupled_lorenz',...
        (0:npnts-1+delta)+3*1e5+1*npnts+randi(npnts*nvert),init);
    data_real(6,:,k) = data_real(6,:,k) + ...
        x(1:npnts,1)'./norm(x(1:npnts,1),'fro');
    weights(6) = weights(6)+1;
    data_real(8,:,k) = data_real(8,:,k) + ...
        x(1+delta:end,4)'./norm(x(1+delta:end,4),'fro') .* 3;
    weights(8) = weights(8)+3;
    
%     %% Coupled Lorenz: 2 ===== 3 ----- 4, 2 ----- 4
%     % Coupled Lorenz: 2 ----- 3
%     rng('shuffle'); init = rand(1,6); delta = 1+ randi(2);
%     [~,x] = ode45('coupled_lorenz',...
%         (0:npnts-1+delta)+3*1e5+2*npnts+randi(npnts*nvert),init);
%     data_real(4,:,k) = data_real(4,:,k) + ...
%         x(1:npnts,1)'./norm(x(1:npnts,1),'fro');
%     weights(4) = weights(4)+1;
%     data_real(8,:,k) = data_real(8,:,k) + ...
%         x(1:npnts,4)'./norm(x(1:npnts,4),'fro') .* 3;
%     weights(8) = weights(8)+3;
%     
%     % Coupled Lorenz: 3 ----- 4
%     rng('shuffle'); init = rand(1,6); delta = 1+ randi(2);
%     [~,x] = ode45('coupled_lorenz',...
%         (0:npnts-1+delta)+3*1e5+3*npnts+randi(npnts*nvert),init);
%     data_real(8,:,k) = data_real(8,:,k) + ...
%         x(1:npnts,1)'./norm(x(1:npnts,1),'fro');
%     weights(8) = weights(8)+1;
%     data_real(9,:,k) = data_real(9,:,k) + ...
%         x(1:npnts,4)'./norm(x(1:npnts,4),'fro') .* 3;
%     weights(9) = weights(9)+3;
% 
%     % Coupled Lorenz: 2 ----- 4
%     rng('shuffle'); init = rand(1,6); delta = 1+ randi(2);
%     [~,x] = ode45('coupled_lorenz',...
%         (0:npnts-1+delta)+3*1e5+4*npnts+randi(npnts*nvert),init);
%     data_real(9,:,k) = data_real(9,:,k) + ...
%         x(1:npnts,1)'./norm(x(1:npnts,1),'fro') .* 3;
%     weights(9) = weights(9)+3;
%     data_real(4,:,k) = data_real(4,:,k) + ...
%         x(1:npnts,4)'./norm(x(1:npnts,4),'fro');
%     weights(4) = weights(4)+1;
%     
%     %% Rossler-Lorenz: 6 ===== 4
%     rng('shuffle'); init = rand(1,6); delta = 1 + randi(2);
%     [~,x] = ode45('rossler_lorenz',...
%         (0:npnts-1+delta)+3*1e5+5*npnts+randi(npnts*nvert),init);
%     data_real(6,:,k) = data_real(6,:,k) + ...
%         x(1:npnts,1)'./norm(x(1:npnts,1));
%     weights(6) = weights(6)+1;
%     data_real(4,:,k) = data_real(4,:,k) + ...
%         x(1+delta:end,4)'./norm(x(1+delta:end,4));
%     weights(4) = weights(4)+1;
% %     
    %% IID Noise: 1 - rand(random, lorenz), 9 - rand(random, rossler)
    iidlist = [1 2 3 4 5 9];
    
    for nidx = 1:numel(iidlist)
        rng('shuffle');
        tmp = randn(1,npnts);
        data_real(iidlist(nidx),:,k) = data_real(iidlist(nidx),:,k) + ...
            tmp./norm(tmp,'fro');
        weights(iidlist(nidx)) = weights(iidlist(nidx))+1;
    end

    
    %%
    data_real(:,:,k) = data_real(:,:,k) ./ repmat(weights',1,npnts);
    if snratio ~= -1
        data_real(:,:,k) = awgn(data_real(:,:,k),snratio,'measured');
    end
end

data_mix = zeros(size(data_real));
for i = 1:size(data_real,3)
    data_mix(:,:,i) = gain_min * squeeze(data_real(:,:,i));
end

data_type = 'mix';
switch data_type
    case 'mix'
        data_used = data_mix;
    case 'real'
        data_used = data_real;
end

eeg = def_eegdata_eeglab(data_used,0:tlen:(npnts*tlen-1),...
    elec(1:nvert),fs);
eeg = pop_eegfiltnew(eeg, 50, 100);
data_comp_filt = zeros(size(eeg.data));
for c = 1:size(eeg.data,1)
    data_comp_filt(c,:,:) = cell2mat(arrayfun(@(t) ...
        hilbert(squeeze(eeg.data(c,:,t)))', ...
        1:size(eeg.data,3),'UniformOutput',0));
end

conn_pure = abs(connMatrix(data_comp_filt,'ciCOH', 'diagfill', 0));
conn_pure_aver = mean(conn_pure,3);

%% Phase Shuffling: significance test
% IID1 & IID2: significant if pval < 0.1
% FT1: conn = conn * (1 - pval

data_used = data_comp;
conn_pure = conn_nt;

niter = 200;
sur_test = {'IID1', 'IID2', 'FT1'};
conn_pure_aver = mean(conn_pure,3);
conn_pure_trim = conn_pure;
z_score_trim = zeros(size(conn_pure));

for itest = 1:numel(sur_test)
    conn_pure_iter = zeros(nvert,nvert,niter);
    for iter = 1:niter
        data_pure_shuffle = genSurrogate(data_used,sur_test{itest});
        if isreal(data_pure_shuffle)
            eeg = def_eegdata_eeglab(data_pure_shuffle,...
                0:tlen:(tlen*npnts-1),elec(1:nvert),fs);
            eeg = pop_eegfiltnew(eeg, 50, 100);
            data_comp_shuffle = zeros(size(eeg.data));
            for c = 1:size(eeg.data,1)
                data_comp_shuffle(c,:,:) = cell2mat(arrayfun(@(t) ...
                    hilbert(squeeze(eeg.data(c,:,t)))', ...
                    1:size(eeg.data,3),'UniformOutput',0));
            end
        else
            data_comp_shuffle = data_pure_shuffle;
        end
        conn_pure_tmp = abs(connMatrix(data_comp_shuffle,'ciCOH'));
        conn_pure_iter(:,:,iter) = mean(conn_pure_tmp,3);
    end

    mean_iter = mean(conn_pure_iter,3);
    sigma_iter = sqrt(sum((conn_pure_iter-repmat(mean(conn_pure_iter,3),1,1,...
        size(conn_pure_iter,3))).^2,3)./(size(conn_pure_iter,3)-1));
    z_conn = abs(conn_pure_aver - mean_iter) ./ sigma_iter;
    pval_conn = zeros(size(z_conn));
    for i = 1:(nvert-1)
        for j = i+1:nvert
            pval_conn(i,j) = 2*normcdf(z_conn(i,j),0,1,'upper');
            pval_conn(j,i) = pval_conn(i,j);
        end
    end

    if contains(sur_test{itest},'IID')
        conn_pure_trim = conn_pure_trim .* ...
            repmat(1-pval_conn,1,1,size(conn_pure_trim,3));
    else
        conn_pure_trim = conn_pure_trim .* ...
            repmat(1-pval_conn,1,1,size(conn_pure_trim,3));
    end
    disp(mean(conn_pure_trim,3));
end

disp(mean(conn_pure,3));
% disp(mean(conn_pure_trim,3));

%
% save(fullfile(wd,'ciCOH_sim','minimal_network',...
%     ['conn_minnetwork_' num2str(vc) '_' num2str(snratio) ...
%     '_' data_type '_' scheme_name '.mat']),...
%     'conn_pure','conn_pure_trim','conn_ideal');

%% paste schemes
% 100 = 4 * 25; 1 = 26:50
conn_paste = zeros(nvert,nvert,npnts);
for i = 1:4
    tmp = load(fullfile(wd,'ciCOH_sim','minimal_network',...
        ['conn_minnetwork_0.1_40_mix_scheme' num2str(i) '.mat']));
    conn_paste(:,:,25*(i-1)+1:25*i) = tmp.conn_pure_trim(:,:,26:50);
end

save(fullfile(wd,'ciCOH_sim','minimal_network',...
    'conn_minnetwork_0.1_40_mix_switchScheme.mat'),'conn_paste');