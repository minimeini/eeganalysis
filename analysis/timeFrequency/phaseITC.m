function itpc = phaseITC(data_comp)

itpc = angle(mean(data_comp ./ abs(data_comp),4));

end