function ers = event_related_spectral(data_comp,varargin)
% function ersp = event_related_spectral(data_comp,varargin)
%
% Required Input:
%   data_comp: complex-value 4d array, channels * freqs * times * trials
%
% Optional Optional:
%   baseline: 1d array contains 2 elements, [start_idx, end_idx]
%
% Output:
%   ersp: real-value 3d array, channels * freqs * times
%
% Algorithm:
%   1. ERS: if baseline is not provided, 
%           ERS(f,t) = mean(abs(data_comp).^2,4)
%   2. ERSP: if baseline is provided, 
%           ERSP(f,t) = 10*log10(ERS(f,t) ./ baseERS)

nargs = -nargin('event_related_spectral') - 1;
baseline = [];
if nargin >= nargs + 1, baseline = varargin{1}; end

nchans = size(data_comp,1);
nfreqs = size(data_comp,2);
ntimes = size(data_comp,3);
ntrial = size(data_comp,4);

ers = mean(abs(data_comp).^2, 4);
if ~isempty(baseline)
    baseERS = mean(mean(abs(data_comp(:,:,baseline(1):baseline(2),:)),4),3);
    baseERS = repmat(baseERS,1,1,ntimes);
    ers = 10*log10(ers ./ baseERS);
end

end