%% Initialization
clear all
wd = 'C:\Users\meini\OneDrive\eegAnalysis';
wd_fig = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd);

%% [deprecated]time-frequency decomposition
% complex morlet wavelet
% # of cycles: number of local minima/maxima in a wavelet                 
% wintime (ms) = winsize * 1000 / srate;
% winsize (# of sample pnts) =(approx) srate * cycles(1) / frex(1)

% freqs: 8 - 12 Hz (linear spaced), 37 points, interval length = 1 Hz
% cycles: 3 - 4 (log spaced), 37 points
% time pnts: [-1121.0 119.0]ms, 450 points, interval lenghth = 2 ms

frex = linspace(5, 30, 40);
cycles = logspace(log10(2), log10(4), 40);
ntimesout = 900;

foldern = 'rejica_car_rejbadchan';
[tasklab,~,foldername] = getFilename(fullfile(wd_prep, foldern));
filelist = getFilename(foldername{1}, '*.set');
savepath = fullfile(wd_prep, 'preprocessed_waveleteeg');

for t = 1:numel(tasklab)
    savep = fullfile(savepath, tasklab{t});
    checkPath(savep);
    
    for i = 1:numel(filelist) % subject 28, i = 12
        fname = regexp(filelist{t}, '\_', 'split');
        fname = fname{1};
        
        eeg = pop_loadset(fullfile(foldername{t}, filelist{i}));
        eeg.data = eeg.data - ...
            repmat(mean(eeg.data(:,eeg.times>=-1500&eeg.times<=-500,:),2),...
            1,numel(eeg.times),1);
        
        if exist('data','var') == 1, clear data; end
        for c = 1 : eeg.nbchan
            [data(c, :, :, :), freqs, times] = ...
                timefreq(squeeze(eeg.data(c, :, :)), eeg.srate, ...
                'cycles', cycles, 'freqs', frex, ...
                'freqscale', 'linear', 'ntimesout', ntimesout, ...
                'tlimits', [eeg.xmin * 1000 eeg.xmax * 1000]);
        end
        
        idx_zero = knnsearch(times',-500);
        ersp = event_related_spectral(data,[1 idx_zero]);
        
        roi = [34 35 12 14 42 18 21 23];
        roi_label = {'FC3','FC4','C3','C4','CP3','CP2','P3','P4'};
        for r = 1:numel(roi)
            imagesc(squeeze(ersp(roi(r),:,:)));
            colorbar;
            xlabel('Time (ms)');
            ylabel('Frequency (Hz)');
            xticklabels({num2str(times(100)),num2str(times(200)),...
                num2str(times(300)),num2str(times(400)),...
                num2str(times(500)),num2str(times(600))});
            print(gcf,fullfile(wd,'visualization','ersp_sub28',['ersp_sub28_' roi_label{r} '.png']),'-dpng','-r300');
            close all
            delete(gcf);
        end
        
        save(fullfile(savep, [fname '.mat']), 'data', 'freqs', 'times');        
    end
end

