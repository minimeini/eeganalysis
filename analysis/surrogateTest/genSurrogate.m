function [surdat] = genSurrogate(data, method)

nbchan = size(data,1);
surdat = data;
switch method
    case 'IID1' 
        % IID1 surrogates are realizations of mutually independent IID
        % stochastic process (white noises) with the same mean, variance
        % and histogram as the series under study.
        % Construction:
        % The elements of the original series are randomly permutated in
        % temporal order, in each realization different random permutations
        % are used for the two components of the bicariate series.
        % Null Hypothesis: independent white noises.
        temp = cell2mat(arrayfun(@(x) ...
            rankShuffle(squeeze(data(x,:,:)))', ...
            1:nbchan,'UniformOutput',0));
        surdat = permute(reshape(temp, size(temp,1), ...
            [], nbchan), [3 2 1]);
    case 'FT1'
        % FT1 surrogates are independently generated for each of the two
        % components of the studied bivariate series as realizations of
        % linear stochastic processes with the same sample power spectra as
        % the series under study.
        temp = cell2mat(arrayfun(@(x) ...
            phaseShuffle(squeeze(data(x,:,:)))', ...
            1:nbchan,'UniformOutput',0));
        surdat = permute(reshape(temp, size(temp,1), ...
            [], nbchan), [3 2 1]);
    case 'IID2'
        % IID2 surrogates are realizations of IID stochastic processes 
        % (white noises), which count for possible cross-dependence between
        % the two components of the bivariate series.
        % Construction:
        % In each realization, the same random permutation is used for both
        % components of the bivariate series
        % Null Hypothesis:
        % Mutually (spectral) dependent white noises
        [surdat(1,:,:), idx] = ...
            rankShuffle(squeeze(data(1,:,:))); 
        temp = cell2mat(arrayfun(@(x) ...
            rankShuffle(squeeze(data(x,:,:)),idx)', ...
            2:nbchan, 'UniformOutput', 0));
        surdat(2:nbchan,:,:) = ...
            permute(reshape(temp, size(temp,1), ...
            [], nbchan-1), [3 2 1]);
    case 'FT2'
        [surdat(1,:,:), ph_rnd] = ...
            phaseShuffle(squeeze(data(1,:,:))); 
        temp = cell2mat(arrayfun(@(x) ...
            phaseShuffle(squeeze(data(x,:,:)),ph_rnd)', ...
            2:nbchan,'UniformOutput',0));
        surdat(2:nbchan,:,:) = ...
            permute(reshape(temp, size(temp,1), ...
            [], nbchan-1), [3 2 1]);
end

end