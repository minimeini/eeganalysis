function meanps = pairedSurrogate(eeg, method, metric, freqband, combina)
meanps = zeros(1, size(combina,1));

switch method
    case 'rank'
        tic
        [eeg.data(1,:,:), idx] = rankShuffle(squeeze(eeg.data(1,:,:))); 
        temp = cell2mat(arrayfun(@(x) ...
            rankShuffle(squeeze(eeg.data(x,:,:)),idx)', ...
            2:size(eeg.data,1),'UniformOutput',0));
        eeg.data(2:eeg.nbchan,:,:) = permute(reshape(temp, size(temp,1), ...
            [], eeg.nbchan-1), [3 2 1]);
        toc
    case 'phase'
        [eeg.data(1,:,:), ph_rnd] = phaseShuffle(squeeze(eeg.data(1,:,:))); 
        temp = cell2mat(arrayfun(@(x) ...
            phaseShuffle(squeeze(eeg.data(x,:,:)),ph_rnd)', ...
            2:size(eeg.data,1),'UniformOutput',0));
        eeg.data(2:eeg.nbchan,:,:) = permute(reshape(temp, size(temp,1), ...
            [], eeg.nbchan-1), [3 2 1]);
end

eeg = pop_eegfiltnew(eeg, freqband(1), freqband(2));

eeghilb = zeros(size(eeg.data));
for i = 1:size(eeg.data,1)
    eeghilb(i,:,:) = cell2mat(arrayfun(@(t) ...
        hilbert(squeeze(eeg.data(i,:,t)))', ...
        1:size(eeg.data,3),'UniformOutput',0));
end

for i = 1:size(combina,1)
    chants1 = squeeze(eeghilb(combina(i,1),:,:));
    chants2 = squeeze(eeghilb(combina(i,2),:,:));
    
    switch metric
        case 'PLV'
            pairconn = phaseLockValue(chants1, chants2, 0);
        case 'wPLI'
            pairconn = weightedPhaseLagIndex(chants1, chants2, 0);
        otherwise
            error('invalid metric.\n');
    end
    meanps(i) = mean(pairconn);
end

end