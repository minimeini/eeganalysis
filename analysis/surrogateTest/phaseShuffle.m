function [randts, ph_rnd] = phaseShuffle(chants, varargin)
p = inputParser;
addRequired(p, 'chants', @isnumeric);
addOptional(p, 'ph_rnd', [], @(x) isnumeric(x)||isempty(x));
parse(p, chants, varargin{:});

[nbins, ntrials] = size(p.Results.chants); % pnts * trials
if rem(nbins, 2) ~= 0
    nbins = nbins-1;
end
chants = p.Results.chants(1:nbins,:);
if isreal(chants)
    chanTS = fft(chants, nbins, 1);
else
    chanTS = chants;
end
interv1 = 2:nbins/2;
interv2 = nbins/2+2:nbins;

if isempty(p.Results.ph_rnd)
    rng('shuffle');
    ph_rnd = rand([numel(interv1) 1]);
else
    ph_rnd = p.Results.ph_rnd;
end
ph_interv1 = repmat(exp(2*pi*1i*ph_rnd), 1, ntrials);
ph_interv2 = conj(flipud(ph_interv1)); 

% preserve conjugate symmetry in Fourier spectrum
% fft at 1, nbins/2 + 1 is real
randts = zeros(nbins,ntrials);
randts(1,:) = chanTS(1,:);
randts(interv1,:) = chanTS(interv1,:) .* ph_interv1;
randts(nbins/2+1,:) = chanTS(nbins/2+1,:);
randts(interv2,:) = chanTS(interv2,:) .* ph_interv2;

if isreal(chants)
    randts = real(ifft(randts, nbins, 1));
end

end
