function [randts, idx] = rankShuffle(chants, varargin)
p = inputParser;
addRequired(p, 'chants', @isnumeric);
addOptional(p, 'idx', [], @(x) isempty(x)||isnumeric(x));
parse(p, chants, varargin{:});

if isempty(p.Results.idx)
    rng('shuffle');
    randts = rand(1, size(p.Results.chants,1));
    [~, idx] = sort(randts);
else
    idx = p.Results.idx;
end

randts = zeros(size(chants));
for i = 1:size(chants,2)
    temp = squeeze(chants(:,i));
    randts(:,i) = temp(idx);
end
end