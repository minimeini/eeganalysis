function meanps = iidSurrogate(eeg, method, metric, freqband, combina)
% shuffling, elapsed time ~= 1s
tic
switch method
    case 'rank'
        temp = cell2mat(arrayfun(@(x) ...
            rankShuffle(squeeze(eeg.data(x,:,:)))', ...
            1:size(eeg.data,1),'UniformOutput',0));
        eeg.data = permute(reshape(temp, size(temp,1), ...
            [], size(eeg.data,1)), [3 2 1]);
    case 'phase'
        temp = cell2mat(arrayfun(@(x) ...
            phaseShuffle(squeeze(eeg.data(x,:,:)))', ...
            1:size(eeg.data,1),'UniformOutput',0));
        eeg.data = permute(reshape(temp, size(temp,1), ...
            [], size(eeg.data,1)), [3 2 1]);
end
toc
% narrow band-pass filter & hilbert transform
% elapsed time ~= 0.6s
eeg = pop_eegfiltnew(eeg, freqband(1), freqband(2));
eeghilb = zeros(size(eeg.data));
for i = 1:size(eeg.data,1)
    eeghilb(i,:,:) = cell2mat(arrayfun(@(t) ...
        hilbert(squeeze(eeg.data(i,:,t)))', ...
        1:size(eeg.data,3),'UniformOutput',0));
end

% calculate phase synchronization and test statistic
% elapsed time ~= 7.5s
meanps = zeros(1, size(combina,1));
for i = 1:size(combina,1)
    chants1 = squeeze(eeghilb(combina(i,1),:,:));
    chants2 = squeeze(eeghilb(combina(i,2),:,:));
    switch metric
        case 'PLV'
            pairconn = phaseLockValue(chants1, chants2, 0);
        case 'wPLI'
            pairconn = weightedPhaseLagIndex(chants1, chants2, 0);
    end
    meanps(i) = mean(pairconn);
end

end

