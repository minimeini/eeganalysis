function conn = connSeed(compData, seedIdx, metric, varargin)
% function [conn] = connSeed(compData, seedIdx, metric)
% -------------------------------------------------------------------------
% >>> INPUTS
%
% compData   [required]          complex-value ndarray, [chan*time*trial] 
%                                or [chan*freq*time*trial]
% seedIdx    [required]          integer value, the index of seed
% metric     [required]          string, PLV | dPLV | parPLV | wPLI | ...
%                                dwPLI | iCOH | ciCOH | msCOH
%                                (insensitive to case)
%
% conn       [output]            real-value ndarray
%                                size: [chan*time] or [chan*freq*time]
%
% -------------------------------------------------------------------------
% >>> USAGE
%
% conn = connSeed(compData, 1, 'PLV');
%
% -------------------------------------------------------------------------
% >>> Available Methods
%
% 1. phase locking value (PLV): `help phaseLockValue`
% 2. debiased phase locking value (dPLV): `help phaseLockValue`
% 3. partial phase locking value (parPLV): `help partialPLV`
% 4. weighted phase lag index (wPLI): `help weightedPhaseLagIndex`
% 5. debiased weighted phase lag index (dwPLI): `help weightedPhaseLagIndex`
% 6. imaginary part of coherency (iCOH): `help imagCoherency`
% 7. corrected imaginary part of coherency (ciCOH): `help imagCoherency`




end