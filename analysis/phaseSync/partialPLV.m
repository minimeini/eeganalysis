function [parConnMat] = partialPLV(connMat)
% function [parConnMat] = partialPLV(connMat)
%
% Required Input
% - connMat: real value 2D PLV matrix, [nchan * nchan]
%
% Output
% - parConnMat: real value 2D adjacency matrix, [chan * chan]
%
% Method
% - Partial phase locking value (parPLV) tries to mitigate the volume 
% conduction effect in phase locking value (PLV). PLV must be computed
% first <==> the input connectivity matrix should be measured by PLV.
%
% Notice
% - No time dimension. If you have a time series of PLV connectivity
% matrices, and you want to get a time series of parPLV connectivity
% matrices, you should run this function on every time point.

connMat = squeeze(connMat);
if numel(size(connMat)) ~= 2 || size(connMat,1) ~= size(connMat,2)
    error('INPUT ERROR: connMat must be a nbchan * nbchan adjacency matrix.');
end
if cond(connMat) > 100
    fprintf('Condition Number: %d.\n', cond(connMat));
    warning('CALCULATION ERROR: connMat might be singular');
end

invConn = pinv(connMat);
autoInv = diag(invConn);
delim = autoInv * autoInv';
parConnMat = real(abs(invConn) ./ sqrt(delim));
parConnMat(isinf(parConnMat)) = 0;

end