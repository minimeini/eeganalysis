function [conn] = connMatrix(compData, metric, varargin)
% function [conn] = connMatrix(compData, metric)
% -------------------------------------------------------------------------
% >>> INPUTS
%
% compData   [required]          complex-value ndarray, [chan*time*trial] 
%                                or [chan*freq*time*trial]
% metric     [required]          string, PLV | dPLV | parPLV | wPLI | ...
%                                dwPLI | iCOH | ciCOH | msCOH
%                                (insensitive to case)
% diagfill   [name-value pair]   numeric, fill all the diagonal elements of 
%                                connectivity matrix with given value,
%                                (default = 0)
%
% conn       [output]            adjacency matrix, 
%                                size: [chan*chan*time] or [chan*chan*freq*time]
%
% -------------------------------------------------------------------------
% >>> USAGE
%
% conn = connMatrix(compData, 'PLV', 'diagfill', 0);
%
% -------------------------------------------------------------------------
% >>> Available Methods
%
% 1. phase locking value (PLV): `help phaseLockValue`
% 2. debiased phase locking value (dPLV): `help phaseLockValue`
% 3. partial phase locking value (parPLV): `help partialPLV`
% 4. weighted phase lag index (wPLI): `help weightedPhaseLagIndex`
% 5. debiased weighted phase lag index (dwPLI): `help weightedPhaseLagIndex`
% 6. imaginary part of coherency (iCOH): `help imagCoherency`
% 7. corrected imaginary part of coherency (ciCOH): `help imagCoherency`
% 8. magnitude-squared coherence (msCOH): help `mscohere`
%
% For a pair of gaussian random noise, we have, 
%     msCOH = PLV = 1, iCOH = ciCOH = parPLV = 0
%
% For a pair of coupled rossler-lorenz oscillators, we have,
%
% -------------------------------------------------------------------------
% >>> MISC
% 
% ts_comp = squeeze(compData(i,:,:)); % ts_comp: time*trial*(freq)

%%
p = inputParser;
addRequired(p, 'compData');
addRequired(p, 'metric', @(x) any(validatestring(upper(x), ...
    {'PLV', 'DPLV', 'PARPLV', 'WPLI', 'DWPLI', 'ICOH', 'CICOH', 'MSCOH'})));
addParameter(p, 'diagfill', 0, @isnumeric);
parse(p, compData, metric, varargin{:});

compData = p.Results.compData;
nbchan = size(compData, 1);
if numel(size(compData)) >= 3
    ntime = size(compData, numel(size(compData))-1);
else
    ntime = size(compData, 2);
end
if numel(size(compData)) == 4
    nfreq = size(compData,2);
    conn = zeros(nbchan, nbchan, nfreq, ntime);
else
    conn = zeros(nbchan, nbchan, ntime);
end

for i = 1:nbchan
    if i == nbchan, break; end
    for j = i+1:nbchan
        ts_comp1 = squeeze(compData(i,:,:));
        ts_comp2 = squeeze(compData(j,:,:));
        switch upper(p.Results.metric)
            case 'PLV'
                conn(i,j,:) = phaseLockValue(ts_comp1, ts_comp2, 0);
            case 'PARPLV'
                conn(i,j,:) = phaseLockValue(ts_comp1, ts_comp2, 0);
            case 'DPLV'
                conn(i,j,:) = phaseLockValue(ts_comp1, ts_comp2, 1);
            case 'WPLI'
                conn(i,j,:) = weightedPhaseLagIndex(ts_comp1, ts_comp2, 0);
            case 'DWPLI'
                conn(i,j,:) = weightedPhaseLagIndex(ts_comp1, ts_comp2, 1);
            case 'ICOH'
                conn(i,j,:) = imagCoherency(ts_comp1, ts_comp2, 0);
            case 'CICOH'
                conn(i,j,:) = imagCoherency(ts_comp1, ts_comp2, 1);
            case 'MSCOH'
                % mscohere operates column-wise
                conn(i,j, :) = msCoherence(ts_comp1, ts_comp2);
            otherwise
        end
        conn(j,i,:) = conj(conn(i,j,:));
    end
end

if strcmpi(p.Results.metric, 'PARPLV')
    for t = 1:size(conn,3)
        conn(:,:,t) = partialPLV(squeeze(conn(:,:,t)));
    end
end

for i = 1:size(conn,1)
    if numel(size(conn)) == 3
        conn(i,i,:) = p.Results.diagfill;
    elseif numel(size(conn)) == 4
        conn(i,i,:,:) = p.Results.diagfill;
    else
        error('COMPUTATION ERROR: invalid dimension of conn.');
    end
end


end