function [pairconn] = weightedPhaseLagIndex(ts_comp1, ts_comp2, varargin)
% function [pairconn] = weightedPhaseLagIndex(ts_comp1, ts_comp2, debiased)
% 
% Required Input
% - ts_comp[1|2]: complex-value ndarray, [nfreqs * ntimes * ntrials] or [ntimes (T) * ntrials (N)]
%
% Optional Input
% - debiased: numeric or logical, {0 (default), 1}, 0==wPLI, 1==dwPLI
%
% Available Methods
% - wPLI: alleviate the discontinuity of PLI (hence PLI is DEPRECATED)
% - dwPLI: debiased version of wPLI <==> square weighted PLI
%
% Output
% - pairconn: real-value ndarray, [nfreqs * ntimes] or [1 * ntimes]
%
% More Information
% - PLI: robust to volume conduction compared to PLV and coherence

%%
default_nargin = 2; % number of required inputs

% default value of optional inputs
debiased = false; 

% optional inputs
if nargin >= default_nargin + 1
    debiased = logical(varargin{1});
end

%%
size_ = size(ts_comp1);
crsspctrm = squeeze(squeeze(ts_comp1) .* conj(squeeze(ts_comp2)));
crxtype = isreal(crsspctrm);
crsspctrm = imag(crsspctrm);
outsum = nansum(crsspctrm, numel(size_));
outsumW = nansum(abs(crsspctrm), numel(size_));

switch debiased
    case 0
        if crxtype
            pairconn = squeeze(zeros([1 size_(1:end-1)]));
        else
            pairconn = abs(outsum) ./ outsumW;
        end
    case 1
        % handy way to compute pairwise thing
        % ab + ba = (a + b)^2 - (a^2 + b^2)
        outssq = nansum(crsspctrm.^2, numel(size_));
        pairconn = (outsum.^2 - outssq)./(outsumW.^2 - outssq); 
end

end