function [pairconn] = imagCoherency(ts_comp1, ts_comp2, varargin)
% function [pairconn] = imagCoherency(ts_comp1, ts_comp2, varargin)
%
% Required Input
% - ts_comp[1|2]: complex-value ndarray, [nfreqs (F) * ntimes * ntrials] or [ntimes (T) * ntrials (N)]
%
% Optional Input
% - corrected: logical or numeric, {0 (default), 1}, 0==not corrected,
% 1==corrected
%
% Available Methods
% - imaginary part of coherency (iCOH) by Nolte et al., 2004
% ---- range of value: [-1, 1]
% - corrected imaginary part of coherency (ciCOH) by Ewald et al., 2012
% ---- range of value: [0, ?]
%
% Output
% - pairconn: real-value ndarray, [nfreqs * ntimes] or [1 * ntimes]
%
%
% Example
%
% % `data` is a 3D complex-value matrix, [nchan * ntimes * ntrials]
% ts_comp1 = data(1, :, :);
% ts_comp2 = data(2, :, :);
%
%
% Deduction
%
% Gxy:  cross-spectral density between x and y
% Gxx:  autospectral density of x
% Gyy:  autospectral density of y
% Coherence function (C): Gxy/sqrt(Gxx*Gyy)
% Imaginary Coherence (IC): imag(C)^2 / (1-real(C)^2)

%%
default_nargin = 2;

corrected = false;

if nargin >= default_nargin + 1
    corrected = logical(varargin{1});
end

%%
ts_comp1 = squeeze(ts_comp1);
ts_comp2 = squeeze(ts_comp2);
size_ = size(ts_comp1);
tidx = numel(size_); % trial in the last axis

% if no axis for frequency
% removed trials that contain NaN
if numel(size_) == 2 
    [ridx,cidx1] = find(isnan(ts_comp1));
    cidx1 = unique(cidx1);
    [ridx,cidx2] = find(isnan(ts_comp2));
    cidx2 = unique(cidx2);
    cidx = unique([cidx1 cidx2]);
    
    ts_comp1 = ts_comp1(:,setdiff(1:size_(tidx),cidx));
    ts_comp2 = ts_comp2(:,setdiff(1:size_(tidx),cidx));
end

%%
Gxy = squeeze(mean(ts_comp1.* conj(ts_comp2), tidx));
Gxx = squeeze(mean(abs(ts_comp1).^2, tidx));
Gyy = squeeze(mean(abs(ts_comp2).^2, tidx));

%% Another Algorithm
% amp1 = abs(ts_comp1); amp2 = abs(ts_comp2);
% phase1 = angle(ts_comp1); phase2 = angle(ts_comp2);
% 
% delta_phase = phase1 - phase2;
% exp_phase = exp(1i*delta_phase);
% 
% Gxy = sum(amp1.*amp2.*exp_phase, tidx);
% Gxx = mean(amp1.^2, tidx);
% Gyy = mean(amp2.^2, tidx);

%%
coherency = Gxy ./ sqrt(Gxx .* Gyy);

if ~corrected % iCOH by Nolte et al., 2004
    pairconn = imag(coherency);
else % ciCOH by Ewald et al., 2012; also the default iCOH algorithm in Brainstorm
    if 1-real(coherency) .^2 ~= 0
        pairconn = imag(coherency).^2 ./ (1-real(coherency).^2);
    else
        pairconn = zeros(size(coherency));
    end
end

if any(isnan(coherency(:)))
    error('COMPUTATION ERROR: NaN in coherency.');
end
end