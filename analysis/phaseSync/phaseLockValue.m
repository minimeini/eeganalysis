function [pairconn] = phaseLockValue(ts_comp1, ts_comp2, varargin)
% function [pairconn] = phaseLockValue(ts_comp1, ts_comp2, varargin)
%
% Required Input
% - ts_comp[1|2]: complex-value ndarray, [nfreqs * ntimes * ntrials] or [ntimes (T) * ntrials (N)]
%
% Optional Input
% - debiased: numeric or logical, {0 (default), 1}, 0==PLV, 1==dPLV
% 
% Name-Value Parameter
% - complex: numeric or logical, {0 (default), 1}, return real(0) or complex(1) value
%
% Available Methods
% - PLV: conventional metric for phase synchronization
% - dPLV: debiased version of squared PLV == PPC (pairwise phase consistency)
%
% Output
% - pairconn: real-value ndarray, [nfreqs * ntimes] or [1 * ntimes]

%%
p = inputParser;
addRequired(p, 'ts_comp1');
addRequired(p, 'ts_comp2');
addOptional(p, 'debiased', 0, @(x) x==1||x==0);
addParameter(p, 'complex', 0, @(x) x==1||x==0);
parse(p, ts_comp1, ts_comp2, varargin{:});
%%
% computationally efficient way to calculate PLV
size_ = size(p.Results.ts_comp1);
trials = size_(numel(size_));

% bi-PLV (complex)
crsspctrm = squeeze(squeeze(p.Results.ts_comp1) .* ...
    conj(squeeze(p.Results.ts_comp2))); % T * N\
pairconn = squeeze(mean( crsspctrm ./ abs(crsspctrm), numel(size_) ));

if p.Results.debiased && ~p.Results.complex % bi-dPLV (real)
    pairconn = abs(pairconn);
    pairconn = (pairconn.^2 * trials - 1) / (trials - 1);
elseif ~p.Results.complex % bi-PLV (real)
    pairconn = abs(pairconn);
end

end