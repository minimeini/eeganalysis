function pairconn = msCoherence(ts_comp1, ts_comp2, varargin)
ts_comp1 = squeeze(ts_comp1); ts_comp2 = squeeze(ts_comp2);
crsspctrm = mean(squeeze(ts_comp1 .* conj(ts_comp2)),2); % T * N
atspctrm1 = mean(abs(ts_comp1).^2, 2);
atspctrm2 = mean(abs(ts_comp2).^2, 2);
pairconn = abs(crsspctrm).^2 ./ (atspctrm1 .* atspctrm2);
pairconn = squeeze(mean(pairconn,2))';
end