# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 22:22:48 2018

@author: welkin
"""

import os, time, random, sys
import matplotlib
#matplotlib.use("GTK3Cairo")
import matplotlib.pyplot as pyplot
import numpy as np
import igraph as ig
import comty_utils as comty


def main():
    
    dir_code,dir_prep,dir_vis = comty.initEnv()
    
    #---------------------------------------------------------------------#
    # Layout for simulated EEG
    dtype = {'names':('idx','x','y','label'),'formats':('i','f','f','U3')}
    idx,x,y,label = np.loadtxt(os.path.join(dir_prep,'chanlocs','acticap64xy.csv'),
                        dtype = dtype, delimiter=' ',unpack=True)
    loc = np.stack([x, y],axis=-1).tolist()
    layout = ig.Layout(loc)
    
    # simulated connection
    filepath = os.path.join(dir_prep,'simulation')
    prefix = 'sim_chaos_colpitts_p64_'
    suffix = '.mat'
    filename = ['r6','r6_LF','r6_LF_RP_iid','r6_LF+RP',
                'r4_3r_iid','r4_3r_2inter','r4_3r_3inter','r12']
    matname = 'conn_nt'
    
    for fname in filename:
        conn = comty.loadmat(os.path.join(filepath,prefix+fname+suffix),matname)
        conn_tmp = np.mean(conn,2)
        g,w = comty.create_graph(conn_tmp,labels=label,rescale=False,lowbnd=0)
        comty.graph_plot(g,layout,'fastgreedy')
        
    #---------------------------------------------------------------------#

    

if __name__ == "__main__":
    main()
    
    

