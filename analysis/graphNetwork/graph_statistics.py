# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 15:25:43 2018

@author: welkin
"""
import numpy as np

def small_worldness_2d(conn_2d):
    """
    Input
    
    Output
    
    Reference: 
        Bassett and Bullmore, 2006, Small-World Brain Networks
        Albert and Barabasi, 2002 (average minimum path length and clustering 
                                   coefficient for a corresponding random graph)
    
    Algorithm:
        N: total number of nodes
        K: total number/weight of edges
        L_rand: average minimum path length of a random graph
        C_rand: average clustering coefficient of a random graph
        L: average minimum path length
        C: average clustering coefficient
        lambda = L/L_rand
        gamma = C/C_rand
        sigma = gamma/lambda
    """
    
    N = conn_2d.shape[0]
    K = np.sum(conn_2d[np.nonzero(conn_2d)])
    
    L_rand = np.log(N) / np.log((K/N) - 1)
    C_rand = K / N / N
    
    return sigma
    
    