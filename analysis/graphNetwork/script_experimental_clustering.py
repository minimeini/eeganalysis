# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 20:14:49 2018

@author: welkin
"""

import os
import matplotlib.pyplot as plt
import numpy as np
import igraph as ig

os.chdir('c:/respository/eeganalysis/analysis/graphNetwork')
import comty_utils as comty
import changepoint_detection_cosSimilarity_SVDComps as cpnt
import extract_keygraph as kg


def main():
    
    dir_code,dir_prep,dir_vis = comty.initEnv()
    summary_method = 'svd2d'
    cluster_method = 'fastgreedy'
    time_start = -1500
    freq = 500
    
    sublist = [1,10,11,13,14,17,19,2,22,23,25,27,28,29,3,30,4,5,6,7]
    
    #---------------------------------------------------------------------#
    # Layout for experimental EEG
    idx,x,y,label = np.loadtxt(os.path.join(dir_prep,'chanlocs','acticap64xy.csv'),delimiter=' ',unpack=True,
                        dtype={'names':('idx','x','y','label'),'formats':('i','f','f','U3')})
    loctmp = np.stack([x, -y],axis=-1)
    badchan =np.array([41, 46, 17, 22, 28, 32, 37, 7, 42, 33, 54]) - 1
    usedchan = np.setdiff1d(np.array([x for x in range(64)]),badchan)
    label = label[usedchan]
    
    origin = np.where(label=='Cz')[0][0]
    origin = [x[origin],y[origin]]
    
    if badchan.shape[0] > 0:
        loc = loctmp[usedchan,:].tolist()
    else:
        loc = loctmp.tolist()
    
    layout = ig.Layout(loc)
    
    # experimental connection: ciCOH
    filepath = os.path.normpath(os.path.join(dir_prep,'ciCOH/U'))
    filename = os.listdir(filepath)
    matname = 'conn'
    
    cpnt_statpath = os.path.join(dir_prep,'change_points')
    cpnt_logpath = os.path.join(cpnt_statpath,'cpnts_experimental_u.csv')
    
#    header = 'sub,seg_idx'
#    for i in range(len(usedchan)):
#        header += ',chan' + str(usedchan[i])
#    
#    with open(os.path.join(cpnt_statpath,'cpnts_expr_membership_u.csv'),'w') as file:
#        file.write(header + '\n')
#    
#    for fname in filename:
#        conn = comty.loadmat(os.path.join(filepath,fname),matname)
#        cpnt_idx,cpnt_diff = cpnt.changepoint_detection_cosSimilarity_SVDComps(conn)
##        np.save(os.path.join(cpnt_statpath,'cosSim'+fname[:-4]+'.npy'),cpnt_diff)
##        with open(cpnt_logpath, 'ab') as f:
##            np.savetxt(f, cpnt_idx, fmt='%d', delimiter=',',newline=',')
##            f.write(b'\n')
##        
#        conn_seg = kg.get_segment(conn, cpnt_idx)
#        
#        nseg = len(conn_seg)
#        nbchan = conn.shape[0]
#        conn_kg = np.zeros((nbchan,nbchan,nseg))
#        
#        for i in range(nseg):
#            conn_kg[:,:,i] = kg.keygraph_extract(conn_seg[i],summary_method)
#            g,w = comty.create_graph(conn_kg[:,:,i],labels=label,rescale=False,lowbnd=0)
#            fig,membership = comty.graph_plot(g,layout,cluster_method)
#            with open(os.path.join(cpnt_statpath,'cpnts_expr_membership_u.csv'),'a') as file:
#                file.write(fname.split('_')[1][:-4]+',')
#                np.savetxt(file,np.asarray([i]+membership),delimiter=',',newline=',',fmt='%d')
#                file.write('\n')
            
    
    """
    cluster the membership over all subjects
    """
    cutpct = 90
    
    f = open(os.path.join(dir_prep,'analysis','cpnts_seg_cos_subject_indices.csv'))
    tokens = f.read().split()
    f.close()
    tokens = tokens[1:]
    
    seg_start = []
    seg_end = []
    seg_subs = []
    for token in tokens:
        tk = token.split(',')[0:-1]
        tk = [int(x) for x in tk if x!='']
        seg_start.append(tk[0])
        seg_end.append(tk[1])
        seg_subs.append(tk[2:])
    
    
    nseg = len(seg_start)
    for seg in range(nseg):
        idx_start = int((seg_start[seg]-time_start)/1000*freq)
        idx_end = int((seg_end[seg]-time_start)/1000*freq)
        
        membership_list = []
        sub_indices = np.unique(np.array(seg_subs[seg]))
        for sub in sub_indices:
            fp = os.path.join(filepath,'ciCOH_sub'+str(sublist[sub])+'.mat')
            conn = comty.loadmat(fp,matname)
            conn_kg = kg.keygraph_extract(conn[:,:,idx_start:idx_end],method='svd2d')

            g,w = comty.create_graph(conn_kg,labels=label,lowbnd=np.percentile(conn_kg.flatten(),cutpct))
            fig,membership = comty.graph_plot(g,layout,clustering='fastgreedy')
            membership_list.append(membership)
        
        sim_matrix = comty.cluster_get_distance(membership_list,metric='mutual_info')
        sub_score = np.mean(sim_matrix,axis=1)
        sub_cidx = np.where(sub_score==max(sub_score))[0][0]
        
        sub_center = sublist[sub_indices[sub_cidx]]
        sim_mean = np.mean(sim_matrix[sub_cidx,:])
        sim_std = np.std(sim_matrix[sub_cidx,:])
        print(sub_center,sim_mean,sim_std)
        
        conn = comty.loadmat(os.path.join(filepath,'ciCOH_sub'+str(sub_center)+'.mat'),matname)
        conn_kg = kg.keygraph_extract(conn[:,:,idx_start:idx_end],method='svd2d')
        g,w = comty.create_graph(conn_kg,labels=label,lowbnd=np.percentile(conn_kg.flatten(),cutpct))
        fig,membership = comty.graph_plot(g,layout,clustering='fastgreedy')
        fig.save(os.path.join(dir_vis,'comty_cpnts_cos','comty_cpnts_cos_'+str(cutpct)+'_seg'+str(seg)+'_sub'+str(sub_center)+'.png'))

        
    #---------------------------------------------------------------------#
    time_select = [-1200, -1000, -800, -600, -400, -200, 0, 200]
    time_idx = [int((t-time_start)/1000*freq) for t in time_select]
    ntimeseg = len(time_select)
    
    for i,t in enumerate(time_idx):
        membership_list = []
        for f in filename:
            conn = comty.loadmat(os.path.join(filepath,f),matname)
            conn = conn[:,:,t]
            g,w = comty.create_graph(conn,labels=label,lowbnd=np.percentile(conn.flatten(),cutpct))
            fig,membership = comty.graph_plot(g,layout,clustering='fastgreedy')
            membership_list.append(membership)
        
        sim_matrix = comty.cluster_get_distance(membership_list,metric='mutual_info')
        sub_score = np.mean(sim_matrix,axis=1)
        sub_cidx = np.where(sub_score==max(sub_score))[0][0]
        sub_center = sublist[sub_cidx]
        
        sim_mean = np.mean(sim_matrix[sub_cidx,:])
        sim_std = np.std(sim_matrix[sub_cidx,:])
        print(sub_center,sim_mean,sim_std)
        
        conn = comty.loadmat(os.path.join(filepath,f),matname)
        conn = conn[:,:,t]
        g,w = comty.create_graph(conn,labels=label,lowbnd=np.percentile(conn.flatten(),cutpct))
        fig,membership = comty.graph_plot(g,layout,clustering='fastgreedy')
        fig.save(os.path.join(dir_vis,'comty_tpnts','comty_tpnts_'+str(cutpct)+'_timeseg'+str(i)+'_sub'+str(sub_center)+'.png'))
        
        
    

if __name__ == "__main__":
    main()
    