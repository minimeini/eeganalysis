import numpy as np
from sklearn import decomposition as decomp
import tensorly as tl
from tensorly.decomposition import tucker

import comty_utils as comty
from reconstruct_2dsvd import svd2d_recon


def get_segment(conn_3d,seg_start_index):    
    seg_start_index = np.asarray(seg_start_index)
    seg_end_index = seg_start_index[seg_start_index>0]-1
    end_index = conn_3d.shape[2]-1
    if seg_end_index[-1] < end_index:
        seg_end_index = np.append(seg_end_index, [end_index])
    
    nseg = seg_start_index.shape[0]
    conn_seg = []
    for i in range(nseg):
        conn_temp = conn_3d[:,:,seg_start_index[i]:seg_end_index[i]]
        conn_seg.append(conn_temp)
    
    return conn_seg


def pca_recon(segflat, explev=0.95, ncomp=10):
    segmean = np.mean(segflat, axis=1)
    segmean = np.tile(segmean[:, np.newaxis], (1, segflat.shape[1]))
    segcov = np.dot(segflat - segmean, (segflat - segmean).transpose())
    segcov /= segflat.shape[1]
    pca = decomp.PCA(n_components=ncomp)
    pca.fit(segcov)
    cumsum = np.where(pca.explained_variance_ratio_.cumsum() > explev)[0].tolist()

    if len(cumsum) == 0:
        nselect = 10
    else:
        nselect = min(cumsum)
    vecs = pca.components_[:nselect, :]  # dim = nselect * npnts
    segrecon = np.dot(np.dot(vecs.transpose(), vecs), segflat) + segmean
    segrecon = comty.rescale_minmax(segrecon)
    return segrecon


def tucker_recon(conn_seg, rank=[15, 15, 3]):
    core, factors = tucker(conn_seg, ranks=rank)  # core = 15 * 15 * 3, factors = list(len=3)
    step1 = tl.tenalg.mode_dot(core, factors[0], 0)  # nbchan * 15 * 3, factors[0] = nbchan * 15
    step2 = tl.tenalg.mode_dot(step1, factors[1], 1)  # nbchan * nbchan * 3, factors[1] = nbchan * 15
    reconn = tl.tenalg.mode_dot(step2, factors[2], 2)  # nbchan * nbchan * nseg, factors[2] = nseg * 3
    reconn = comty.rescale_minmax(reconn)
    return reconn


def keygraph_extract(conn_seg, method, subset=None, explev=0.95, eigap_svd2d=-5, rank_tucker=[15, 15, 3]):
    if subset is not None:
        conn_seg = conn_seg[:, :, subset]

    if (method == 'mean') & (len(conn_seg.shape) > 2):
        reconn = conn_seg
    elif (method == 'pca') & (len(conn_seg.shape) > 2):
        segflat = conn_seg.reshape((conn_seg.shape[0] * conn_seg.shape[0], conn_seg.shape[2]), order='c')
        reconn = pca_recon(segflat, explev=explev)
        reconn = reconn.reshape(conn_seg.shape, order='c')
    elif (method == 'svd2d') & (len(conn_seg.shape) > 2):
        reconn = svd2d_recon(conn_seg, eigengap=eigap_svd2d)
    elif (method == 'tucker') & (len(conn_seg.shape) > 2):
        reconn = tucker_recon(conn_seg, rank=rank_tucker)

    keyGraph = np.mean(reconn, axis=2)
    return keyGraph


