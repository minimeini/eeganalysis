import numpy as np
import os
import igraph as ig
import igraph.drawing.colors as palettes
import h5py
import scipy.io as spio
from sklearn import metrics
from sklearn.cluster import SpectralClustering


def initEnv():
    dir_prep = 'C:/Users/meini/OneDrive/eegAnalysis'
    dir_code = 'C:/Respository/eeganalysis'
    dir_vis = 'C:/Users/meini/OneDrive/eegAnalysis/visualization'
    
    return dir_code, dir_prep, dir_vis


def checkPath(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def getFilename(filedir, pattern=None):
    filename = os.listdir(filedir)
    if pattern is not None:
        filename = [f for f in filename if pattern in f]
    
    return filename


def loadmat(filename, matname):
    try:
        conn = spio.loadmat(filename)
        conn = conn[matname]
    except NotImplementedError:
        with h5py.File(filename,'r') as file:
            conn = np.array(file[matname])
    
    return conn
                


def checkSymmetric(w, diagfill=0, tol=1e-8):
    def checkSymmetric2D(w_sin, diagfill=0, tol=1e-8):
        flag = np.allclose(w_sin, w_sin.transpose(), atol=tol)
        if not flag:
            w_sin *= np.tri(*w_sin.shape)
            w_sin = w_sin + w_sin.T
            np.fill_diagonal(w_sin, diagfill)
        return flag, w_sin

    flag = True
    if len(w.shape) > 2:
        for i in range(w.shape[2]):
            flag_sin, w[:, :, i] = checkSymmetric2D(w[:, :, i], diagfill=diagfill, tol=tol)
            if not flag_sin:
                flag = flag_sin
    else:
        flag, w = checkSymmetric2D(w, diagfill=diagfill, tol=tol)

    return flag, w


def rescale_minmax(data, min_scale=0, int_len=1):
    data -= np.min(np.array(data).flatten())
    data /= (np.max(data.flatten()) - np.min(data.flatten()))

    data += min_scale
    data *= int_len
    return data


def create_graph(w, labels=None, selfloop=False, directed=False, rescale=True, lowbnd=0, wmin=0, wlen=1):
    w = np.multiply(w, w>lowbnd)
    if rescale:
        w = rescale_minmax(w, min_scale=wmin, int_len=wlen)
    flag, w = checkSymmetric(w, diagfill=selfloop)
    
    g = ig.Graph.Adjacency((w>0).tolist())
    if not directed:
        g = g.as_undirected()
        wght = w * np.tri(*w.shape)
    else:
        wght = w
    
    g.es['weight'] = wght[wght.nonzero()]
    g.es['width'] = wght[wght.nonzero()]
    
    if labels is not None:
        g.vs['name'] = labels
        g.vs['label'] = labels

    return g, w


def graph_style(g, layout, labels=None, vertex_size='degree'):
    visual_style = dict()
    visual_style['margin'] = np.array([20,20,20,40]).tolist() # L,U,R,D
    
    visual_style['vertex_color'] = 'black'
    visual_style['vertex_label_size'] = 20
    visual_style['vertex_label_dist'] = 2
    visual_style['vertex_label_color'] = 'black'
    try:
        visual_style['vertex_label'] = g.vs['label']
    except KeyError:
        if labels is not None:
            if type(labels) is np.ndarray:
                labels = labels.tolist()
            
            visual_style['vertex_label'] = labels
            
    visual_style['edge_width'] = g.es['weight']
    visual_style['layout'] = layout
    
    if vertex_size == 'stable':
        visual_style['vertex_size'] = 20
    elif vertex_size == 'degree':
        outdegree = g.outdegree()
        visual_style["vertex_size"] = [x/max(outdegree)*15+12 for x in outdegree]
    
    return visual_style    


def find_comty(g, method):
    if method == 'fastgreedy': # *****
        comty = g.community_fastgreedy(weights='weight')
        comty = comty.as_clustering()
    elif method == 'leading_eigenvector': # *****
        comty = g.community_leading_eigenvector(weights='weight')
    elif method == 'multilevel':
        comty = g.community_multilevel(weights='weight')
    elif method == 'spinglass':
        comty = g.community_spinglass(weights='weight')
    else:
        print('Invalid method.')

    membership = comty.membership
    return membership


def graph_plot(g, layout, clustering=None):
    visual_style = graph_style(g,layout)
    if clustering is not None:
        cluster_idx = find_comty(g,clustering)
        palette = palettes.ClusterColoringPalette(len(np.unique(cluster_idx)))
        g.vs['color'] = [palette[cluster_idx[x]] for x in range(len(cluster_idx))]
        visual_style['vertex_color'] = g.vs['color']
    else:
        visual_style['vertex_color'] = 'black'
        
    fig = ig.plot(g, **visual_style)
    return fig, cluster_idx


def match_comty_elementwise(membership_a, membership_b):
    """
    Input:
        membership_a: cluster of nelemts, 1*nelemts numpy array
        membership_b: cluster of nelemts, 1*nelemts numpy array
    Algorithm:
        Jaccard index = capSet / cupSet
    """
    nelemts = len(membership_a)
    nsrc = len(np.unique(membership_a)) # number of clusters
    ntar = len(np.unique(membership_b)) # number of clusters
    
    binmat_a = np.zeros((nsrc,nelemts))
    binmat_b = np.zeros((ntar,nelemts))
    for i in range(nelemts):
        binmat_a[membership_a[i],i] = 1
        binmat_b[membership_b[i],i] = 1
    
    capSet = np.zeros((nsrc,ntar))
    cupSet = np.zeros((nsrc,ntar))
    for i in range(nsrc):
        for j in range(ntar):
            capSet[i,j] = np.sum(np.multiply(binmat_a[i,:],binmat_b[j,:]))
            cupSet[i,j] = np.sum((binmat_a[i,:] + binmat_b[j,:])>0)
    
    jaccard_score = np.divide(capSet, cupSet)
    max_jaccard = np.max(jaccard_score,axis=0) # find max for each row (source/a)
    match_idx = [np.where(row==max_jaccard[i])[0][0] for i,row in enumerate(jaccard_score)]
    
    return match_idx


def cluster_get_distance(membership_list,metric='rand_index'):
    ncluster = len(membership_list)
    sim_matrix = np.zeros((ncluster,ncluster))
    for i in range(ncluster):
        for j in range(i+1,ncluster):
            if metric == 'rand_index':
                sim_matrix[i,j] = metrics.adjusted_rand_score(membership_list[i],
                          membership_list[j])
            elif metric == 'mutual_info':
                sim_matrix[i,j] = metrics.mutual_info_score(membership_list[i],
                          membership_list[j])
            elif metric == 'adjusted_mutual_info':
                sim_matrix[i,j] = metrics.adjusted_mutual_info_score(membership_list[i],
                          membership_list[j])
            
            sim_matrix[j,i] = sim_matrix[i,j]
    
    return sim_matrix


def cluster_find_optim_n(adjacency_matrix,ncmax=10, ncmin=2,return_best_score=False):
    """
    score = intra cluster weights / inter cluster weights
    """
    adjacency_matrix = np.asarray(adjacency_matrix)
    nsample = adjacency_matrix.shape[0]
    ncmax = min(nsample-1,ncmax)
    ncmin = min(nsample-1,ncmin)
    
    score_best = 0
    nc_best = ncmin
    
    for nc in range(ncmin,ncmax+1):
        membership_cluster = SpectralClustering(n_clusters=nc,affinity='precomputed').fit(adjacency_matrix)
        membership_labels = membership_cluster.labels_
        
        intra_cluster = np.zeros(nc)
        inter_cluster = np.zeros(nc)
        
        for cidx in range(nc):
            inclass_idx = np.where(membership_labels==cidx)[0]
            outclass_idx = np.where(membership_labels!=cidx)[0]
            for a_in in range(len(inclass_idx)):
                for b_in in range(a_in+1,len(inclass_idx)):
                    intra_cluster[cidx] += adjacency_matrix[inclass_idx[a_in],inclass_idx[b_in]]
            
            for a_in in range(len(inclass_idx)):
                for b_out in range(len(outclass_idx)):
                    inter_cluster[cidx] += adjacency_matrix[inclass_idx[a_in],outclass_idx[b_out]]
        
        score_cluster = np.mean(np.divide(intra_cluster, inter_cluster))
        if score_cluster >= score_best:
            score_best = score_cluster
            nc_best = nc
        
    if return_best_score:
        return nc_best,score_best
    else:
        return nc_best


def cluster_comty(membership_list,metric='rand_index',ncmin=2,return_dist=False):
    """
    Input:
        membership_list
        metric: optional
        ncmin: optional
        return_dist: optional
        
    Output:
        membership_labels
        sim_matrix: optional
        
    Metric: 
        rand_index
        mutual_info
        adjusted_mutual_info
        
    References:
        Fan and Yeung, 2014, Similarity between community structures of different 
            online social networks and its impact on underlying community detection
        A Tutorial on Spectral Clustering, 2007 Ulrike von Luxburg
        Multiclass spectral clustering, 2003 Stella X. Yu, Jianbo Shi
    """
    
    sim_matrix = cluster_get_distance(membership_list,metric)
    best_nc = cluster_find_optim_n(sim_matrix,ncmin=ncmin)
    membership_cluster = SpectralClustering(n_clusters=best_nc,affinity='precomputed').fit(sim_matrix)
    membership_labels = membership_cluster.labels_
    
    if return_dist:
        return membership_labels,sim_matrix
    else:
        return membership_labels