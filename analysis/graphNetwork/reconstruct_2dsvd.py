import numpy as np
from numpy.linalg import svd


def demean_3d(conn_3d):
    ntimes = conn_3d.shape[2]    
    conn_mean = np.mean(conn_3d, axis=2)
    conn_mean = conn_mean[:,:,np.newaxis]
    conn_mean = np.tile(conn_mean, (1,1,ntimes))
    
    conn_demean = conn_3d - conn_mean
    return conn_demean, conn_mean


def svd_2d(conn_3d):
    ntimes = conn_3d.shape[2]
    nbchan = conn_3d.shape[0]
    conn_demean, conn_mean = demean_3d(conn_3d)
    
    F = np.zeros((nbchan,nbchan))
    for t in range(ntimes):
        F = F + conn_demean[:,:,t] * conn_demean[:,:,t]
    
    u,s,vh = svd(F, full_matrices=False)
    return u,s,vh,F


def svd2d_recon(conn_3d, eigengap=-5):
    ntimes = conn_3d.shape[2]
    nbchan = conn_3d.shape[0]
    conn_demean, conn_mean = demean_3d(conn_3d)
    u,s,vh,F = svd_2d(conn_demean)
    
    diff = np.zeros(np.int8(nbchan/2))
    for k in range(np.int8(nbchan/2)):
        F_reconstruct = np.dot(u[:,:k],np.dot(np.diag(s[:k]),vh[:k,:]))
        diff[k] = np.linalg.norm(F-F_reconstruct,ord=2)
    
    k = np.sum(np.diff(diff)< eigengap)
    
    ut = u[:,:k]
    ut2 = np.dot(ut, np.transpose(ut))
    vht = vh[:k,:]
    vht2 = np.dot(np.transpose(vht),vht)
    
    conn_reconstruct = np.zeros((nbchan,nbchan,ntimes))
    for t in range(ntimes):
        conn_reconstruct[:,:,t] = np.dot(ut2, np.dot(conn_demean[:,:,t],vht2)) + conn_mean[:,:,t]
    
    return conn_reconstruct