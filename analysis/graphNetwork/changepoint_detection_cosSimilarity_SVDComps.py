# -*- coding: utf-8 -*-
# @Author: yll
# @Date:   2018-11-08 09:16:34
# @Last Modified by:   yll
# @Last Modified time: 2018-11-20 15:14:58

import numpy as np
from numpy.linalg import svd
import math
from scipy import stats
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score


def similarity_cosin(u1, u2):
	'''calulate the cosine similarity of two vectors u1 and u2'''
	similarity_cos = np.dot(u1,u2)/(np.linalg.norm(u1)* np.linalg.norm(u2))
	return similarity_cos


def indices_clustering(indices, ncmax=10, ncmin = 2):
    indices = np.asarray(indices)
    
    ncmax = min(ncmax, len(np.unique(indices))-1)
    ncmin = max(min(ncmin, len(np.unique(indices))-1),2)
    
    X = indices.reshape(-1,1)
    best_silhouette = 0
    best_nc = ncmin
    for nc in range(ncmin,1+ncmax):
        kmeans = KMeans(n_clusters=nc).fit(X)
        silhouette_avg = silhouette_score(X, kmeans.labels_)
        if silhouette_avg > best_silhouette:
            best_silhouette = silhouette_avg
            best_nc = nc
	
    kmeans = KMeans(n_clusters=best_nc).fit(X)
    labels = np.asarray(kmeans.labels_)
    return labels


def indices_segment(indices,labels,return_nseg=False):
    labels_unq,labels_idx = np.unique(labels, return_index=True)
    sort_idx = labels_idx.argsort()
    labels_unq = labels_unq[sort_idx]
    
    nseg = labels_unq.shape[0]
    seg_start = labels_idx[sort_idx]
    seg_end = np.append(seg_start[1:]-1, len(labels)-1)
    
    if return_nseg:
        return seg_start, seg_end, nseg
    else:
        return seg_start, seg_end


def get_clusters(indices,labels=None,treat_cluster='segment',feature=None,return_labels=False):
    """
    Input:
        indices: required, 1d numpy array, feature indices to be clustered
        labels: optional, 1d numpy array, clustering labels of the indices
        treat_cluster: optional, string in {'segment', 'maxdiff', 'median', 'mean'}, 
            method used to generate clusters of feature based on indices clusstering
        feature: optional, 1d list or 1d numpy array, feature to be clusterd
        return_labels: optional, logical, return labels or not
    
    Output:
        indices: 1d numpy array, clustered feature indices
        labels: optional, 1d numpy array, clustering label of the indices
    """   
    
    """
    Format Input
    """
    indices = np.asarray(indices)
    if labels is not None:
        labels = np.asarray(labels)
    else:
        labels = indices_clustering(indices)
    
    if feature is not None:
        feature = np.asarray(feature)
    
    """
    Segment of Indices
    """
    seg_start,seg_end,nseg = indices_segment(indices,labels,return_nseg=True)
    
    """
    Cluster Indices
    """
    if treat_cluster == 'segment':
        indices_riot = indices[seg_start].tolist()
        indices_steady = (indices[seg_end]+1).tolist()
        indices = np.sort(np.array(indices_riot+indices_steady))
        if feature is not None:
            indices = indices[indices<len(feature)]
        
    elif treat_cluster == 'maxdiff':
        indices_max = np.zeros(len(seg_start))
        feature_max = np.zeros(len(seg_start)) + 1
        for i in range(len(seg_start)):
            for j in range(seg_start[i],seg_end[i]+1):
                if feature[j] < feature_max[i]:
                    feature_max[i] = feature[j]
                    indices_max[i] = j
        indices = indices[indices_max.astype(int)]
        
    elif treat_cluster == 'median':
        indices_select = (seg_end - seg_start + 1)/2
        indices_select = [math.floor(x) for x in indices_select] + seg_start
        indices = indices[indices_select]
        
    elif treat_cluster == 'mean':
        indices_mean = np.zeros(nseg)
        for i,s,e in zip(range(nseg),seg_start,seg_end):
            indices_mean[i] = np.mean(indices[s:(e+1)])
        
        indices = np.round(indices_mean)
        
    if return_labels:
        return indices,labels
    else:
        return indices



def changepoint_detection_cosSimilarity_SVDComps(matrix_Con, cluster=True, treat_cluster='maxdiff'):
	""" detect the change points using connectivity matrix based on cosine similarity of SVD components

		each matrix (n_chns * n_chns) is first decomposed using SVD
		Then, cosSimilarity is calcuated using weighted cosin similarity

		@ parameter matrix_Con: connectivity matrix, n_chns * n_chns * n_times

		return points_change: a vector of change points, start with 1

	"""

	''' load data for testing '''
	'''
	from scipy.io import loadmat
	matloaded = loadmat('./simulation/matrixConn_Surrogate.mat')
	matrix_Con = matloaded['matrix_Con_Surr']
	'''
	n_chns, n_chns, n_times = matrix_Con.shape
	''' SVD decomposition for the connectivity matrix in each time i_time'''
	Us = np.zeros((n_chns, n_chns, n_times))
	Ss = np.zeros((n_chns, n_times))
	for i_time in range(n_times):
		matrix_conEach = matrix_Con[:,:,i_time] # matrix_conEach: the connectivity matrix in each time i
		u,s,vh = svd(matrix_conEach, full_matrices = True) # matrix_conEach = u * s * vh
		Us[:,:,i_time] = u
		Ss[:,i_time] = s # s is a vector
		del matrix_conEach, u, s, vh

	''' calcuate different matrix based on cosine similarity '''
	diff = np.zeros((n_times-1,)) # cosine similarity
	for i_time in range(1, n_times):
		U_current, S_current = Us[:,:,i_time], Ss[:,i_time]
		U_previous, S_previous = Us[:,:,i_time - 1],Ss[:,i_time -1] 
		n_comp = U_current.shape[1]
		similarity_cos = np.zeros((n_comp,))
		for i_comp in range(U_current.shape[1]):
			u1, u2 = U_current[:,i_comp], U_previous[:,i_comp]
			similarity_cos[i_comp] = similarity_cosin(u1, u2)
			del u1, u2
		S_mean = (S_current + S_previous)/2
		weight = S_mean /np.sum(S_mean)
		diff[i_time-1] = np.sqrt(np.sum(weight * (similarity_cos*similarity_cos)))
		del weight, S_mean
		del U_current, U_previous, S_previous, S_current
		del similarity_cos
		del n_comp, i_comp

	''' change points are found as the values belong to the area <signlev '''
	signlev = 0.05 # set the significant level
	m,s = stats.norm.fit(diff)
	point_per = stats.norm.ppf(signlev, m,s) # Percent point
	indices = np.asarray(np.nonzero(diff<point_per))[0] # indices of the change point
	
	if cluster is True:
		labels = indices_clustering(indices)
		indices = get_clusters(indices,labels,treat_cluster,feature=diff)
	
	
	return indices,diff
