# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 20:35:23 2018

@author: welkin
"""

import os
import csv
import numpy as np
import math
import scipy.stats.stats as stats
from scipy.stats import ttest_ind
import ruptures

os.chdir('c:/respository/eeganalysis/analysis/graphNetwork')
import comty_utils as comty
import changepoint_detection_cosSimilarity_SVDComps as cpnt


def main():
    """
    Global Parameters
    """
    signlev = 0.05
    freq = 500 # unit: Hz
    time_start = -1500
    
    dir_code,dir_prep,dir_vis = comty.initEnv()
    cpnt_file = os.path.join(dir_prep,'change_points','cpnts_experimental_u.csv')
    
    realeeg_path = os.path.join(dir_prep,'preprocessed_realeeg_mat/U')
    realeeg_file = comty.getFilename(realeeg_path)
    
    dtype = {'names':('idx','x','y','label'),'formats':('i','f','f','U3')}
    idx,x,y,label = np.loadtxt(os.path.join(dir_prep,'chanlocs','acticap64xy.csv'),
                        dtype = dtype, delimiter=' ',unpack=True)
    badchan = ['FT9','FT1','TP9','TP1','PO9','PO1','F5','F8','FT7','AF7','CP4']
    
    if badchan is not None:
        select_idx = [x for x in range(label.shape[0]) if label[x] not in badchan]
        x = x[select_idx]
        y = y[select_idx]
        label = label[select_idx]
    
    chan_select = [['F7','F5','F3','F1','Fz','F2','F4','F6','F8'],
                   ['FC5','FC3','FC1','FC2','FC4','FC6'],
                   ['C5','C3','C1','Cz','C2','C4','C6'],
                   ['CP5','CP3','CP1','CPz','CP2','CP4','CP6'],
                   ['P7','P5','P3','P1','Pz','P2','P4','P6','P8']]
    
    
    """
    Compare cosine similarity between different subjects
    """
    f = open(cpnt_file)
    tokens = f.read().split()
    f.close()
    cpnts = np.zeros((1,1))
    sub_idx = np.zeros(1) - 1
    cnt = 0
    for token in tokens:
        tk = token.split(',')[0:-1]
        tk = [int(x) for x in tk if x!= '']
        sub_idx = np.append(sub_idx,np.zeros(len(tk))+cnt)
        cpnts = np.append(cpnts,tk)
        cnt += 1
    
    cpnts = cpnts[1:]
    sub_idx =sub_idx[1:]
    
    cpnts_sort = np.argsort(cpnts)
    cpnts = cpnts[cpnts_sort]
    sub_idx = sub_idx[cpnts_sort]
    
    cpnts_centroid,cpnts_labels = cpnt.get_clusters(cpnts,treat_cluster='mean',return_labels=True)
    cpnts_median = cpnt.get_clusters(cpnts,cpnts_labels,treat_cluster='median')
    times_centroid = cpnts_centroid*(1000/freq) + time_start
    times_median = cpnts_median*(1000/freq) + time_start
    
    seg_start,seg_end,nseg = cpnt.indices_segment(cpnts,cpnts_labels,return_nseg=True)
    times_segment = []
    subidx_segment = []
    for s,e in zip(seg_start,seg_end):
        times_segment.append(cpnts[s:(e+1)]*2 - 1500)
        subidx_segment.append(sub_idx[s:(e+1)])
    
    sub_n = np.zeros(nseg)
    sub_mean_std = np.zeros(nseg)
    sub_median_std = np.zeros(nseg)
    for i in range(nseg):
        sub_n[i] = len(times_segment[i])
        sub_mean_std[i] = math.sqrt(np.sum(np.power(times_segment[i] - times_centroid[i],2)) / sub_n[i])
        sub_median_std[i] = math.sqrt(np.sum(np.power(times_segment[i] - times_median[i],2)) / sub_n[i])
    
    sub_stat = np.array([sub_n,times_centroid,sub_mean_std,times_median,sub_median_std])
    sub_stat = sub_stat.transpose()
    
    logpath = os.path.join(dir_prep,'analysis','changepoint_subject_statistics.csv')
    header = 'N,mean,mean_std,median,median_std,subjects'
    np.savetxt(logpath,sub_stat,delimiter=',',header=header,fmt='%d,%d,%.2f,%d,%.2f')
    
    for i in range(nseg):
        with open(os.path.join(dir_prep,'analysis','changepoint_subject_indices.csv'), 'a') as file:
            np.savetxt(logpath,subidx_segment[i],delimiter=',',newline=',',fmt='%d')
            file.write('\n')
    
    
    """
    Time analysis between different subjects: MRCP
    """
    for f in realeeg_file:
        sub_label = f.split('_')[0]
        fname = 'cosSimciCOH_'+sub_label+'.npy'
        cos_sim = np.load(os.path.join(dir_prep,'change_points',fname))
        realeeg = comty.loadmat(os.path.join(realeeg_path,f), 'data')

        out_log = os.path.join(dir_prep,'changepoint_mrcp','changepoint_mrcp_'+sub_label+'.csv')
        with open(out_log,'w') as f:
            f.write('channel,index,pvalue,cpnts\n')
        
        for chan in chan_select:
            chan_idx = [x for x in range(label.shape[0]) if label[x] in chan]
            eeg_select = np.mean(realeeg[chan_idx,1:,:],2)
            
            tmp = [abs(stats.pearsonr(cos_sim,eeg_select[x,:])[1]) for x in range(len(chan_idx))]
            
            for j,row in zip(range(eeg_select.shape[0]),eeg_select):
                cpnts_algo = ruptures.GreedyAR().fit(row)
                row_cpnts = cpnts_algo.predict(pen=0.01)
                row_labels = cpnt.indices_clustering(row_cpnts,ncmin=4)
                row_cpnts = cpnt.get_clusters(row_cpnts,row_labels,treat_cluster='median')
                row_cpnts = row_cpnts*2 - 1500
                
                with open(out_log,'a') as f:
                    np.savetxt(f,np.asarray([label[chan_idx[j]]]),delimiter=',',newline=',',fmt='%s')
                    np.savetxt(f,np.asarray([chan_idx[j]]),delimiter=',',newline=',',fmt='%d')
                    np.savetxt(f,np.asarray([tmp[j]]),delimiter=',',newline=',',fmt='%.4f')
                    np.savetxt(f,row_cpnts,delimiter=',',newline=',',fmt='%d')
                    f.write('\n')
    
    
    """
    Channels that MRCP is significantly correlated to ciCOH
    """
    mrcp_logpath = os.path.normpath(os.path.join(dir_prep,'changepoint_mrcp'))
    loglist = comty.getFilename(mrcp_logpath)
    dtype = {'names':['chanlab','chanidx','pval'],'formats':['S3',np.int32,'f']}
    chan_score = np.zeros(label.shape)
    
    signidx = []
    for f in loglist:
        data = np.genfromtxt(os.path.join(mrcp_logpath,f),dtype=dtype,
                             usecols=(0,1,2),skip_header=1,delimiter=',')
        tmp = [row[1] for row in data if row[2]<signlev]
        chan_score[tmp] += 1
        signidx.append(tmp)
        
    idx_corr = np.nonzero(chan_score)
    idx_sort = np.argsort(chan_score[idx_corr])
    print(label[idx_corr][idx_sort])
    
    
    """
    Change points in MRCP: channels that (not) correlated with ciCOH
    """
    # Scenario 1. For each subject
    out_log_corr = os.path.join(dir_prep,'analysis','changepoint_mrcp_subject_clustering_corr.csv')
    with open(out_log_corr,'w') as file:
        file.write('subname,cpnts\n')
    
    out_log_all = os.path.join(dir_prep,'analysis','changepoint_mrcp_subject_clustering_all.csv')
    with open(out_log_all,'w') as file:
        file.write('subname,cpnts\n')
    
    for f in loglist:
        with open(os.path.join(mrcp_logpath,f)) as file:
            reader = csv.reader(file)
            cpnts_idx = [np.array(row[1:3]).astype(float) for idx,row in enumerate(reader) if idx!=0]
        
        with open(os.path.join(mrcp_logpath,f)) as file:
            reader = csv.reader(file)
            cpnts_sub = [np.array(row[3:-1]).astype(int) for idx,row in enumerate(reader) if idx!=0]
        
        cpnts_idx_corr = [idx for idx,row in enumerate(cpnts_idx) if row[1]<signlev]
        
        # >>> for channels that correlated with ciCOH
        cpnts_corr = np.zeros(1) - 9999
        for idx in cpnts_idx_corr:
            cpnts_corr = np.append(cpnts_corr,cpnts_sub[idx])
        cpnts_corr = np.sort(cpnts_corr[cpnts_corr>-9999])
        
        if len(cpnts_corr) > 0:
            cpnts_corr_cluster = cpnt.get_clusters(cpnts_corr,treat_cluster='median')
        
        with open(out_log_corr,'a') as file:
            file.write(f.split('_')[-1][:-4]+',')
            if len(cpnts_corr) > 0:
                np.savetxt(file,cpnts_corr_cluster,delimiter=',',newline=',',fmt='%d')
            file.write('\n')
    
        # >>> for all channels
        cpnts_idx_all = [idx for idx,row in enumerate(cpnts_idx)]
        cpnts_corr_all = np.zeros(1) - 9999
        for idx in cpnts_idx_all:
            cpnts_corr_all = np.append(cpnts_corr_all,cpnts_sub[idx])
        cpnts_all = np.sort(cpnts_corr_all[cpnts_corr_all>-9999])
        cpnts_all_cluster = cpnt.get_clusters(cpnts_all,treat_cluster='median')
        
        with open(out_log_all,'a') as file:
            file.write(f.split('_')[-1][:-4]+',')
            np.savetxt(file,cpnts_all_cluster,delimiter=',',newline=',',fmt='%d')
            file.write('\n')
    
    # Scenario 2. For all subjects
    with open(out_log_corr) as file:
        reader = csv.reader(file)
        tmp = [np.array(row[1:-1]).astype(int) for idx,row in enumerate(reader) if idx!=0]
    
    cpnts_corr = np.zeros(1) - 9999
    cpnts_corr_slabels = np.zeros(1) - 1
    for i,t in enumerate(tmp):
        cpnts_corr = np.append(cpnts_corr,t)
        cpnts_corr_slabels = np.append(cpnts_corr_slabels,np.asarray([i]*(t.shape[0])))
    
    cpnts_corr = cpnts_corr[cpnts_corr>-9999]
    cpnts_corr_slabels = cpnts_corr_slabels[cpnts_corr_slabels>-1]
    
    cpnts_corr_sort = np.argsort(cpnts_corr)
    cpnts_corr = cpnts_corr[cpnts_corr_sort]
    cpnts_corr_slabels = cpnts_corr_slabels[cpnts_corr_sort]
    
    cpnts_corr_clabels = cpnt.indices_clustering(cpnts_corr,ncmin=3)
    cpnts_corr_cluster = cpnt.get_clusters(cpnts_corr,cpnts_corr_clabels,treat_cluster='median')
    
    cpnts_corr_cnt = np.zeros(len(cpnts_corr_cluster))
    for i in range(len(cpnts_corr_cluster)):
        cpnts_corr_cnt[i] = len(np.unique(cpnts_corr_slabels[np.where(cpnts_corr_clabels==i)]))
    
    seg_start_corr,seg_end_corr,nseg_corr = cpnt.indices_segment(cpnts_corr,cpnts_corr_clabels,return_nseg=True)
    timeseg_corr = []
    for s,e in zip(seg_start_corr,seg_end_corr):
        timeseg_corr.append(cpnts_corr[s:(e+1)])
    
    std_corr = np.zeros(nseg_corr)
    for i in range(nseg_corr):
        std_corr[i] = math.sqrt(np.sum(np.power(timeseg_corr[i]-cpnts_corr_cluster[i],2))/cpnts_corr_cnt[i])
    
    corr_stat = np.array([cpnts_corr_cnt,cpnts_corr_cluster,std_corr])
    corr_stat = corr_stat.transpose()
    np.savetxt(os.path.join(dir_prep,'analysis','changepoint_mrcp_corr.csv'),
               corr_stat,delimiter=',',header='cnt,median,std',fmt='%d,%d,%.2f')
    
    
    with open(out_log_all) as file:
        reader = csv.reader(file)
        tmp = [np.array(row[1:-1]).astype(int) for idx,row in enumerate(reader) if idx!=0]
    
    cpnts_all = np.zeros(1) - 9999
    cpnts_all_slabels = np.zeros(1) - 1
    for i,t in enumerate(tmp):
        cpnts_all = np.append(cpnts_all,t)
        cpnts_all_slabels = np.append(cpnts_all_slabels,np.asarray([i]*(t.shape[0])))
    
    cpnts_all = cpnts_all[cpnts_all>-9999]
    cpnts_all_slabels = cpnts_all_slabels[cpnts_all_slabels>-1]
    
    cpnts_all_sort = np.argsort(cpnts_all)
    cpnts_all = cpnts_all[cpnts_all_sort]
    cpnts_all_slabels = cpnts_all_slabels[cpnts_all_sort]
    
    cpnts_all_clabels = cpnt.indices_clustering(cpnts_all,ncmin=3)
    cpnts_all_cluster = cpnt.get_clusters(cpnts_all,cpnts_all_clabels,treat_cluster='median')
    
    cpnts_all_cnt = np.zeros(len(cpnts_all_cluster))
    for i in range(len(cpnts_all_cluster)):
        cpnts_all_cnt[i] = len(np.unique(cpnts_all_slabels[np.where(cpnts_all_clabels==i)]))
        
    seg_start_all,seg_end_all,nseg_all = cpnt.indices_segment(cpnts_all,cpnts_all_clabels,return_nseg=True)
    timeseg_all = []
    for s,e in zip(seg_start_all,seg_end_all):
        timeseg_all.append(cpnts_all[s:(e+1)])
    
    std_all = np.zeros(nseg_all)
    for i in range(nseg_all):
        std_all[i] = math.sqrt(np.sum(np.power(timeseg_all[i]-cpnts_all_cluster[i],2))/cpnts_all_cnt[i])
    
    all_stat = np.array([cpnts_all_cnt,cpnts_all_cluster,std_all])
    all_stat = all_stat.transpose()
    np.savetxt(os.path.join(dir_prep,'analysis','changepoint_mrcp_all.csv'),
               all_stat,delimiter=',',header='cnt,median,std',fmt='%d,%d,%.2f')    
    
    
    
    return

    

if __name__ == "__main__":
    main()