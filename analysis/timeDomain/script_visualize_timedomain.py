# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 03:36:55 2018

@author: welkin
"""

import os
import csv
import numpy as np
import matplotlib.pyplot as plt

os.chdir('c:/respository/eeganalysis/analysis/graphNetwork')
import comty_utils as comty
import changepoint_detection_cosSimilarity_SVDComps as cpnt


def main():
    """
    Global Parameters
    """
    signlev = 0.05
    freq = 500 # unit: Hz
    time_start = -1500
    dir_code,dir_prep,dir_vis = comty.initEnv()
    
    cpnt_file = os.path.join(dir_prep,'change_points','cpnts_experimental_u.csv')
    cpnts_tmp = []
    with open(cpnt_file) as file:
        reader = csv.reader(file)
        cpnts_tmp.append([row for row in reader])
    
    cpnts_tmp = cpnts_tmp[0]
    cpnts = []
    for tmp in cpnts_tmp:
        cpnts.append([int(t) for t in tmp if t!=''])
    
    realeeg_path = os.path.normpath(os.path.join(dir_prep,'preprocessed_realeeg_mat/U'))
    realeeg_file = comty.getFilename(realeeg_path)
    conn_path = os.path.normpath(os.path.join(dir_prep,'ciCOH/U'))
    conn_file = comty.getFilename(conn_path)
    nsub = len(conn_file)
    
    dtype = {'names':('idx','x','y','label'),'formats':('i','f','f','U3')}
    idx,x,y,label = np.loadtxt(os.path.join(dir_prep,'chanlocs','acticap64xy.csv'),
                        dtype = dtype, delimiter=' ',unpack=True)
    badchan = ['FT9','FT1','TP9','TP1','PO9','PO1','F5','F8','FT7','AF7','CP4']
    
    if badchan is not None:
        select_idx = [x for x in range(label.shape[0]) if label[x] not in badchan]
        x = x[select_idx]
        y = y[select_idx]
        label = label[select_idx]
    
    chan_select = [['FC5','FC3','FC1','FC2','FC4','FC6'],
                   ['C5','C3','C1','Cz','C2','C4','C6'],
                   ['CP5','CP3','CP1','CPz','CP2','CP6'],
                   ['P5','P3','P1','Pz','P2','P4','P6']]
    
    
    """
    Cosine similarity and the corresponding change points over all subjects
    """
    f, axarr = plt.subplots(5, 1, sharex=True)
    for i in range(15,20):
        conn = comty.loadmat(os.path.join(conn_path,conn_file[i]),'conn')
        indices,diff = cpnt.changepoint_detection_cosSimilarity_SVDComps(conn,cluster=False)
        
        axarr[i%5].plot(diff)
        for cp in cpnts[i][1:]:
            axarr[i%5].axvline(x=cp,alpha=0.5,linestyle='--',color='orange')
        
        axarr[i%5].set_ylim(bottom=0.5,top=1)
        axarr[i%5].set_xticklabels(['-1900','-1500','-1100','-700','-300','100'])
        
        if i%5 == 4:
            axarr[i%5].set_xlabel('Time (ms)')
    
    plt.tight_layout()
    plt.savefig(os.path.join(dir_vis,'cos_sim_ts/U','cosSim_sub_15_20.png'),dpi=300)
    
    
    """
    MRCP in ROI
    """
    cpnts_sub = [-1320,-1100,-920,-620,-400,-300,-120,100,298]
    cpnts_sub = [int((cp-time_start)/1000*freq) for cp in cpnts_sub]
    
    roi_idx = []
    for cs in chan_select:
        roi_idx += cs
        
    roi_idx = [np.where(label==rlab)[0][0] for rlab in roi_idx if np.sum(label==rlab)>0]
    
    eeg = comty.loadmat(os.path.join(realeeg_path,'sub28_lxz_20171102.mat'),'data')
    mrcp = np.mean(eeg,2)
    
    nroi = len(chan_select)
    for i in range(nroi):
        f,axarr = plt.subplots(len(chan_select[i]),1,sharex=True)
        for j in range(len(chan_select[i])):
            idx = np.where(label==chan_select[i][j])[0]
            
            if len(idx) == 0:
                continue
            
            idx = idx[0]
            axarr[j].plot(mrcp[idx,:])
            axarr[j].set_ylim(bottom=-3,top=3)
            
            for cp in cpnts_sub:
                axarr[j].axvline(x=cp,alpha=0.5,linestyle='--',color='orange')
            
            if j == len(chan_select[i])-1:
                axarr[j].set_xticklabels(['-1900','-1500','-1100','-700','-300','100'])
                axarr[j].set_xlabel('Time (ms)')
        
        plt.tight_layout()
        plt.savefig(os.path.join(dir_vis,'mrcp_ts/U','mrcp_sub28_roi'+str(i)+'.png'),dpi=300)
    
    
    
    return


if __name__ == "__main__":
    main()

