# Current Preprocessing Workflow

## code

`eegAnalysis/newPipeline.m`

## workflow

1. excludes all resting periods and aligns the rest in their natural time order
2. filter to [1 100]Hz
3. for every epoch, baseline correction using the [-1500 -500]ms time period
4. remove epochs that are seriously contaminated by outliers
5. detect bad channels using band power at 50 Hz
6. (optional) clean line noise at 50Hz and 100Hz
    - high time complexity, totally not necessary
7. low-pass filter data to [1 40]Hz, with cutoff frequencies at [0.5 45] Hz
8. (optional) interpolate some outliers
    - not necessary
    - only interpolate in the most conservative manner
9. reject some bad epochs
    - epochs that contains value that exceeds `abs(valEdge)`, $valEdge = 500\mu V$
    - for every channel, find epochs whose `epochSD > max(prctile(chanstd, softEdge), hardEdge)`
    - for each epoch, count the number of channels where they are marked as `bad` at step 2
    - reject epochs if `bad > prctile(chancnt,cutpct)`
10. detect and remove bad channels using band power and standard deviation
    - flatline
    - high standard deviation: crazy channel
    - hugh 50Hz power line noise: EEG electrode loses contact with scalp
11. run ICA (infomax ICA)
12. (manual) reject IC components that contain artifacts, including eye blinks, EOG, EMG, pulses
    - usually the first two ICs have the greatest impact
    - usually only the first 20 ICs need to be inspected
12. (optional) surface Laplacian: use Perrin method
13. (optional) go to the frequency domain
    - filter to [8 12]Hz
    - then use Hilbert transform
14. (optional) go to the time-frequency domain: morlet wavelet transform

## miscellaneous

* for empirical data analysis, go to `eegAnalysis/report` in this respository, refer to `data analysis*.md`
* technical discussion
    - common ICA artifacts `eegAnalysis / report / data analysis4.md`
* previously, the preprocessing is done in another way `eegAnalysis/README_deprecated.md`
    - follow the `makoto's preprocessing pipeline`
    - use the ASR method and wICA
    - no manual independent components rejection