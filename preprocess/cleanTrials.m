function [] = cleanTrials(errTrials, filepath, format, savepath)
p = inputParser;
addRequired(p, 'errTrials', @isstruct);
addRequired(p, 'filepath', @ischar);
addRequired(p, 'format', ...
    @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savepath', @ischar);
parse(p, errTrials, filepath, format, savepath);

errTrials = p.Results.errTrials;
filepath = p.Results.filepath;
format = p.Results.format;

filelist = dir(fullfile(filepath, format));
filelist = {filelist.name};
totalFile = length(filelist);
trialStart = {'U', 'V', 'W', 'X'};

for sub = 1 : totalFile     
    switch format
        case '*.set'
            EEG = pop_loadset(char(filelist(sub)), filepath);
        case '*.vhdr'
            EEG = pop_loadbv(filepath, char(filelist(sub)));
        otherwise
            warning('Unexpected file type.\n');
    end
    
    if ~sum(errTrials(sub).trial == -1)
       [~, startIdx, ~] = getStart(EEG, trialStart, true);
        l = length(errTrials(sub).trial);
        for t = 0 : l - 1
            s = errTrials(sub).idx(l - t);
            if l > 1
                e = startIdx(errTrials(sub).trial(l - t) + 1);
            else
                e = startIdx(errTrials(sub).trial + 1);
            end
            for i = 1 : e - s, EEG.event(e - i) = []; end
        end
    end
    
    pop_saveset(EEG, 'filename', char(filelist(sub)), ...
        'filepath', p.Results.savepath);
end
end