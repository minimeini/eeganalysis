function [] = save_chan(EEG, filename, savepath)
    if exist(savepath, 'dir') ~= 7, mkdir(savepath); end
    
    for i = 1 : 64
        fprintf('*');
        data = squeeze(EEG(i, :, :));
        save(fullfile(savepath, [filename '_chan' num2str(i) '.mat']), ...
            'data', '-v7.3');
    end
    fprintf('\n');
end