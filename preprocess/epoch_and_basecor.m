function [] = epoch_and_basecor(loadpath, savepath1, savepath2, folderlist)
epochEEG(loadpath, savepath1, ...
    'H', [-2 1], 'labelB', 'h', 'timeIntB', [-0.5 0], 'partition', true);
% baseline correction
for t = 1 : numel(folderlist)
    epochpath = fullfile(savepath1, [char(folderlist(t)) '_-2_1']);
    basepath = fullfile(savepath1, [char(folderlist(t)) '_-0.5_0']);
    savepath = fullfile(savepath2, char(folderlist(t)));
    
    checkPath(savepath);
    epochfile = getFilename(epochpath, '*.set');
    basefile = getFilename(basepath, '*.set');
    for sub = 1 : numel(epochfile)
        epoch = pop_loadset(char(epochfile(sub)), epochpath);
        base = pop_loadset(char(basefile(sub)), basepath);
        meanBaseCor(epoch, base, 'savepath', savepath, ...
            'filename', char(epochfile(sub)));
    end
end

end