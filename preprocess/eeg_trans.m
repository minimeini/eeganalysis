function [] = eeg_trans(task, loadpath, savepath, varargin)
    p = inputParser;
    addRequired(p, 'task', @ischar);
    addRequired(p, 'loadpath', @ischar);
    addRequired(p, 'savepath', @ischar);
    addParameter(p, 'frex', logspace(log10(2),log10(30),30), @isnumeric);
    addParameter(p, 'demean', false, @islogical)
    addParameter(p, 'G', [], @isnumeric);
    addParameter(p, 'H', [], @isnumeric);
    parse(p, task, loadpath, savepath, varargin{:});
    
    checkPath(p.Results.savepath);
    [filelist, totalFile] = getFilename(p.Results.loadpath, '*.set');
    switch p.Results.task
        case 'laplacian'
            if isempty(p.Results.G) || isempty(p.Results.H)
                disp('G and H matrices are required for Laplacian.');
                return
            end
            for sub = 1 : totalFile
                EEG = pop_loadset(char(filelist(sub)), p.Results.loadpath);
                size_ = size(EEG.data);
                tmpEEG = CSD(reshape(EEG.data, size_(1), []), ...
                    p.Results.G, p.Results.H);
                EEG.data = reshape(tmpEEG, size_);
                if p.Results.demean, EEG.data = demeaning(EEG.data); end                
                pop_saveset(EEG, 'filename', char(filelist(sub)), ...
                    'filepath', p.Results.savepath);
            end

        case 'timef'
            for sub = 1 : totalFile
                if isempty(p.Results.frex)
                    frex = linespace(1, 40, 30);
                else
                    frex = p.Results.frex;
                end
                cycles = logspace(log10(3), log10(8), 30);
                
                EEG = pop_loadset(char(filelist(sub)), p.Results.loadpath);
                fname = regexp(char(filelist(sub)), '\.', 'split');
                fname = char(fname(1));
                data = zeros(EEG.nbchan, 30, 200, EEG.trials);
                chanlocs = EEG.chanlocs;
                if p.Results.demean, EEG.data = demeaning(EEG.data); end
                
                for c = 1 : EEG.nbchan
                    [data(c, :, :, :), freqs, times] = ...
                        timefreq(squeeze(EEG.data(c, :, :)), EEG.srate, ...
                        'cycles', cycles, 'freqs', frex, 'freqscale', 'linear', ...
                        'tlimits', [EEG.xmin * 1000 EEG.xmax * 1000]);
                end
                % # of cycles: number of local minima/maxima in a wavelet                 
                % wintime (ms) = winsize * 1000 / srate;
                % winsize (# of sample pnts) = srate*cycles(1)/frex(1)
                save(fullfile(savepath, [fname '.mat']), 'data', ...
                    'freqs', 'times', 'chanlocs');
            end
    end
            
end

function [outEEG] = demeaning(EEGdata)
% EEGdata = [chan * pnts * trials]
outEEG = zeros(size(EEGdata));
for i = 1 : size(EEGdata, 1)
    for j = 1 : size(EEGdata, 3)
        outEEG(i, :, j) = EEGdata(i, :, j) - squeeze(mean(EEGdata(i, :, j)));
    end
end
end