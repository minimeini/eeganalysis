function eeg_ft = eeg_re_reference(eeg_st, reference, varargin)
% function [] = eeg_re_reference(eeg_st, reference, varargin)
%
% INPUT
% - required: eegdata, reference
% - optional: bad_chans, interpolate
%
% DETAILS
% - eegdata: structure
% - reference: {'CAR', 'SL', 'both'}
% - bad_chans: indices of bad channels
% - interpolate: {0,1} or {true, false}

if ~isstruct(eeg_st)
    error('eegdata: structure, bst or ft or eeglabs');
end
if ~ismember(reference, {'CAR', 'SL', 'both'})
    error('reference: CAR or SL or both');
end

nrequired = 2;
bad_chans = []; interpolate = 0;
if nargin >= nrequired + 1
    if ~isnumeric(varargin{1})
        error('Indices of bad channels should be integers');
    end
    bad_chans = varargin{1};
end
if nargin >= nrequired + 2
    if ~isnumeric(varargin{2}) && ~islogical(varargin{2})
        error('Indicator of interpolation should be {1,0} or {true,false}');
    end
    interpolate = double(varargin{2});
end

%% Interpolation or Drop Bad Channels
nselect = eeg_st.nbchan;

if interpolate == 1
    eeg_ft = trans_eegdata(eeg_st, 'ft');
    eeg_ft.elec = ft_convert_units(eeg_ft.elec, 'mm');
    
    cfg = [];
    cfg.method = 'spline';
    cfg.badchannel = eeg_ft.elec.label(bad_chans)';
    cfg.missingchannel = cfg.badchannel';
    cfg.trials ='all';
    eeg_ft = ft_channelrepair(cfg, eeg_ft);
else
    if ~isempty(bad_chans)
        eeg_st = pop_select(eeg_st, 'nochannel', bad_chans);
    end
    nselect = nselect - numel(bad_chans);
    eeg_ft = trans_eegdata(eeg_st, 'ft');
    eeg_ft.elec = ft_convert_units(eeg_ft.elec, 'mm');
end

% redo baseline correction after interpolation
eeg_eeglab = trans_eegdata(eeg_ft, 'eeglab');
base_itrv = [eeg_eeglab.times(1) ...
    min(eeg_eeglab.times(1)+1000, eeg_eeglab.times(end))];
baseline = calcBaseline(eeg_eeglab, eeg_eeglab.times, base_itrv);
eeg_eeglab.data = eeg_eeglab.data - baseline;

% transfer to ft format
eeg_ft = trans_eegdata(eeg_eeglab, 'ft');
eeg_ft.elec = ft_convert_units(eeg_ft.elec, 'mm');
%% Reference: CAR, SL, CAR+SL
rawdata = get_rawdata(eeg_ft, 'ft');

% config
car = repmat(mean(rawdata,1), nselect, 1);

cfg_sl = [];
cfg_sl.method = 'spline';
cfg_sl.lambda = 1e-05;
cfg_sl.order = 4;
cfg_sl.degree = 14;

%
switch reference
    case 'CAR'
        rawdata = rawdata - car;
        for i = 1:numel(eeg_ft.trial)
            eeg_ft.trial{i} = rawdata(:,:,i);
        end
    case 'SL'
        eeg_ft = ft_scalpcurrentdensity(cfg_sl, eeg_ft);
    case 'both'
        rawdata = rawdata - car;
        for i = 1:numel(eeg_ft.trial)
            eeg_ft.trial{i} = rawdata(:,:,i);
        end
        eeg_ft = ft_scalpcurrentdensity(cfg_sl, eeg_ft);
    otherwise
        error('Invalid re-referencing scheme.');
end

%% remove interpolated channels, if exists
if interpolate == 1
    selchan = ft_channelselection(['all', arrayfun(@(x) ...
        ['-' eeg_ft.elec.label{bad_chans(x)}], 1:numel(bad_chans), ...
        'UniformOutput', 0)], eeg_ft.elec.label);
    cfg = [];
    cfg.channel = selchan;
    eeg_ft = ft_selectdata(cfg, eeg_ft);
%     eeg_ft = drop_badchans(eeg_ft, bad_chans);
end

end

%% Utils: drop bad channels
function eeg_trim = drop_badchans(eeg_st, bad_chans)
eeg_trim = trans_eegdata(eeg_st, 'ft');
eeg_trim.elec = ft_convert_units(eeg_trim.elec, 'mm');
nbchan = numel(eeg_trim.label);


if ~isempty(bad_chans)
    chan_select = setdiff(1:nbchan, bad_chans);
else
    chan_select = 1:nbchan;
end

eeg_trim.label = eeg_trim.label(chan_select);
trials = arrayfun(@(x) eeg_trim.trial{x}(chan_select, :), ...
    1:numel(eeg_trim.trial), 'UniformOutput', 0);
eeg_trim.trial = trials;
eeg_trim.elec.label = eeg_trim.label;
eeg_trim.elec.elecpos = eeg_trim.elec.elecpos(chan_select, :);
eeg_trim.elec.chanpos = eeg_trim.elec.chanpos(chan_select, :);
eeg_trim.elec.chantype = eeg_trim.elec.chantype(chan_select, :);
eeg_trim.elec.chanunit = eeg_trim.elec.chanunit(chan_select, :);

end