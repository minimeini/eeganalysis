function EEG = read_by_sub(sub, wd, varargin)
    fprintf('subject: %d \n',sub);
    cd(fullfile(wd, ['sub' num2str(sub)]));
    files = dir('*.mat');
    files = {files.name};
        
    fprintf('Load and concatenate epochs. \n');
    for f = 1 : length(files)
        if ~ismember(f, 1:length(files))
            warning('skip one unexisted file.\n');
            continue;
        end

        fprintf('*');
        eeg = load(fullfile([wd '\sub' num2str(sub)], ...
            char(files(f))));
        eeg = eeg.data;
        if f == 1
            EEG = double(eeg);
        else
            EEG = cat(3, EEG, double(eeg));
        end   
    end
    fprintf('\n');
    
    if nargin == 4
        fprintf('Surface Laplacian transform. \n');
        size_ = size(EEG);
        EEG = CSD(reshape(EEG, size_(1), []), varargin{1}, varargin{2});
        EEG = reshape(EEG, size_);
    end
    fprintf('Loading done. \n');
end