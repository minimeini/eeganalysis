function [EEG] = rankDeficientICA(EEG, type, varargin)
% EEG.icawinv = pinv(EEG.icaweights*EEG.icasphere) inv of weights = mix (A)
% EEG.icachansind = 1 : EEG.nbchan
% EEG.icasphere = sphere
% EEG.icaweights = weights
% unmixing matrix: W = weights * sphere
% activations: EEG.icaact = activations = ICAcomps = W * EEGdata >> dim = EEG.data

p = inputParser;
addRequired(p, 'EEG', @isstruct);
addRequired(p, 'type', ...
    @(x) any(validatestring(x, {'runica', 'fastica', 'robustica', 'amica'})));
addParameter(p, 'icarank', 1, @isnumeric);
parse(p, EEG, type, varargin{:});

EEG = p.Results.EEG;
tmpdata = reshape(EEG.data, size(EEG.data, 1), []);
if p.Results.icarank > 1
    icarank = p.Results.icarank;
elseif p.Results.icarank == 1
    icarank = getrank(tmpdata);
end

switch p.Results.type
    case 'runica'
        if icarank > 0
            [EEG.icaweights, EEG.icasphere, ~, ~, ~, ~, EEG.icaact] = ...
                runica(tmpdata, 'pca', icarank, 'verbose', 'on');
        else
            [EEG.icaweights, EEG.icasphere, ~, ~, ~, ~, EEG.icaact] = ...
                runica(tmpdata, 'verbose', 'off');
        end
        EEG.icawinv = pinv(EEG.icaweights * EEG.icasphere);
        
    case 'fastica'
        [EEG.icaact, EEG.icawinv, EEG.icaweights] = fastica(tmpdata, ...
            'verbose', 'off', 'displayMode', 'off');
        EEG.icasphere = eye(EEG.nbchan);
        
    case 'robustica'
        [EEG.icaact, EEG.icawinv, ~, EEG.icaweights] = robustica(tmpdata, ...
            {'prewhi', false, 'deftype', 'regression', 'dimred', true});
        EEG.icasphere = eye(EEG.nbchan);
        
    case 'amica'
        if icarank > 0
            [EEG.icaweights, EEG.icasphere, ~] = runamica15(tmpdata, ...
                'pcakeep', icarank, 'write_nd', 0, 'write_llt', 0);
        else
            [EEG.icaweights, EEG.icasphere, ~] = runamica15(tmpdata);
        end
        EEG.icawinv = pinv(EEG.icaweights * EEG.icasphere);
        EEG.icaact = reshape(EEG.icaweights * EEG.icasphere * tmpdata, ...
            icarank, [], EEG.trials);
end

EEG.icachansind = 1 : EEG.nbchan;
end