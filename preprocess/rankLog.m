fp(1) = {fullfile(wd_prep, 'filtered_1_30')};
fp(2) = {fullfile(wd_prep, 'robustCAR2_1_30')};
folderlist = {'U', 'V', 'W', 'X'};
fp(3) = {fullfile(wd_prep, 'basecor2_1_30')};
fp(4) = {fullfile(wd_prep, 'cleanEpoch3_1')};
fp(5) = {fullfile(wd_prep, 'AMICA_1')};
fp(6) = {fullfile(wd_prep, 'cleanEpoch3_2')};
epochStart = 3;
orderlist = fileOrder(char(fp(1)), '*.set');
%%
ranklog = zeros(length(orderlist), epochStart - 1);
for f = 1 : epochStart - 1
    [filename, totalfile] = getFilename(char(fp(f)), '*.set');
    for s = 1 : totalfile
        EEG = pop_loadset(char(filename(s)), char(fp(f)));
        ranklog(s, f) = getrank(reshape(EEG.data, EEG.nbchan, []));
    end
end
save(fullfile(wd_ana, 'ranklog.mat'), 'ranklog');