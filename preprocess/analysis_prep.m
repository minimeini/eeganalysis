addpath('D:\Study Resources\neuroscience\code\preprocess');
addpath('D:\Study Resources\neuroscience\code\analysis');
wd = 'D:/Study Resources/neuroscience/data';

sub = 1;
cd([wd '/sub' num2str(sub)]);
files = dir('*.vhdr');
files = {files.name}; 

f = 1;
EEG = pop_loadbv([wd '/sub' num2str(sub)], char(files(1)));
EEG.data = double(EEG.data);

[letters, ~, subs] = unique({EEG.event.type});
countCell = num2cell(accumarray(subs(:),1,[],@sum));
output = [letters.' countCell];

for trial = 3 : length(EEG.event)
    if mod(trial - 3, 6) == 0
        EEG.event(trial).type = 'S  11';
    end
end

latency = cell2mat({EEG.event.latency});
latency = latency / 500;
latency = [0 diff(latency)];

events = {EEG.event.type};
la_black = latency(strcmp(events, 'S  2'));
la_end = latency(strcmp(events, 'S  11'));

