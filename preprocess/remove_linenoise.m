function [] = remove_linenoise(wd_prep, operfolder, noiseloc, bandwidth)
% folder label
if numel(noiseloc)>1
    label = [num2str(noiseloc(1)) '-'];
    for i = 2:numel(noiseloc)
        label = [label num2str(noiseloc(i)) '-'];
    end
else
    label = [num2str(noiseloc) '-'];
end

label = [label 'w' num2str(bandwidth) '_'];

% remove line noise
[filename,~,filelist] = ...
    getFilename(fullfile(wd_prep, operfolder), '*.set');
savepath = fullfile(wd_prep, ['cl' label operfolder]);
checkPath(savepath);
for i = 1:numel(filelist)
    fname = char(filename(i)); fname = fname(1:end-4);
    eeg = pop_loadset(char(filelist(i)));
    
    eeg = pop_cleanline(eeg, 'ChanCompIndices', 1:eeg.nbchan, ...
        'Bandwidth', bandwidth, 'SignalType','Channels', ...
        'ComputeSpectralPower', false, 'LineAlpha', 0.05, ...
        'LineFrequencies', noiseloc, 'ScanForLines', true);
    
    pop_saveset(eeg, 'filename', fname, 'filepath', savepath);
end
end