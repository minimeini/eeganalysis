% delete epoches which contains extreme value (90% percentile)
% that exceed a certain length of window (95% percentile)
function [] = remove_outliers(wd_prep, filedir, val, cutpct)
[~,~,filelist] = ...
    getFilename(fullfile(wd_prep, filedir), '*.set');
savepath = fullfile(wd_prep, ['reve_' filedir]);
checkPath(savepath);

for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    filename = char(fname(end));
    filename = filename(1:end-4);
    
    eeg = pop_loadset(char(filelist(i)));
    if numel(size(eeg.data)) == 2
        eeg = pop_epoch(eeg, {'U', 'V', 'W', 'X'}, [-1.5 0.5]);
    end
    
    eegcat = reshape(eeg.data, 64, []);
    ntimes = size(eegcat, 2);
    
    max = prctile(abs(eegcat), val, 2); 
    ecntdata = squeeze(sum(sum(...
        reshape(abs(eegcat)>repmat(max,1,ntimes), size(eeg.data)), ...
        2), 1));
    cutoff = prctile(ecntdata, cutpct);
    
    eegNew = pop_select(eeg, 'notrial', find(ecntdata>cutoff));
    eegconcat = eeg_epoch2continuous(eegNew);
    pop_saveset(eegconcat, 'filename', filename, ...
        'filepath', savepath);
end
