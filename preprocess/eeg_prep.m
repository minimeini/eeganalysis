function [] = eeg_prep(wd_prep, foldername, chanloc, freqrange, basecor)
warning('off', 'verbose');
savepath = ...
    ['filt' num2str(freqrange(1)) '-' num2str(freqrange(2))];
if basecor, savepath = ['bc_' savepath]; end
savepath = fullfile(wd_prep, [savepath '_' foldername]);
checkPath(savepath);

[filename, ~, filelist] = ...
    getFilename(fullfile(wd_prep,foldername), '*.set');
    
for sub = 1 : numel(filelist)
    fname = char(filename(sub));
    fname = fname(1:end-4);
        
    EEG = pop_loadset(char(filelist(sub)));           
    EEG.data = double(EEG.data); 
    EEG.chanlocs = chanloc;
    [EEG, ~, ~] = ...
        pop_eegfiltnew(EEG, freqrange(1), freqrange(2));

    if basecor
    % baseline correction using [-1.5 -0.5]s period
        baseline = calcBaseline(EEG, EEG.times, [-1500 -500]);
        EEG.data = EEG.data - baseline;
    end        
        
    pop_saveset(EEG, 'filepath', savepath, 'filename', fname);
end
end