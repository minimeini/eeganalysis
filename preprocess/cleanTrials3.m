function [] = cleanTrials3(filepath, format, savepath)
imgLabel = {'W', 'X'};
startLabel = {'U', 'V', 'W', 'X'};
filelist = dir(fullfile(filepath, format));
filelist = {filelist.name};
totalFile = length(filelist);

checkPath(savepath);

for sub = 1 : totalFile
    EEG = pop_loadset(char(filelist(sub)), filepath);
    imgStart = getStart(EEG, imgLabel, true);
    startStat = getStart(EEG, startLabel, true);
    totalImg = length(imgStart.idx);
    cnt = 1;
    errImg = -1;
    for i = 0 : totalImg - 1
        s = imgStart.idx(totalImg - i);
        if find(startStat.idx == s) < length(startStat.idx)
            e = startStat.idx(find(startStat.idx == s) + 1) - 1;
        else
            e = length(EEG.event);
        end
        
        if (e - s + 1) == 6, continue; end
        fprintf('Delete the %d th trial.\n', s);
        if cnt == 1
            errImg = s;
        else
            errImg = [errImg s];
        end
        for k = 0 : e - s, EEG.event(e - k) = []; end
        cnt = cnt + 1;
    end
    errLog(sub).total = totalImg;
    errLog(sub).idx = errImg;
    errLog(sub).bad = length(errImg);
    if errImg(1) == -1, errLog(sub).bad = 0; end
    
    fprintf('%d trials in total, %d trials were deleted.\n', ...
        totalImg, length(errImg));
    clear errImg
    pop_saveset(EEG, 'filename', char(filelist(sub)), 'filepath', savepath);
end
save(fullfile(savepath, 'cleanTrials3Log.mat'), 'errLog');

end