function [] = clean_epoch(total_sub, status_s2, type, wd_prep, pre_epoch, save_epoched)
p = inputParser;
addRequired(p, 'total_sub', @isnumeric);
addRequired(p, 'status_s2', @isstruct);
addRequired(p, 'type', ...
    @(x) any(validatestring(x, {'*.set', '*.mat'})));
addRequired(p, 'wd_prep', @ischar);
addRequired(p, 'pre_epoch', @ischar);
addRequired(p, 'save_epoched', @ischar);
parse(p, total_sub, status_s2, type, wd_prep, pre_epoch, save_epoched);

status_s2 = p.Results.status_s2;
wd_prep = p.Results.wd_prep;
pre_epoch = p.Results.pre_epoch;
save_epoched = p.Results.save_epoched;

for sub = p.Results.total_sub
    files = dir(fullfile(wd_prep, pre_epoch, p.Results.type));
    files = {files.name};
    pre_trials = 0;
    for f = 1 : length(files)
        switch p.Results.type
            case '*.mat'
                fprintf('subject %d: \n', sub);
                load(fullfile(wd_prep, pre_epoch, char(files(f))));
                temp = size(data, 3);
                data = data(:, :, status_s2(sub).status(1 + pre_trials : ...
                    temp + pre_trials) == 1);
                pre_trials = pre_trials + temp;
                save_epoch(sub, f, data, wd_prep, save_epoched);
            case '*.set'
                EEG = pop_loadset(char(files(f)), ...
                    fullfile(wd_prep, pre_epoch));
                temp = EEG.trials;
                EEG.trials = sum(status_s2(sub).status(1 + pre_trials : ...
                    EEG.trials + pre_trials));
                EEG.data = EEG.data(:, :, status_s2(sub).status(1 + ...
                    pre_trials : temp + pre_trials) == 1);
                
                if ~isempty(EEG.epoch)
                    EEG.epoch = EEG.epoch(status_s2(sub).status(1 + ...
                        pre_trials : temp + pre_trials) == 1);
                end
                EEG.event = [];
                pre_trials = pre_trials + temp;
                save_epoch(EEG, char(files(f)), ...
                    fullfile(wd_prep, save_epoched));
            otherwise
                error('Invalid format.\n');
        end
    end
    fprintf('\n');
end
end