function [] = partitionAll(filepath, format, savedir)
p = inputParser;
addRequired(p, 'filepath', @ischar);
addRequired(p, 'format', ...
    @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savedir', @ischar);
parse(p, filepath, format, savedir);

format = p.Results.format;
filepath = p.Results.filepath;
savedir = p.Results.savedir;

filelist = dir(fullfile(filepath, format));
filelist = {filelist.name};
totalFile = length(filelist);

for sub = 1 : totalFile
    switch format
        case '*.set'
            EEG = pop_loadset(char(filelist(sub)), filepath);
        case '*.vhdr'
            EEG = pop_loadbv(filepath, char(filelist(sub)));
    end
    [eeg_re, eeg_ri, eeg_le, eeg_li] = partition(EEG);
    savePart(eeg_re, 're', char(filelist(sub)), format, savedir);
    savePart(eeg_le, 'le', char(filelist(sub)), format, savedir);
    savePart(eeg_ri, 'ri', char(filelist(sub)), format, savedir);
    savePart(eeg_li, 'li', char(filelist(sub)), format, savedir);
end

end

function [eeg_re, eeg_ri, eeg_le, eeg_li] = partition(EEG)
p = inputParser;
addRequired(p, 'EEG', @isstruct);
parse(p, EEG);
EEG = p.Results.EEG;

re = 'U'; le = 'V'; ri = 'W'; li = 'X';
eeg_re = partitionType(EEG, re);
eeg_le = partitionType(EEG, le);
eeg_ri = partitionType(EEG, ri);
eeg_li = partitionType(EEG, li);

end

function [eeg_part] = partitionType(EEG, label)
p = inputParser;
addRequired(p, 'EEG', @isstruct);
addRequired(p, 'label', ...
    @(x) any(validatestring(x, {'U', 'V', 'W', 'X'})));
parse(p, EEG, label);
EEG = p.Results.EEG;

[~, startIdx, ~] = getStart(EEG, p.Results.label, true);
eeg_part = EEG; eeg_part.data = []; 
eeg_part.event = []; eeg_part.times = [];

for i = 1 : length(startIdx)
    s = EEG.event(startIdx(i)).latency - 500; % start of this section
    if ismember(label, {'U', 'V'})
        e_idx = startIdx(i) + 13;
    else
        e_idx = startIdx(i) + 5;
    end
    e = EEG.event(e_idx).latency + 500;
    dataTemp = EEG.data(:, s : e);
    eventTemp = EEG.event(startIdx(i) : e_idx);
    
    eeg_part.data = [eeg_part.data dataTemp];
    eeg_part.event = [eeg_part.event eventTemp];
end
eeg_part.times = 0 : 2 : size(eeg_part.data, 2);
eeg_part.pnts = size(eeg_part.data, 2);
eeg_part.xmax = (eeg_part.pnts - 1) / eeg_part.srate;
end

function [] = savePart(eeg_part, type, filename, format, savedir)
p = inputParser;
addRequired(p, 'eeg_part', @isstruct);
addRequired(p, 'type', ...
    @(x) any(validatestring(x, {'re', 'le', 'ri', 'li'})));
addRequired(p, 'filename', @ischar);
addRequired(p, 'format', ...
    @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savedir', @ischar);

parse(p, eeg_part, type, filename, format, savedir);
format = p.Results.format;
type = p.Results.type;
savedir = p.Results.savedir;

fname = p.Results.filename(1 : end - length(format) + 1);
if exist(fullfile(savedir, type), 'dir') ~= 7
    mkdir(fullfile(savedir, type))
end
pop_saveset(p.Results.eeg_part, 'filename', [fname '_' type format(2 : end)], ...
    'filepath', fullfile(savedir, type));
end