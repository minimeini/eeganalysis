foldername = 'car_nc_revo20-24_lp30_cl50-100-w2_rev_bc_filt1-100_norest';
[filename, ~, filelist] = ...
    getFilename(fullfile(wd_prep, foldername), '*.set');

%% change for .set format to .mat format for the epoched data
[~,~,filelist] = getFilename(fullfile(wd_prep, ['epoch_' foldername]));
[~,~,filelist] = getFilename(filelist(1));
filelist = filelist(contains(filelist, 'set'));
savepath = fullfile(wd_prep, ['epoch_' foldername '_mat']);

for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    taskn = char(fname(end-1));
    fname = regexp(char(fname(end)), '\_', 'split');
    fname = char(fname(1));
    
    eeg = pop_loadset(char(filelist(i)));
    data = eeg.data;
    times = eeg.times;
    savep = fullfile(savepath, taskn); 
    checkPath(savep);
    save(fullfile(savep, [fname '.mat']), 'data', 'times');
end