function [] = eeg_clean(filepath, filetype, savepath, typerej, rej_type, rejthresh) 
p = inputParser;
addRequired(p, 'filepath', @ischar);
addRequired(p, 'filetype', @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savepath', @ischar); % '*.type'
addRequired(p, 'typerej', @isnumeric);
addRequired(p, 'rej_type',  @(x) any(validatestring(x, ...
    {'all', 'thresh', 'improb', 'kurt', 'spec', 'none'})));
addRequired(p, 'rejthresh', @isnumeric);
parse(p, filepath, filetype, savepath, typerej, rej_type, rejthresh);
rejthresh = p.Results.rejthresh;

if exist(savepath, 'dir') ~= 7, mkdir(savepath); end
[filelist, totalFile] = getFilename(p.Results.filepath, p.Results.filetype);

for sub = 1 : totalFile
    filename = char(filelist(sub));
    EEG = pop_loadset(filename, p.Results.filepath);
    EEG.data = double(EEG.data);
    
    if typerej == 0
        selected = size(EEG.icaact, 1);
    else
        selected = size(EEG.data, 1);
    end
    
    switch p.Results.rej_type
        case 'all' 
            EEG = rej_thresh(EEG, typerej, rejthresh, -25, 25); 
%             EEG = rej_improb(EEG, typerej, rejthresh, 7, 7); 
            EEG = pop_jointprob(EEG, typerej, 1 : selected, 6, 6, 0, 1);
%             EEG = rej_kurt(EEG, typerej, rejthresh, 7, 7);
%             EEG = rej_spec(EEG, typerej, rejthresh, [-38 38]);
        case 'thresh'
            EEG = rej_thresh(EEG, typerej, EEG.trials,  -20, 20);
        case 'improb'
            EEG = rej_improb(EEG, typerej, rejthresh, 7, 7);
        case 'kurt'
            EEG = rej_kurt(EEG, typerej, rejthresh, 7, 7);
        case 'spec'
            EEG = rej_spec(EEG, typerej, rejthresh, [-38 38]);
        case 'none'
    end
    pop_saveset(EEG, filename, p.Results.savepath);
end

end

function [EEGOUT, idx] = rej_thresh(EEG, typerej, rejthresh, lowthresh, upthresh)
idx = 1 : EEG.trials;
trials = EEG.trials;
if typerej == 0
    selected = size(EEG.icaact, 1);
else
    selected = size(EEG.data, 1);
end
while length(idx) > ceil(rejthresh * trials)
    lowthresh = lowthresh - 2;
    upthresh = upthresh + 2;
    if isstruct(EEG)
        [~, idx] = pop_eegthresh(EEG, typerej, 1 : selected, ...
            lowthresh, upthresh, EEG.xmin, EEG.xmax, 0, 0);
    elseif isnumeric(EEG)
        frames = size(EEG, 2) / trials;
        [~, idx] = eegthresh(EEG, frames, 1 : selected, ...
            lowthresh, upthresh, [0 frames], 0, frames);
    end
end

if isstruct(EEG)
    EEGOUT = pop_rejepoch(EEG, idx, 0);
elseif isnumeric(EEG)
    [~, ~, EEGOUT] = eegthresh(EEG, frames, 1 : selected, ...
        lowthresh, upthresh, [0 frames], 0, frames);
end
end

function [EEGOUT, idx] = rej_improb(EEG, typerej, rejthresh, locthresh, globthresh)
idx = EEG.trials;
if typerej == 0
    selected = size(EEG.icaact, 1);
else
    selected = size(EEG.data, 1);
end
while idx > ceil(rejthresh * EEG.trials)
    locthresh = locthresh + 0.3;
    globthresh = globthresh + 0.3;
    [~, ~, ~, idx] = pop_jointprob(EEG, typerej, 1 : selected, ...
        locthresh, globthresh, 0, 0);
end
EEGOUT = pop_jointprob(EEG, typerej, 1 : selected, locthresh, globthresh, 0, 1);
end

function [EEGOUT, idx] = rej_kurt(EEG, typerej, rejthresh, locthresh, globthresh)
idx = EEG.trials;
if typerej == 0
    selected = size(EEG.icaact, 1);
else
    selected = size(EEG.data, 1);
end
while idx > ceil(rejthresh * EEG.trials)
    locthresh = locthresh + 0.3;
    globthresh = globthresh + 0.3;
    [~, ~, ~, idx] = pop_rejkurt(EEG, typerej, 1 : selected, ...
        locthresh, globthresh, 0, 0);
end
EEGOUT = pop_rejkurt(EEG, typerej, 1 : selected, ...
    locthresh, globthresh, 0, 1);
end

function [EEGOUT, idx] = rej_spec(EEG, typerej, rejthresh, threshold)
idx = 1 : EEG.trials;
if typerej == 0
    selected = size(EEG.icaact, 1);
else
    selected = size(EEG.data, 1);
end
while length(idx) > ceil(rejthresh * EEG.trials)
    threshold(1) = threshold(1) - 5;
    threshold(2) = threshold(2) + 5;
    disp(threshold);
    [~, idx] = pop_rejspec(EEG, typerej, 'elecrange', 1 : selected, ...
        'threshold', threshold, 'freqlimits', [1 30], ...
        'method', 'multitaper');
end
EEGOUT = pop_rejspec(EEG, typerej, 'elecrange', 1 : size(EEG.icaact, 1), ...
    'threshold', threshold, 'freqlimits', [1 30], 'method', 'multitaper');
end