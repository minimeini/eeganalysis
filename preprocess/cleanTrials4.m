function [] = cleanTrials4(errTrials, filepath, format, savepath)
filelist = dir(fullfile(filepath, format));
filelist = {filelist.name};
totalFile = length(filelist);

if (totalFile ~= length(errTrials)) 
    warning('unmatched length.\n');
end

if exist(savepath, 'dir') ~= 7, mkdir(savepath); end

for sub = 1 : totalFile
    if strcmp(format, '*.vhdr')
        EEG = pop_loadbv(filepath, char(filelist(sub)));
    else
        EEG = pop_loadset(char(filelist(sub)), filepath);
    end
    [~, startIdx, ~] = getStart(EEG, {'U', 'V', 'W', 'X'}, true);  
    
    if errTrials(sub).bad > 0
        for i = 0 : errTrials(sub).bad - 1
            s = errTrials(sub).idx(errTrials(sub).bad - i);
            if find(startIdx == s) == length(startIdx)
                e = length(EEG.event);
            else
                e = startIdx(find(startIdx == s) + 1) - 1;
            end
            for j = 0 : e - s, EEG.event(e - j) = []; end
        end
    end
    
    pop_saveset(EEG, 'filename', char(filelist(sub)), 'filepath', savepath);
end

end