function [] = save_epoch(data, filename, savepath)
if exist(savepath, 'dir') ~= 7, mkdir(savepath); end
    
if isstruct(data)
    pop_saveset(data, 'filename', filename,  'filepath', savepath);
elseif isnumeric(data)
    save(fullfile(savepath, filename), 'data', '-v7.3');
else
    error('Invalid data type.');
end

end