% function [] = exclude_rest(wd_prep, foldername, concat)
% exclue resting period by epoching all the task sessions

function [] = exclude_rest(wd_prep, foldername, concat)

[filename,~,filelist] = getFilename(fullfile(wd_prep, ...
    foldername, '*.set'));
savepath = fullfile(wd_prep, 'norest');
if ~concat, savepath = [savepath '_epoch']; end
checkPath(savepath);

for i = 1:numel(filelist)
    filen = char(filename(i));
    filen = filen(1:end-4);
    
    eeg = pop_loadset(char(filelist(i)));
    
    [~, startH] = getStart(eeg, 'H', true);
    [~, starth] = getStart(eeg, 'h', true);
    [~, startU] = getStart(eeg, 'U',true);
    [~, startV] = getStart(eeg, 'V',true);
    [~, startW] = getStart(eeg, 'W',true);
    [~, startX] = getStart(eeg, 'X',true);
    if numel(startU)+numel(startV)+...
            numel(startW)+numel(startX)~=numel(startH) || ...
            numel(starth)~=numel(startH)
        error('missing labels.\n');
    end
    
    for j = 1:numel(startH)
        eeg.event(startH(j)).type = eeg.event(startH(j)-2).type;
        eeg.event(startH(j)-2).type = 'H';
    end

    eeg = pop_epoch(eeg, {'U', 'V', 'W', 'X'}, [-1.5 2]);
    % baseline correction before concatenate
    if concat
        eeg = eeg_epoch2continuous(eeg);
    end
    
    pop_saveset(eeg, 'filename', filen, 'filepath', savepath);
end
end

