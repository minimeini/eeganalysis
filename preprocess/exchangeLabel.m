function [EEG, status] = exchangeLabel(EEG, label_A, label_B)
status = 0;
[~, startA] = getStart(EEG, label_A, true);
[~, startB] = getStart(EEG, label_B, true);

if numel(startA) ~= numel(startB)
    error('length of vectors are not equal.');
end

for i = 1 : length(startA)
    temp = EEG.event(startA(i)).type;
    EEG.event(startA(i)).type = EEG.event(startB(i)).type;
    EEG.event(startB(i)).type = temp;
end
end