function [EEG] = labelClean(EEG)
    eventList = ...
        {'o', 'O', 'y', 'Y', 'g', 'G', 'r', 'R', 'b', 'B',...
        'p', 'P',  'c', 'C', 'm', 'M', 'h', 'H', 'i', 'I',...
        'U', 'V', 'W', 'X', 'Z'};
    trialEnd = 'Z';

    totalEvent = length(EEG.event);
    for i = 1 : totalEvent
        EEG.event(i).type = char(str2double(EEG.event(i).type(2:4)));
        EEG.urevent(i).type = char(str2double(EEG.urevent(i).type(2:4)));
    end

    latdiff = [diff([EEG.event.latency]) 9999];
    for i = 1 : totalEvent - 1
        if latdiff(totalEvent - i) == 1
            EEG.event(totalEvent - i) = [];
            EEG.urevent(totalEvent - i) = [];
        end
    end

    totalEvent = length(EEG.event);
    for i = 0 : totalEvent - 1
        if ~ismember(EEG.event(totalEvent - i).type, eventList)
            EEG.event(totalEvent - i) = [];
            EEG.urevent(totalEvent - i) = [];
        end
    end
    
    EEG.event(end + 1) = EEG.event(end);
    EEG.event(end).type = trialEnd;
end

