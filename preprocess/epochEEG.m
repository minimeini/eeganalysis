function [] = epochEEG(filelist, savepath, labelA, timeIntA, varargin)
p = inputParser;
addRequired(p, 'filelist', @(x) ischar(x)||iscell(x));
addRequired(p, 'savepath', @ischar);
addRequired(p, 'labelA', @ischar);
addRequired(p, 'timeIntA', @isnumeric);

% addParameter(p, 'labelB', [], @ischar);
% addParameter(p, 'timeIntB', [], @isnumeric);
addParameter(p, 'partition', false, @islogical);

parse(p, filelist, savepath, labelA, timeIntA, varargin{:});
checkPath(p.Results.savepath);

for sub = 1 : numel(filelist)
    fname = regexp(char(filelist(sub)), '\\', 'split');
    fname = regexp(char(fname(end)), '\.', 'split');
    fname = char(fname(1));
    
    EEG = pop_loadset(char(filelist(sub)));
    
    partLab = {'U', 'V', 'W', 'X'};
    labelA = p.Results.labelA;
    if p.Results.partition
        temp = partLab;
        partLab = labelA; % labelA
        labelA  = temp; % U/V/W/X
        [EEG, status] = exchangeLabel(EEG, partLab, labelA);
        if status, return; end
    end
    
    for i = 1 : length(labelA)
        EEGpartA = pop_epoch(EEG, {char(labelA(i))}, p.Results.timeIntA);
        savepathNew = fullfile(p.Results.savepath, char(labelA(i)));
        checkPath(savepathNew);
        pop_saveset(EEGpartA, 'filename', fname, ...
            'filepath', savepathNew);
    end
    
%     if isempty(p.Results.labelB) || isempty(p.Results.timeIntB)
%         continue;
%     end
%     labelB = p.Results.labelB;
    
    % cancel out the exchange first
%     if ~isempty(p.Results.partition) && p.Results.partition
%         temp = partLab;
%         partLab = labelA;
%         labelA  = temp;
%         [EEG, status] = exchangeLabel(EEG, partLab, labelA);
%         if status, return; end
        % p.Results.labelA = labelA
        % partLab = U/V/W/X
%         temp = partLab;
%         partLab = labelB; % labelB
%         labelB = temp; % U/V/W/X
%         [EEG, status] = exchangeLabel(EEG, partLab, labelB);
%         if status, return; end
%     end    
    
%     for i = 1 : length(labelB)
% %         disp(char(labelB(i)));
%         EEGpartB = pop_epoch(EEG, {char(labelB(i))}, p.Results.timeIntB);
%         savepathNew = fullfile(p.Results.savepath, ...
%             [char(labelB(i)) '_'...
%             num2str(min(p.Results.timeIntB)) '_' ...
%             num2str(max(p.Results.timeIntB))]);
%         checkPath(savepathNew);
%         pop_saveset(EEGpartB, 'filename', fname,  ...
%             'filepath', savepathNew);
%     end     
    
end
end