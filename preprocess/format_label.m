function EEGOUT = format_label(EEG)
    % add 'S  1' label if not exists
    if sum(ismember({EEG.event.type}, 'S  1')) == 0
        new_event = EEG.event(1);
        new_event.type = 'S  1';
        EEG.event = [EEG.event(1:2), new_event, EEG.event(3:end)];
    end
    
    % remove useless 'S  1' label
    [letters, ~, subs] = unique({EEG.event.type});
    countCell = num2cell(accumarray(subs(:),1,[],@sum));
    output = [letters.' countCell];            
    cnt = cell2mat(output(strcmp(output(:,1),'S  1'),2));
    if cnt ~= 1
        idx = find(strcmp({EEG.event.type}, 'S  1'));
        for i = sort(idx, 'descend')
            EEG.event(i) = [];
        end
    end

    EEGOUT = EEG;
end