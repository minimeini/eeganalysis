%% Analyze the quality of different preprocessing scheme
% 1. the range of a good EEG time series should be falled in [-30 30] \muV
%    - mean, min, max, std
% 2. there should be at least an obscure ERP pattern in the central of
% brain
%    - the channels which show BP patterns
%    - the characteristic of BP patterns: range, variance
% 3. visualize ERP

foldername = 'epoch_car_revo15-24_lp30_cl50-100-w2_rev_bc_filt1-100_norest';
[~,~,filelist] = getFilename(fullfile(wd_prep, [foldername '_mat']));
[~,~,filelist] = getFilename(char(filelist(1)));
% run('re_epoch.m');
%% detect channels with BP patterns
bpchan = zeros(numel(filelist), 64+1);
for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    fname = regexp(char(fname(end)), '\.', 'split');
    fname = char(fname(1));
    fname = str2double(fname(4:end));
    
    eegdata = load(char(filelist(i)));
    
    [bpstat, erpstat] = detectBP(mean(eegdata.data,3), eegdata.times);
    bp_temp = bpstat(bpstat.slope_earlyBP<0 & bpstat.slope_lateBP<0, :);
    chan_temp = bp_temp.chan';
    bpchan(i, :) = [fname ismember(1:64, chan_temp)];
    
    checkPath(fullfile(wd_prep, 'analysis', [foldername '_erp']));
    writeToCSV(erpstat, ...
        fullfile(wd_prep, 'analysis', [foldername '_erp'], ...
        ['sub' num2str(fname) '_' foldername '_erpstat.csv']), ...
        {'min', 'mean', 'median', 'max', 'std'});
end

varnames = [{'sub'}, ...
    arrayfun(@(x) ['chan' num2str(x)],1:64,'UniformOutput',0)];

writeToCSV(bpchan, ...
    fullfile(wd_prep, 'analysis', [foldername '_erpchan.csv']), ...
    varnames);

bpstat = bpchan(:,2:end);
chanstat = sum(bpstat, 1); % sum by subjects
disp(mean(chanstat));
disp(max(chanstat));
find(chanstat>mean(chanstat))
find(chanstat==max(chanstat))

%% first averaged across trials, then averaged across subjects
for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    fname = regexp(char(fname(end)), '\.', 'split');
    fname = char(fname(1));
    fname = str2double(fname(4:end));
    
    eegdata = load(char(filelist(i)));
    if i==1, erp = zeros(size(eegdata.data,1), size(eegdata.data,2)); end
    erp = squeeze(mean(eegdata.data, 3)) + erp;
end
erp = erp ./ numel(filelist);
[bpstat, erpstat] = detectBP(erp, eegdata.times);
bp_temp = bpstat(bpstat.slope_earlyBP<0 & bpstat.slope_lateBP<0, :);
bp_temp.chan'

%% calculate intertrial variability
% evaluate how trial deviates from the across-trial-averaging ERP

var_chandeviant = {'minERP', 'meanERP', 'maxERP', ...
    'minSSTO', 'meanSSTO', 'medSSTO', 'maxSSTO'};
savepath = fullfile(wd_ana, ['ERPrange_' foldername], 'U');

for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    fname = regexp(char(fname(end)), '\.', 'split');
    fname = char(fname(1));
    
    eegdata = load(char(filelist(i)));
    eegdata = eegdata.data;
    erpdata = squeeze(mean(eegdata, 3));
    ntime = size(eegdata,2);
    chansse = zeros(size(eegdata,1), size(eegdata,3)); % chan * trial
    for j = 1:size(eegdata,3)
        eegtrial = squeeze(eegdata(:,:,j));
        chansse(:,j) = sqrt(1/(ntime-1) .* ...
            diag((eegtrial-erpdata) * (eegtrial-erpdata)'));
    end
    
    chandeviant = zeros(size(eegdata,1),7);
    chandeviant(:,1) = min(erpdata,[],2);
    chandeviant(:,2) = mean(erpdata,2);
    chandeviant(:,3) = max(erpdata,[],2);
    
    chandeviant(:,4) = min(chansse,[],2);
    chandeviant(:,5) = mean(chansse,2);
    chandeviant(:,6) = median(chansse,2);
    chandeviant(:,7) = max(chansse,[],2);  
    
    checkPath(savepath);
    writeToCSV(chandeviant, fullfile(savepath, [fname '.csv']), ...
        var_chandeviant);
end
%% calculate SNR
% for each channels, 
% SNRtrial = (sum(erp)/ntimes) / sqrt(1/(ntimes-1) .* 
%            (eegtrial-erp)' * (eegtrial-erp))
% SNRmean = mean(SNRtrial)

snrmean = zeros(numel(filelist), 64);
for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    fname = regexp(char(fname(end)), '\.', 'split');
    fname = char(fname(1));
    fname = str2double(fname(4:end));
    
    eegdata = load(char(filelist(i)));
    eegdata = eegdata.data;
    erpdata = squeeze(mean(eegdata,3));
    
    erpmean = mean(erpdata,2);
    
    ntrials = size(eegdata,3);
    ntimes = size(eegdata,2);
    nchans = size(eegdata,1);
    
    snrtrial = zeros(ntrials, nchans);
    for j=1:ntrials
        eegtrial = squeeze(eegdata(:,:,j));
        stdtrial = sqrt((1/(ntimes-1)) .* ...
            diag((eegtrial-erpdata) * (eegtrial-erpdata)'));
        snrtrial(j,:) = erpmean' ./ stdtrial';
    end
    
    snrmean(i,:) = squeeze(abs(mean(snrtrial,1)));
end

varnames = arrayfun(@(x) ['chan' num2str(x)], ...
    1:64, 'UniformOutput', 0);
writeToCSV(snrmean, ...
    fullfile(wd_ana, ['meanSNR_' foldername '.csv']), varnames);

%% change in SNR (%)
snr_base = csvread(fullfile(wd_ana, ...
    'meanSNR_epoch_car_revo15-24_lp30_cl50-100-w2_rev_bc_filt1-100_norest.csv'),1);
snr_this = csvread(fullfile(wd_ana, ...
    'meanSNR_epoch_car_intp_revo15-24_lp30_cl50-100-w2_rev_bc_filt1-100_norest.csv'), 1);

snr_rate = (snr_this - snr_base) ./ snr_base * 100;
writeToCSV(snr_rate, ...
    fullfile(wd_ana, ['snr_rate_nointp_intp_norevo.csv']), ...
    varnames);

%% visualize ERP
savepath = fullfile(wd_vis, ['erp_' foldername], 'U');
for i = 1:numel(filelist)
    fname = regexp(char(filelist(i)), '\\', 'split');
    fname = regexp(char(fname(end)), '\.', 'split');
    fname = char(fname(1));
    
    eegdata = load(char(filelist(i)));
    eventRelatedPotential(eegdata.data, 'XData', eegdata.times, ...
        'timerange', [-1000 500], 'title', [fname '-ERP'], ...
        'savepath', fullfile(savepath, fname));
end
