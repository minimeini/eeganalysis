function [baseline] = calcBaseline(eegepoch, times, timerange)
baseline = zeros(size(eegepoch.data));
for i = 1:eegepoch.nbchan
    for j = 1:eegepoch.trials
        eegtemp = squeeze(eegepoch.data(i,times>=timerange(1) & ...
            times<=timerange(2),j));
        eegtemp = eegtemp(eegtemp>prctile(eegtemp,5) & ...
            eegtemp<prctile(eegtemp,95));
        baseline(i,:,j) = mean(eegtemp);
    end
end
end