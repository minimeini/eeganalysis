function [] = cleanTrials2(homeDur, filepath, format, savepath)
p = inputParser;
addRequired(p, 'homeDur', @isstruct);
addRequired(p, 'filepath', @ischar);
addRequired(p, 'format', ...
    @(x) any(validatestring(x, {'*.set', '*.vhdr'})));
addRequired(p, 'savepath', @ischar);
parse(p, homeDur, filepath, format, savepath);

homeDur = p.Results.homeDur;
filepath = p.Results.filepath;
format = p.Results.format;

[filelist, totalFile] = getFilename(filepath, format);
trialStart = {'U', 'V', 'W', 'X'};

for sub = 1 : totalFile   
    if homeDur(sub).bad > 100, continue; end % too bad, discard this sub
    
    switch format
        case '*.set'
            EEG = pop_loadset(char(filelist(sub)), filepath);
        case '*.vhdr'
            EEG = pop_loadbv(filepath, char(filelist(sub)));
        otherwise
            warning('Unexpected file type.\n');
    end
    
    cnt = 1;
    trial = 0;
    for i = 1 : length(homeDur(sub).dur)
        if homeDur(sub).dur(i) < 1.5 * 500
            if cnt == 1, trial = i;
            else, trial = [trial i]; end
            cnt = cnt + 1;
        end
    end
    
    if trial(1) ~= 0
       [~, startIdx, ~] = getStart(EEG, trialStart, true);
        l = length(trial);
        for t = 0 : l - 1
            s = startIdx(trial(l - t));
            if trial(l - t) < length(startIdx)
                e = startIdx(trial(l - t) + 1) - 1;
            else
                e = length(EEG.event);
            end
            for i = 0 : e - s, EEG.event(e - i) = []; end
        end
    end
    
    pop_saveset(EEG, 'filename', char(filelist(sub)), ...
        'filepath', p.Results.savepath);
end
end