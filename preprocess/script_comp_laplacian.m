%% Initialize
wd = 'C:\Users\meini\OneDrive\eegAnalysis';
wd_fig = fullfile(wd, 'figure');
wd_ana = fullfile(wd, 'analysis');
cd(wd);

loadpath = fullfile(wd, ...
    'reve_epoch_intpo20-24_lp40_bc_filt1-100_norest', 'U');
[filename, ~, filepath] = getFilename(loadpath, '*.set');
nsub = numel(filename);

savepath = fullfile(wd, 'comp_laplacian');
checkPath(savepath);
pop_editoptions('option_single', false);

%% Load channel files
chanlocs = load(fullfile(wd, 'chanlocs', ...
    'channel_BrainProducts_ActiCap_64.mat'));
nbchan = numel(chanlocs.Channel);
chanlocs = trans_electrodes(chanlocs, 'ft');
chanlocs = ft_convert_units(chanlocs, 'mm'); % convert for SL
chanlocs = trans_electrodes(chanlocs, 'eeglab');

bad_chans = [ ...
    41 46 17 22 28 32 ...
    37 7 42 33 54];
%% Apply Spatial Filter: CAR and/or SL
% Default SL method: Perrin et al.
% fieldtrip's implementation is preferred
% 1 volt (V) = 1e6 microvolt (uV)

% SSL in fieldtrip: determine parameters of spherical splines
% unit: V/m^2 e.g. 0.2*1e4, also we have V/m^2 == uV/mm^2

nscheme = 6;
ref = [];
ref.name = '';
ref.ref = '';
ref.interpolate = 0;
reref_scheme = repmat(ref, 1, nscheme);
scheme_name = {'CAR_0', 'SL_0', 'both_0', ...
    'CAR_1', 'SL_1', 'both_1'};

reref_scheme(1).ref = 'CAR'; reref_scheme(1).interpolate = 0; refer_scheme(1).name = scheme_name{1};
reref_scheme(2).ref = 'SL'; reref_scheme(2).interpolate = 0; refer_scheme(2).name = scheme_name{2};
reref_scheme(3).ref = 'both'; reref_scheme(3).interpolate = 0; refer_scheme(3).name = scheme_name{3};
reref_scheme(4).ref = 'CAR'; reref_scheme(4).interpolate = 1; refer_scheme(4).name = scheme_name{4};
reref_scheme(5).ref = 'SL'; reref_scheme(5).interpolate = 1; refer_scheme(5).name = scheme_name{5};
reref_scheme(6).ref = 'both'; reref_scheme(6).interpolate = 1; refer_scheme(6).name = scheme_name{6};

snr_origin = zeros(1, nsub);

for i = 1:nsub
    eeg = pop_loadset(filepath{i});
    eeg.chanlocs = chanlocs;
    
    rawdata = get_rawdata(eeg, 'eeglab');
    rawdata = reshape(rawdata, size(rawdata,1), []);
    snrmat = arrayfun(@(x) snr(rawdata(x,:), Fs), 1:size(rawdata,1));
    snr_origin(i) = mean(snrmat);
    
    for r = 1:nscheme
        reref.(refer_scheme(r).name) = eeg_re_reference(eeg, reref_scheme(r).ref, ...
            bad_chans, reref_scheme(r).interpolate);
    end
    
    save(fullfile(savepath, [filename{i}(1:end-4) '.mat']), 'reref');
    
end

% % SSL in PerrinX by MikeXCohen
% % unit: ??
% leg_order = 50;
% lambda = 1e-5;
% for i = 1:nsub
%     [data_sl, G, H] = laplacian_perrinX(eeg.data, [eeg.chanlocs.X], ...
%         [eeg.chanlocs.Y], [eeg.chanlocs.Z], leg_order, lambda);
% end

%% Estimate SNR and Compare EEG topologies
% SNR: `snr`
% Topo plot: `ft_topoplotER`
% Cluster: `ft_clusterplot`
savepath = fullfile(wd_fig, 'comp_laplacian');
checkPath(savepath);

Fs = 500;
scheme_name = {'CAR_0', 'SL_0', 'both_0', ...
    'CAR_1', 'SL_1', 'both_1'};

snr_reref = zeros(nsub, numel(scheme_name));

for i = 1:nsub
    data = load(fullfile(wd, 'comp_laplacian', ...
        [filename{i}(1:end-4) '.mat']));
    data = data.reref;
    
    f = figure;
    f.Units = 'pixels';
    f.Position = [1.67 1.67 638.67 639.33];
    f.PaperPosition = [1 1 7.5 10];
    plot_ord = [1 3 5 19 21 23];
    for r = 1:numel(scheme_name)
        tmp = data.(scheme_name{r});
        
        rawdata = get_rawdata(tmp, []);
        rawdata = reshape(rawdata, size(rawdata,1), []);
        snrmat = arrayfun(@(x) snr(rawdata(x,:), Fs), 1:size(rawdata,1));
        snr_reref(i,r) = mean(snrmat);
        
        cfg = [];
        tmp_avg = ft_timelockanalysis(cfg, tmp);
        
        subplot(6,6,[plot_ord(r) plot_ord(r)+1 ...
            plot_ord(r)+6 plot_ord(r)+7]);
        cfg = [];
        cfg.comment = 'no';
        cfg.xlim = [-500 0];
        ft_topoplotER(cfg, tmp_avg);
        title(scheme_name{r});        
        
        subplot(6,6,[plot_ord(r)+12 plot_ord(r)+13]);
        histogram(tmp_avg.avg(:));
        title(['-min=' num2str(round(min(tmp_avg.avg(:)),4)) ...
            '  -max=' num2str(round(max(tmp_avg.avg(:)),4))]);
        xlabel(['-snr=' num2str(round(snr_reref(i,r),2)) 'db'])
    end
    suptitle(['-avg  -time=[-0.5 0]s' ...
        '  -snr=' num2str(round(snr_origin(i),2)) 'db'...
        '  -sub=' replace(filename{i}(1:end-4), '_', '-')]);
    
    if i == 1
        export_fig(fullfile(savepath, 'comp_laplacian.pdf'), '-pdf');
    else
        export_fig(fullfile(savepath, 'comp_laplacian.pdf'), ...
            '-append', '-pdf');
    end
%     print(f, fullfile(savepath, [filename{i}(1:end-4) '.png']), ...
%         '-dpng', '-r300');
    
    close all
    delete(f);
    delete(gcf);
end

save(fullfile(wd_ana, 'comp_reref_car_sl.mat'), ...
    'snr_origin', 'snr_reref');

%% Independent Components
cfg = [];
cfg.method = 'runica';
cfg.channel = 'all';
cfg.trials = 'all';

for i = 1:nsub
    eeg_ft = load(fullfile(wd, 'comp_laplacian', ...
        [filename{i}(1:end-4) '.mat']));
    eeg_ft = eeg_ft.reref.SL_0;
    
    comp = ft_componentanalysis(cfg, eeg_ft);
    
    cfg = [];
    cfg.elec = eeg_ft.elec;
    cfg.channel = 'all';
    layout = ft_prepare_layout(cfg, comp);
    
    cfg = [];
    cfg.component = 1:53;
    cfg.layout = layout;
    ft_topoplotIC(cfg,comp);
    
end
