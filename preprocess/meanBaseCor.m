function [cor] = meanBaseCor(epoch, base, varargin)
p = inputParser;
addRequired(p, 'epoch', @isstruct);
addRequired(p, 'base', @isstruct);
addParameter(p, 'savepath', [], @ischar);
addParameter(p, 'filename', [], @ischar);
parse(p, epoch, base, varargin{:});
epoch = p.Results.epoch;
base = p.Results.base;

cor = epoch;
for trial = 1 : size(epoch.data, 3) % # of epoches
    for channel = 1 : size(epoch.data, 1) % # of channels
        meanV = mean(base.data(channel, :, trial));
        cor.data(channel, :, trial) = ...
            cor.data(channel, :, trial) - meanV;
    end
end 

if (nargin > 2) && ~isempty(p.Results.savepath) && ~isempty(p.Results.filename)
    pop_saveset(cor, 'filename', p.Results.filename, ...
        'filepath', p.Results.savepath);
end

end