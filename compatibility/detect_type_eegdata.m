function eegdata_type = detect_type_eegdata(eegdata_st)
msg = def_error_message();
if ~isstruct(eegdata_st)
    error(msg.not_structure);
end

candidates = {'eeglab', 'ft'};
required = [];
required.ft = {'label', 'time', 'trial'};
required.eeglab = {'times', 'data', 'chanlocs'};

eegdata_type = '';
for i = 1:numel(candidates)
    flag = 0;
    field = required.(candidates{i});
    flag_field = 1;
    for j = 1:numel(field)
        if ~isfield(eegdata_st, field{j})
            flag_field = 0;
            break;
        end
    end
    
    if flag_field == 1
        flag = 1;
        eegdata_type = candidates{i};
        break;
    end
end

if isempty(eegdata_type) || flag == 0
    error(msg.unknown_datatype);
end

end