function data_new = trans_eegdata(data_orig, new_type, varargin)
msg = def_error_message();
if ~isstruct(data_orig)
    error(msg.not_structure);
end

if nargin >= 3
    orig_type = varargin{1};
else
    orig_type = detect_type_eegdata(data_orig);
end

rawdata = get_rawdata(data_orig, orig_type);

switch lower(orig_type)
    case 'ft'
        times = data_orig.time;
        if iscell(times)
            times = times{1};
        end
        elec = data_orig.elec;
        srate = data_orig.fsample * 1000; % in Hz
    case 'eeglab'
        elec = data_orig.chanlocs;
        times = data_orig.times;
        event = data_orig.event;
        epoch = data_orig.epoch;
        srate = data_orig.srate; % in Hz
    otherwise
        error(msg.invalid_datatype);
end

switch lower(new_type)
    case 'ft'
        data_new = def_eegdata_ft(rawdata, times, elec, srate);
    case 'eeglab'
        data_new = def_eegdata_eeglab(rawdata, times, elec, srate);
    otherwise
        error(msg.invalid_datatype);
end

end


