function data_eeglab = def_eegdata_eeglab(rawdata, times, elec, srate)
% function data_eeglab = def_eegdata_eeglab(rawdata, times, elec, srate)
% Definition of data structure in EEGLAB.
%
% Required Inputs
% - rawdata: 2D or 3D numeric matrix, [nbchan * npnts * nepochs]
% - times: 1D numeric array, exact time of each point, numel(times)==npnts
% - elec: structure of electrode information, in the EEGLAB or Fieldtrip or
% Brainstorm form
% - srate: numeric value, the unit of sampling rate is Hz, e.g. srate=500
%
% Output
% - EEGLAB structure

data_eeglab = [];

% data information
nbchan = size(rawdata, 1);
pnts = size(rawdata, 2);
if numel(size(rawdata)) > 2
    trials = size(rawdata, 3);
else
    trials = 1;
end

data_eeglab.nbchan = nbchan;
data_eeglab.trials = trials;
data_eeglab.pnts = pnts;
data_eeglab.data = rawdata;

% time information
sbnd = [-20 20]; % range of time in seconds
mt = mean(times);
if mt >= sbnd(1) && mt <= sbnd(2)
    tunit = 's';
elseif mt >= sbnd(1)*1000 && mt <= sbnd(2)*1000
    tunit = 'ms';
else
    tunit = 'undefined';
end
    
if strcmp(tunit, 's')
    times = times .* 1000;
end
xmin = times(1) / 1000;
xmax = times(end) / 1000;
    
data_eeglab.xmin = xmin;
data_eeglab.xmax = xmax;
data_eeglab.times = times;

data_eeglab.srate = srate;

% channel information
elec_type = detect_type_electrodes(elec);
if ~strcmp(elec_type, 'eeglab')
    elec = trans_electrodes(elec, 'eeglab');
end
data_eeglab.chanlocs = elec;
data_eeglab.urchanlocs = [];
data_eeglab.chaninfo = [];
data_eeglab.ref = '';

% event information
data_eeglab.event = [];
data_eeglab.urevent = [];
data_eeglab.eventdescription = [];
data_eeglab.epoch = [];

% ICA information
data_eeglab.icaact = [];
data_eeglab.icawinv = [];
data_eeglab.icasphere = [];
data_eeglab.icaweights = [];
data_eeglab.icachansind = [];

% other information
field_char_empty = {'setname', 'filename', 'filepath', 'subject', ...
    'group', 'condition', 'comments', 'saved', 'datfile'};
for i = 1:numel(field_char_empty)
    data_eeglab.(field_char_empty{i}) = '';
end

end