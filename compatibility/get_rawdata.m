function rawdata = get_rawdata(input_st, st_type)
% function rawdata = get_rawdata(input_st, st_type)
% rawdata = [nbchan, pnts/times, trials]
msg = def_error_message();
if ~isstruct(input_st)
    error(msg.not_structure);
end

if isempty(st_type)
    st_type = detect_type_eegdata(input_st);
end

switch st_type
    case 'eeglab'
        rawdata = input_st.data;
    case 'ft'
        trial = input_st.trial;
        if numel(trial) == 1
            try
                rawdata = trial{1};
            catch ME
                if strcmp(ME.identifier, 'MATLAB:cellRefFromNonCell')
                    rawdata = trial;
                end
                rethrow(ME);
            end
                
        else
            rawdata = zeros([size(trial{1}) numel(trial)]);
            for i = 1:numel(trial)
                rawdata(:,:,i) = trial{i};
            end
        end
    otherwise
        error(msg.invalid_datatype);
end

end