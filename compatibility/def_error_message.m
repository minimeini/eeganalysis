% function msg = def_error_message()
%
% msg.not_structure = 'data must be a structure';
% msg.unknown_datatype = 'Unknown electrode data structure';
% msg.invalid_datatype = 'Current data type is not supported yet.';
% msg.invalid_elec_input = 'orig electrodes information must be a structure';
% msg.invalid_elec_eeglab = 'Current data type is an invalid eeglab electrode format';


function msg = def_error_message()
msg = [];
msg.not_structure = 'data must be a structure';
msg.unknown_datatype = 'Unknown electrode data structure';
msg.invalid_datatype = 'Current data type is not supported yet.';
msg.invalid_elec_input = 'orig electrodes information must be a structure';
msg.invalid_elec_eeglab = 'Current data type is an invalid eeglab electrode format';

end