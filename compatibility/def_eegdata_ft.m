function data_ft = def_eegdata_ft(rawdata, times, elec, srate)
% definition of data structure in fieldtrip

data_ft = [];

% data information
nbchan = size(rawdata, 1);
ntime = size(rawdata, 2);
epochdata = zeros([nbchan ntime]);
if numel(size(rawdata)) > 2
    ntrial = size(rawdata, 3);
    trial = repmat({epochdata}, 1, ntrial);
    for i = 1:ntrial
        trial{i} = rawdata(:,:,i);
    end
else
    ntrial = 1;
    trial = epochdata;
end

% time information
sbnd = [-20 20]; % range of time in seconds
mt = mean(times);
if mt >= sbnd(1) || mt <= sbnd(2)
    tunit = 's';
elseif mt >= sbd(1)*1000 || mt <= sbnd(2)*1000
    tunit = 'ms';
else
    tunit = 'undefined';
end
    
if strcmp(tunit, 'ms')
    times = times ./ 1000;
end

time = repmat({times}, 1, ntrial);

% channel information
elec_type = detect_type_electrodes(elec);
if ~strcmp(elec_type, 'ft')
    elec = trans_electrodes(elec, 'ft');
end
label = elec.label;

data_ft.label = label;
data_ft.time = time;
data_ft.trial = trial;
data_ft.elec = elec;
data_ft.fsample = srate / 1000;

end