function [elec_new] = trans_electrodes(elec_orig, new_type, varargin)
msg = def_error_message();

if ~isstruct(elec_orig)
    error(msg.invalid_elec_input);
end

required_args = 2;
orig_type = [];
if nargin >= required_args + 1
    orig_type = varargin{1};    
end
if isempty(orig_type)
    orig_type = detect_type_electrodes(elec_orig);
end

if nargin >= required_args + 2
    chantype = varargin{1};
else
    chantype = 'eeg';
end
if nargin >= required_args + 3
    chanunit = varargin{2};
else
    chanunit = 'uV';
end

%%
switch orig_type
    case 'ft'
        nbchan = size(elec_orig.elecpos, 1);
        elecpos = elec_orig.elecpos;
        label = elec_orig.label;
    case 'bst'
        nbchan = numel(elec_orig.Channel);
        elecpos = zeros(nbchan, 3);
        for i = 1:nbchan
            elecpos(i, :) = elec_orig.Channel(i).Loc;
        end
        label = arrayfun(@(i) elec_orig.Channel(i).Name, ...
            1:nbchan, 'UniformOutput', 0);
    case 'eeglab'
        nbchan = numel(elec_orig);
        if nbchan <= 1
            error(msg.invalid_elec_eeglab);
        end
        
        coord_fields = {'X', 'Y', 'Z'};
        flag = 1;
        for i = 1:numel(coord_fields)
            if ~isfield(elec_orig(1), coord_fields{i})
                flag = 0;
                break;
            end
        end
        if flag == 0
            error(msg.invalid_datatype)
        end
        
        elecpos = zeros(nbchan, 3);
        for i = 1:nbchan
            elecpos(i, :) = ...
                [elec_orig(i).X elec_orig(i).Y elec_orig(i).Z];
        end
        label = arrayfun(@(i) elec_orig(i).labels, ...
            1:nbchan, 'UniformOutput', 0);
    otherwise 
        error(msg.invalid_datatype);
end

%%
switch new_type
    case 'ft'
        elec_new = def_elec_ft(nbchan, chantype, chanunit);
        elec_new.label = label;
        elec_new.elecpos = elecpos;
        elec_new.chanpos = elecpos;
        elec_new = ft_determine_units(elec_new);
    case 'bst'
        elec_new = def_elec_bst(nbchan, chantype);
        for i = 1:nbchan
            elec_new.Channel(i).Name = label{i};
            elec_new.Channel(i).Loc = elecpos(i, :);
        end
    case 'eeglab'
        elec_new = def_elec_eeglab(nbchan);
        for i = 1:nbchan
            elec_new(i).X = elecpos(i, 1);
            elec_new(i).Y = elecpos(i, 2);
            elec_new(i).Z = elecpos(i, 3);
            elec_new(i).labels = label{i};
        end
    otherwise
        error(msg.invalid_datatype);
end

end

%% definition of electrodes structure in fieldtrip
function elec_ft = def_elec_ft(nbchan, varargin)

if nargin > 1
    chantype = varargin{1};
else
    chantype = 'eeg';
end

if nargin > 2
    chanunit = varargin{2};
else
    chanunit = 'uV';
end

elec_ft = [];

% required fields: `label`, 'elecpos', 'chanpos'
elec_ft.label = arrayfun(@(x) {''}, ...
    1:nbchan, 'UniformOutput', 0);
elec_ft.elecpos = zeros(nbchan, 3);
elec_ft.chanpos = elec_ft.elecpos;

% optional fields
elec_ft.chantype = repmat({chantype}, nbchan, 1);
elec_ft.chanunit = repmat({chanunit}, nbchan, 1);
elec_ft.senstype = chantype;

end

%% definition of electrodes structure in brainstorm
function elec_bst = def_elec_bst(nbchan, varargin)

if nargin > 1
    chantype = upper(varargin{1});
else
    chantype = 'EEG';
end

elec_bst = [];

% required fields: `Channel`
chan_st = [];
chan_st.Name = '';
chan_st.Comment = '';
chan_st.Type = chantype;
chan_st.Loc = zeros(3, 1);
chan_st.Orient = [];
chan_st.Weight = 1;
chan_st.Group = [];

elec_bst.Channel = repmat(chan_st, 1, nbchan);

% optional fields: fnames
fnames = {'Comment', 'HeadPoints', 'History', ...
    'IntraElectrodes', 'MegRefCoef', 'Projector', 'SCS', ...
    'TransfEeg', 'TransfEegLabels', 'TransfMeg', 'TransfMegLabels'};
for i = 1:numel(fnames)
    elec_bst.(fnames{i}) = [];
end
elec_bst.TransfEEG = {zeros(4,4) zeros(4,4)};
elec_bst.TransfEegLabels = {'' ''};
elec_bst.Comment = '';

end

%% definition of electrodes structure in EEGLAB
function elec_eeglab = def_elec_eeglab(nbchan, varargin)
chan_st.X = 0;
chan_st.Y = 0;
chan_st.Z = 0;
chan_st.labels = '';
elec_eeglab = repmat(chan_st, 1, nbchan);
end