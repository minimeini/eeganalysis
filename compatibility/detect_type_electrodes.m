function elec_type = detect_type_electrodes(elec_st)
% define previous knowledge
msg = def_error_message();
candidates = {'ft', 'bst', 'eeglab'};
eeglab_coord = {'xyz', 'sph', 'polar'};

required = [];
required.ft = {'label', 'elecpos', 'chanpos'};
required.bst = {'Channel'};

required.eeglab.common = 'labels';
required.eeglab.xyz = {'X', 'Y', 'Z'};
required.eeglab.sph = {'sph_theta', 'sph_phi', 'sph_radius'};
required.eeglab.polar = {'theta', 'radius'};

if ~isstruct(elec_st)
    error(msg.not_structure);
end

if numel(elec_st) == 1
    if isfield(elec_st, 'Channel')
        elec_type = 'bst';
    else
        flag = 1;
        for i = 1:numel(required.ft)
            if ~isfield(elec_st, required.ft{i})
                flag = 0;
                break;
            end
        end
        
        if flag == 1
            elec_type = 'ft';
        else
            error(msg.unknown_datatype);
        end
    end
elseif numel(elec_st) > 1
    if ~isfield(elec_st, required.eeglab.common)
        error(msg.unknown_datatype);
    else
        flag_coord = 0;
        for i = 1:numel(eeglab_coord)
            field = required.eeglab.(eeglab_coord{i});
            flag_field = 1;
            for j = 1:numel(field)
                if ~isfield(elec_st, field{j})
                    flag_field = 0;
                    break;
                end
            end
            
            if flag_field == 1
                flag_coord = 1;
                break;
            end
        end
        
        if flag_coord == 0
            error(msg.unknown_datatype);
        else
            elec_type = 'eeglab';
        end
    end
end

end